﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace RegEquip
{
    public partial class Principal : Form
    {
        DateTimePicker dtp = new DateTimePicker();
        Rectangle _rect;
        private Util util = new Util();
        private StringBuilder msgAnuncio;
        private string id_usuario;
        //private string rutaDestino = @"C:\Users\gabriel.gutierrez\Desktop\";
        private string rutaDestino = @"\\antofagasta.molina.local\global\Informatica\";

        /* Constructor del formulario, "algo" para obtener parametros del formulario de inicio.
         */
        public Principal(string usuarioID)
        {
            InitializeComponent();
            cargarListas();
            id_usuario = usuarioID;
            gvEquipos.Controls.Add(dtp);
            dtp.Visible = false;
            dtp.Format = DateTimePickerFormat.Custom;
            dtp.TextChanged += new EventHandler(dtp_TextChange);
            btnSubir.Enabled = true;
        }

        //Metodo para la carga de listas del formulario
        private void cargarListas()
        {

            //Usuarios
            Log_sistema_usuario_sistema m_log_usuario = new Log_sistema_usuario_sistema();
            Lista p_lista = new Lista();
            //cbUsers.Items.Insert(0, "Seleccione Usuario");

            DataSet m_lista_usuarios = m_log_usuario.get_lista_usuario_sistemas_dt(p_lista, true);
            DataRow row = m_lista_usuarios.Tables[0].NewRow();
            row["id_usuario_pk"] = 0;
            row["nombre_completo"] = "<Seleccione un Usuario>";
            m_lista_usuarios.Tables[0].Rows.InsertAt(row, 0);

            if (m_lista_usuarios != null)
            {                
                cbUsers.DataSource = m_lista_usuarios.Tables[0];
                cbUsers.ValueMember = "id_usuario_pk";
                cbUsers.DisplayMember = "nombre_completo";
                cbUsers.SelectedIndex = 0;
            }            
            cbUsers.Focus();

            //Items (Tipos de Equipos)
            //Datos que sean validos
            p_lista.sql_parametros.Add("valido");
            p_lista.sql_valores.Add("1");
            
            Log_sistema_as_tipo_equipo log_tipo_equipo = new Log_sistema_as_tipo_equipo();
            DataSet m_lista_tipo_equipo = log_tipo_equipo.get_lista_as_tipo_equipos_ds(p_lista);
            
            DataGridViewComboBoxColumn cmbItem = gvEquipos.Columns["clItem"] as DataGridViewComboBoxColumn;
            cmbItem.DataSource = m_lista_tipo_equipo.Tables[0];
            cmbItem.DisplayMember = "descripcion_tipo_equipo";
            cmbItem.ValueMember = "id_tipo_equipo_pk";

            p_lista.sql_parametros.Clear();
            p_lista.sql_valores.Clear();


            //Marcas
            //Datos que sean validos
            p_lista.sql_parametros.Add("valido");
            p_lista.sql_valores.Add("1");

            Log_sistema_as_marca log_marca = new Log_sistema_as_marca();
            DataSet m_lista_marca = log_marca.get_lista_as_marcas_ds(p_lista);

            DataGridViewComboBoxColumn cmbMarca = gvEquipos.Columns["clMarca"] as DataGridViewComboBoxColumn;
            cmbMarca.DataSource = m_lista_marca.Tables[0];
            cmbMarca.DisplayMember = "descripcion_marca";
            cmbMarca.ValueMember = "id_marca_pk";

            p_lista.sql_parametros.Clear();
            p_lista.sql_valores.Clear();


            //Marcas
            //Datos que sean validos
            p_lista.sql_parametros.Add("valido");
            p_lista.sql_valores.Add("1");

            Log_sistema_as_modelo log_modelo = new Log_sistema_as_modelo();
            DataSet m_lista_modelo = log_modelo.get_lista_as_modelos_ds(p_lista);

            DataGridViewComboBoxColumn cmbModelo = gvEquipos.Columns["clModelo"] as DataGridViewComboBoxColumn;
            cmbModelo.DataSource = m_lista_modelo.Tables[0];
            cmbModelo.DisplayMember = "descripcion_modelo";
            cmbModelo.ValueMember = "id_modelo_pk";

        }
        //Cierre de la aplicación
        private void Principal_FormClosed(object sender, FormClosedEventArgs e)
        {
            //this.Close();
            Application.Exit();
        }

        //Al cargar formulario, carga la fecha actual.
        private void Principal_Load(object sender, EventArgs e)
        {
            lblFecha.Text += " " + DateTime.Now.ToShortDateString();
            //gvEquipos.Columns[9].DefaultCellStyle.Format = "dd/MM/yyyy";
            //gvEquipos.Rows[0].Cells[9].Value = DateTime.Now.Date;            
        }

        //Evento de grilla al crear una nueva fila.
        private void gvEquipos_RowsAdded(object sender, DataGridViewRowsAddedEventArgs e)
        {
            gvEquipos.Rows[e.RowIndex - 1].Cells[0].Value = e.RowIndex;
        }
        
        //Evento de la grilla, se le agrego la caracteristica de dibujar el numero de fila.
        private void gvEquipos_RowPostPaint(object sender, DataGridViewRowPostPaintEventArgs e)
        {
            using (SolidBrush b = new SolidBrush(gvEquipos.RowHeadersDefaultCellStyle.ForeColor))
            {
                e.Graphics.DrawString((e.RowIndex + 1).ToString(), e.InheritedRowStyle.Font, b, e.RowBounds.Location.X + 10, e.RowBounds.Location.Y + 4);
            }            
        }

        //Evento para crear el cuadro de fecha en una de las columnas de la grilla.
        private void gvEquipos_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            try
            {
                if (e.RowIndex >= 0)
                {
                    switch (gvEquipos.Columns[e.ColumnIndex].Name)
                    {
                        /*Si selecciona la celda Hasta y el check de la celda Momentaneo esta en true
                         * se activa el selector de fechas, en caso contrario, se deja inactivado.*/
                        case "clHasta":
                            DataGridViewCheckBoxCell chkMomentaneo = gvEquipos.Rows[e.RowIndex].Cells[9] as DataGridViewCheckBoxCell;
                            if (chkMomentaneo.Value == chkMomentaneo.TrueValue)
                            {
                                _rect = gvEquipos.GetCellDisplayRectangle(e.ColumnIndex, e.RowIndex, true);
                                dtp.Size = new Size(_rect.Width, _rect.Height);
                                dtp.Location = new Point(_rect.X, _rect.Y);
                                dtp.Visible = true;
                            }
                            break;
                    }
                }                
            }
            catch (Exception ex)
            {

            }
        }

        //Evento asignado para las fechas.
        private void dtp_TextChange(object sender, EventArgs e)
        {
            gvEquipos.CurrentCell.Value = dtp.Text.ToString();
        }


        private void gvEquipos_ColumnWidthChanged(object sender, DataGridViewColumnEventArgs e)
        {
            dtp.Visible = false;
        }

        private void gvEquipos_Scroll(object sender, ScrollEventArgs e)
        {
            dtp.Visible = false;
        }
        
        //Evento para registrar la información señalada de registro.
        private void btnRegistro_Click(object sender, EventArgs e)
        {
            int cantFilas = gvEquipos.Rows.Count;
            if (cantFilas > 1)//Valida que hayan filas ingresadas.
            {
                List<As_asignacion> listaAsignacion = new List<As_asignacion>();//Crea una lista de asignaciones.
                List<As_checklist> listaCheckList = new List<As_checklist>();//Crea una lista de checklist
                //Genera un folio para el documento.
                long folio = DateTime.Now.ToFileTime();

                for (int i = 0; i < cantFilas - 1; i++)//Recorre la grilla
                {
                    if (!validarDatosFila(gvEquipos.Rows[i]))//Verifica si están todos los datos ingresados.
                    {                        
                        MessageBox.Show(msgAnuncio.ToString(), "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                        break;
                    }
                    else
                    {
                        //Crea objeto asignacion e ingresa los datos.
                        As_asignacion asignacion = new As_asignacion();
                        asignacion.folio = folio.ToString();
                        asignacion.id_usuario_asignado = cbUsers.SelectedValue.ToString();
                        asignacion.fecha_asignacion = DateTime.Now.ToString();
                        asignacion.id_equipo = gvEquipos.Rows[i].Cells[1].Value.ToString();
                        asignacion.id_estado = "1";
                        asignacion.observacion = gvEquipos.Rows[i].Cells[11].Value.ToString();
                        listaAsignacion.Add(asignacion);//Se agrega el objeto a una lista la cual se guardara en la base.

                        //Crea objeto checklist e ingresa los datos.
                        As_checklist checkList = new As_checklist();
                        string tipoEquipo = gvEquipos.Rows[i].Cells[1].EditedFormattedValue.ToString();
                        switch (tipoEquipo)
                        {
                            case "Notebook":
                                checkList.chk_a = "1";
                                checkList.chk_b = "1";
                                checkList.chk_c = "1";
                                checkList.chk_d = "1";
                                checkList.chk_e = "1";
                                checkList.chk_f = "1";
                                checkList.chk_g = "1";
                                checkList.chk_h = "1";
                                break;
                            case "Celular":
                                checkList.chk_i = "1";
                                checkList.chk_j = "1";
                                break;
                            case "Chip":
                                checkList.chk_i = "1";
                                checkList.chk_j = "1";
                                break;
                        }

                        listaCheckList.Add(checkList);
                    }                                            
                }

                /*Compara si estan todas las filas en la lista, de ser asi, se sube a la base y se genera reporte.
                 * en caso contrario, se informa que no se pueden guardar datos*/
                if (listaAsignacion.Count == (cantFilas - 1))
                {
                    //Crea el objeto as_asignación.
                    Log_sistema_as_asignacion log_asignacion = new Log_sistema_as_asignacion();
                    
                    //Crea el objeto as_detalle_linea
                    As_detalle_linea detalle_linea = new As_detalle_linea();
                    detalle_linea.fecha_creacion_as = DateTime.Now.ToString();
                    detalle_linea.id_usuario_creador = id_usuario;

                    bool guardado = true;//Guardar la asignacion
                    if (guardado)
                    {
                        MessageBox.Show("Se han guardado los datos correctamente. \nFavor subir reporte generado.", "Datos Almacenados", MessageBoxButtons.OK, MessageBoxIcon.Asterisk);
                        btnRegistro.Enabled = false;
                        btnSubir.Enabled = true;
                        Formulario nReport = new Formulario();
                        nReport.Show();
                    }
                    else
                    {
                        MessageBox.Show("Error al intentar guardar los datos.", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    }                    
                }
                else
                {
                    MessageBox.Show("No se puede guardar Datos", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
            }
            else
            {
                MessageBox.Show("Debe ingresar datos", "Error", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
            }
        }

        //Valida que se hayan ingresado todos los datos del registro según el tipo de equipo.
        private bool validarDatosFila(DataGridViewRow fila)
        {
            bool completo = true;
            msgAnuncio = new StringBuilder();
            /*Pregunta por la primera columna (Tipo Equipo).
             * Según el tipo de equipo, se realiza la verificación de ingreso de datos.*/
            ArrayList numColum = new ArrayList();//Se crea arreglo con las columnas que se revisan generalmente.
            numColum.Add(2);
            numColum.Add(3);            
            numColum.Add(5);
            numColum.Add(6);
            numColum.Add(7);
            numColum.Add(11);
            int cantFilas = gvEquipos.Rows.Count;
            int cantColumnas = gvEquipos.Columns.Count;//12 columnas
            int filaError = fila.Index + 1;//Se entrega información de la fila que presenta campos sin datos.
            string tipoEquipo = (fila.Cells[1].Value != null) ? fila.Cells[1].EditedFormattedValue.ToString() : "";
            switch (tipoEquipo)
            {
                case "":
                    //para cuando no selecciona un tipo de equipo
                    break;
                case "Notebook":
                    numColum.Add(4);
                    //numColum.Add(6);                    
                    break;
                case "Chip":
                    //numColum.Add(6);
                    break;
                case "Celular":
                    //numColum.Add(6);
                    break;
            }

            //Se revisan que las columnas tengan información.
            for (int i = 0; i < numColum.Count; i++)
            {
                if (fila.Cells[Convert.ToInt32(numColum[i])].Value == null)
                {
                    completo = false;
                    msgAnuncio.Append("Faltan datos en la fila " + filaError + ". Favor Completar." + Environment.NewLine);
                    break;
                }
            }

            //Se revisa si es momentaneo o no, de serlo, debe colocar la fecha.
            DataGridViewCheckBoxCell chkMomentaneo = fila.Cells[9] as DataGridViewCheckBoxCell;
            if (chkMomentaneo.Value == chkMomentaneo.TrueValue && fila.Cells[10].Value == null)
            {
                msgAnuncio.Append("Favor seleccionar fecha" + Environment.NewLine);
                completo = false;
            }

            //Se revisa el checklist de que este completo
            for (int i = 1; i < cantFilas; i++)
            {
                if (tipoEquipo.Equals("Notebook"))
                {
                    if (!chkA.Checked || !chkB.Checked || !chkC.Checked || !chkD.Checked || !chkE.Checked || !chkF.Checked || !chkG.Checked || !chkH.Checked)
                    {
                        msgAnuncio.Append("Favor de completar CheckList de Equipos" + Environment.NewLine);
                        completo = false;
                    }
                }
                else
                {
                    if (tipoEquipo.Equals(""))
                    {
                        msgAnuncio.Append("Debe Seleccionar un tipo de equipo." + Environment.NewLine);
                    }
                    else
                    {
                        if (!chkI.Checked || !chkJ.Checked)
                        {
                            msgAnuncio.Append("Favor de completar CheckList de Celulares" + Environment.NewLine);
                            completo = false;
                        }
                    }
                    
                }
            }
            return completo;
        }

        //Evento para activar el botón de registro cuando se selecciona un usuario.
        private void cbUsers_SelectedIndexChanged(object sender, EventArgs e)
        {
            string valor_usuario = cbUsers.SelectedValue.ToString();
            bool es_numero = util.esNumero(valor_usuario);
            if (es_numero && !valor_usuario.Equals("0"))
            {
                btnRegistro.Enabled = true;
            }
            else
            {
                btnRegistro.Enabled = false;
            }
        }
        
        //Evento que se encarga de subir un archivo a una carpeta en el servidor.
        private void btnSubir_Click(object sender, EventArgs e)
        {
            try
            {
                OpenFileDialog open = new OpenFileDialog();
                open.Filter = "Archivos pdf (*.pdf)|*.pdf";
                open.Title = "Formulario de registro";
                if (open.ShowDialog() == DialogResult.OK)
                {
                    if (!System.IO.Directory.Exists(rutaDestino))
                    {
                        System.IO.Directory.CreateDirectory(rutaDestino);
                    }
                    string path = Path.Combine(rutaDestino, open.SafeFileName);
                    File.Copy(open.FileName, path);
                    MessageBox.Show("Archivo " + open.SafeFileName + " guardado con exito.", "Archivo Respaldado", MessageBoxButtons.OK, MessageBoxIcon.Information);
                }
                open.Dispose();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Archivo Respaldado", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }            
        }        
    }
}
