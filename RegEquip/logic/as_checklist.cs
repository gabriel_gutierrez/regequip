/**::::::::::::::::::::::::::::::::::::::::::
 *
 * Clase: OBJETO as_checklist
 * @author Gabriel Gutierrez Carcamo <gabriel.gutierrez@emfe.cl>
 * @copyright Sistema de Registro de Equipos (c) 2018
 * @version 0.1 lunes, 19 de marzo de 2018 (10:33 Hr.)
 * 
 * :::::::::::::::::::::::::::::::::::::::::::*/
 
/// <summary>Define un objeto  as_checklist que permite almacenarlo en la memoria.</summary>

using System;

public class As_checklist
{
  private string a_id_checklist;
  private string a_chk_a;
  private string a_chk_b;
  private string a_chk_c;
  private string a_chk_d;
  private string a_chk_e;
  private string a_chk_f;
  private string a_chk_g;
  private string a_chk_h;
  private string a_chk_i;
  private string a_chk_j;
    

/** ------------- SE INICIA EL CONSTRUCTOR DE LA CLASE -----------*/
  public As_checklist(string p_id_checklist, string p_chk_a, string p_chk_b, string p_chk_c, string p_chk_d, string p_chk_e, string p_chk_f, string p_chk_g, string p_chk_h, string p_chk_i, string p_chk_j )
  {
    this.a_id_checklist = p_id_checklist;
    this.a_chk_a = p_chk_a;
    this.a_chk_b = p_chk_b;
    this.a_chk_c = p_chk_c;
    this.a_chk_d = p_chk_d;
    this.a_chk_e = p_chk_e;
    this.a_chk_f = p_chk_f;
    this.a_chk_g = p_chk_g;
    this.a_chk_h = p_chk_h;
    this.a_chk_i = p_chk_i;
    this.a_chk_j = p_chk_j;

  }
/** --------------------------------------------------------------*/

/** ------------- SE INICIA EL CONSTRUCTOR DE LA CLASE -----------*/
  public As_checklist() { }
/** --------------------------------------------------------------*/

/** -------- METODO QUE SE INICIA AL ELIMINAR EL OBJETO ----------*/
  ~As_checklist() { }
/** --------------------------------------------------------------*/


  /// <summary> Funci�n para asignar o retorna el valor id_checklist sin parametros </summary>
  /// <returns> Devuelve una cadena de caracteres (string) </returns>
  public string id_checklist
  {  get { return this.a_id_checklist; }
     set { this.a_id_checklist = value; }  }
    
  /// <summary> Funci�n para asignar o retorna el valor chk_a sin parametros </summary>
  /// <returns> Devuelve una cadena de caracteres (string) </returns>
  public string chk_a
  {  get { return this.a_chk_a; }
     set { this.a_chk_a = value; }  }

  /// <summary> Funci�n para asignar o retorna el valor chk_b sin parametros </summary>
  /// <returns> Devuelve una cadena de caracteres (string) </returns>
  public string chk_b
  {  get { return this.a_chk_b; }
     set { this.a_chk_b = value; }  }

  /// <summary> Funci�n para asignar o retorna el valor chk_c sin parametros </summary>
  /// <returns> Devuelve una cadena de caracteres (string) </returns>
  public string chk_c
  {  get { return this.a_chk_c; }
     set { this.a_chk_c = value; }  }

  /// <summary> Funci�n para asignar o retorna el valor chk_d sin parametros </summary>
  /// <returns> Devuelve una cadena de caracteres (string) </returns>
  public string chk_d
  {  get { return this.a_chk_d; }
     set { this.a_chk_d = value; }  }

  /// <summary> Funci�n para asignar o retorna el valor chk_e sin parametros </summary>
  /// <returns> Devuelve una cadena de caracteres (string) </returns>
  public string chk_e
  {  get { return this.a_chk_e; }
     set { this.a_chk_e = value; }  }

  /// <summary> Funci�n para asignar o retorna el valor chk_f sin parametros </summary>
  /// <returns> Devuelve una cadena de caracteres (string) </returns>
  public string chk_f
  {  get { return this.a_chk_f; }
     set { this.a_chk_f = value; }  }

  /// <summary> Funci�n para asignar o retorna el valor chk_g sin parametros </summary>
  /// <returns> Devuelve una cadena de caracteres (string) </returns>
  public string chk_g
  {  get { return this.a_chk_g; }
     set { this.a_chk_g = value; }  }

  /// <summary> Funci�n para asignar o retorna el valor chk_h sin parametros </summary>
  /// <returns> Devuelve una cadena de caracteres (string) </returns>
  public string chk_h
  {  get { return this.a_chk_h; }
     set { this.a_chk_h = value; }  }

  /// <summary> Funci�n para asignar o retorna el valor chk_i sin parametros </summary>
  /// <returns> Devuelve una cadena de caracteres (string) </returns>
  public string chk_i
  {  get { return this.a_chk_i; }
     set { this.a_chk_i = value; }  }

  /// <summary> Funci�n para asignar o retorna el valor chk_j sin parametros </summary>
  /// <returns> Devuelve una cadena de caracteres (string) </returns>
  public string chk_j
  {  get { return this.a_chk_j; }
     set { this.a_chk_j = value; }  }


} /** FIN DE LA CLASE AS_CHECKLIST */
