/**::::::::::::::::::::::::::::::::::::::::::
 *
 * Clase: OBJETO empresa
 * @author Gabriel Gutierrez Carcamo <gabriel.gutierrez@emfe.cl>
 * @copyright Sistema de Registro de Equipos (c) 2018
 * @version 0.1 lunes, 19 de marzo de 2018 (12:10 Hr.)
 * 
 * :::::::::::::::::::::::::::::::::::::::::::*/
 
/// <summary>Define un objeto  empresa que permite almacenarlo en la memoria.</summary>

using System;

public class Empresa
{
  private string a_id_empresa;
  private string a_id_estado;
  private string a_descripcion;
  private string a_direccion;
  private string a_razon_social;
  private string a_rut;
  private string a_telefono;
  private string a_sitioweb;
  private string a_url_imagen;
  private string a_esExportada;
  private string a_codigoSAP1;

  /** atributos objetos */
  private Estado_empresa a_estado;


/** ------------- SE INICIA EL CONSTRUCTOR DE LA CLASE -----------*/
  public Empresa(string p_id_empresa, string p_id_estado, string p_descripcion, string p_direccion, string p_razon_social, string p_rut, string p_telefono, string p_sitioweb, string p_url_imagen, string p_esExportada, string p_codigoSAP1 )
  {
    this.a_id_empresa = p_id_empresa;
    this.a_id_estado = p_id_estado;
    this.a_descripcion = p_descripcion;
    this.a_direccion = p_direccion;
    this.a_razon_social = p_razon_social;
    this.a_rut = p_rut;
    this.a_telefono = p_telefono;
    this.a_sitioweb = p_sitioweb;
    this.a_url_imagen = p_url_imagen;
    this.a_esExportada = p_esExportada;
    this.a_codigoSAP1 = p_codigoSAP1;

  }
/** --------------------------------------------------------------*/

/** ------------- SE INICIA EL CONSTRUCTOR DE LA CLASE -----------*/
  public Empresa() { }
/** --------------------------------------------------------------*/

/** -------- METODO QUE SE INICIA AL ELIMINAR EL OBJETO ----------*/
  ~Empresa() { }
/** --------------------------------------------------------------*/


  /// <summary> Funci�n para asignar o retorna el valor id_empresa sin parametros </summary>
  /// <returns> Devuelve una cadena de caracteres (string) </returns>
  public string id_empresa
  {  get { return this.a_id_empresa; }
     set { this.a_id_empresa = value; }  }

  /// <summary> Funci�n para asignar o retorna el valor id_estado sin parametros </summary>
  /// <returns> Devuelve una cadena de caracteres (string) </returns>
  public string id_estado
  {  get { return this.a_id_estado; }
     set { this.a_id_estado = value; }  }

  /// <summary> Funci�n para asignar o retorna el valor id_estado sin parametros </summary>
  /// <returns> Devuelve una cadena de caracteres (string) </returns>
  public Estado_empresa estado
  {  get { return this.a_estado; }
     set { this.a_estado = value; }  }

  /// <summary> Funci�n para asignar o retorna el valor descripcion sin parametros </summary>
  /// <returns> Devuelve una cadena de caracteres (string) </returns>
  public string descripcion
  {  get { return this.a_descripcion; }
     set { this.a_descripcion = value; }  }

  /// <summary> Funci�n para asignar o retorna el valor direccion sin parametros </summary>
  /// <returns> Devuelve una cadena de caracteres (string) </returns>
  public string direccion
  {  get { return this.a_direccion; }
     set { this.a_direccion = value; }  }

  /// <summary> Funci�n para asignar o retorna el valor razon_social sin parametros </summary>
  /// <returns> Devuelve una cadena de caracteres (string) </returns>
  public string razon_social
  {  get { return this.a_razon_social; }
     set { this.a_razon_social = value; }  }

  /// <summary> Funci�n para asignar o retorna el valor rut sin parametros </summary>
  /// <returns> Devuelve una cadena de caracteres (string) </returns>
  public string rut
  {  get { return this.a_rut; }
     set { this.a_rut = value; }  }

  /// <summary> Funci�n para asignar o retorna el valor telefono sin parametros </summary>
  /// <returns> Devuelve una cadena de caracteres (string) </returns>
  public string telefono
  {  get { return this.a_telefono; }
     set { this.a_telefono = value; }  }

  /// <summary> Funci�n para asignar o retorna el valor sitioweb sin parametros </summary>
  /// <returns> Devuelve una cadena de caracteres (string) </returns>
  public string sitioweb
  {  get { return this.a_sitioweb; }
     set { this.a_sitioweb = value; }  }

  /// <summary> Funci�n para asignar o retorna el valor url_imagen sin parametros </summary>
  /// <returns> Devuelve una cadena de caracteres (string) </returns>
  public string url_imagen
  {  get { return this.a_url_imagen; }
     set { this.a_url_imagen = value; }  }

  /// <summary> Funci�n para asignar o retorna el valor esExportada sin parametros </summary>
  /// <returns> Devuelve una cadena de caracteres (string) </returns>
  public string esExportada
  {  get { return this.a_esExportada; }
     set { this.a_esExportada = value; }  }

  /// <summary> Funci�n para asignar o retorna el valor codigoSAP1 sin parametros </summary>
  /// <returns> Devuelve una cadena de caracteres (string) </returns>
  public string codigoSAP1
  {  get { return this.a_codigoSAP1; }
     set { this.a_codigoSAP1 = value; }  }


} /** FIN DE LA CLASE EMPRESA */
