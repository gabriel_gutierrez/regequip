/**::::::::::::::::::::::::::::::::::::::::::
 *
 * Clase: OBJETO as_detalle_linea
 * @author Gabriel Gutierrez Carcamo <gabriel.gutierrez@emfe.cl>
 * @copyright Sistema de Registro de Equipos (c) 2018
 * @version 0.1 lunes, 19 de marzo de 2018 (10:30 Hr.)
 * 
 * :::::::::::::::::::::::::::::::::::::::::::*/
 
/// <summary>Define un objeto  as_detalle_linea que permite almacenarlo en la memoria.</summary>

using System;

public class As_detalle_linea
{
  private string a_id_detalle_linea;
  private string a_fecha_creacion_as;
  private string a_id_usuario_creador;
  private string a_fecha_modificacion_as;
  private string a_id_usuario_modificador;
  private string a_fecha_anulacion_as;
  private string a_id_usuario_anulador;

  /** atributos objetos */
  private Usuario_sistema a_usuario_creador;
  private Usuario_sistema a_usuario_modificador;
  private Usuario_sistema a_usuario_anulador;


/** ------------- SE INICIA EL CONSTRUCTOR DE LA CLASE -----------*/
  public As_detalle_linea(string p_id_detalle_linea, string p_fecha_creacion_as, string p_id_usuario_creador, string p_fecha_modificacion_as, string p_id_usuario_modificador, string p_fecha_anulacion_as, string p_id_usuario_anulador )
  {
    this.a_id_detalle_linea = p_id_detalle_linea;
    this.a_fecha_creacion_as = p_fecha_creacion_as;
    this.a_id_usuario_creador = p_id_usuario_creador;
    this.a_fecha_modificacion_as = p_fecha_modificacion_as;
    this.a_id_usuario_modificador = p_id_usuario_modificador;
    this.a_fecha_anulacion_as = p_fecha_anulacion_as;
    this.a_id_usuario_anulador = p_id_usuario_anulador;

  }
/** --------------------------------------------------------------*/

/** ------------- SE INICIA EL CONSTRUCTOR DE LA CLASE -----------*/
  public As_detalle_linea() { }
/** --------------------------------------------------------------*/

/** -------- METODO QUE SE INICIA AL ELIMINAR EL OBJETO ----------*/
  ~As_detalle_linea() { }
/** --------------------------------------------------------------*/


  /// <summary> Funci�n para asignar o retorna el valor id_detalle_linea sin parametros </summary>
  /// <returns> Devuelve una cadena de caracteres (string) </returns>
  public string id_detalle_linea
  {  get { return this.a_id_detalle_linea; }
     set { this.a_id_detalle_linea = value; }  }

  /// <summary> Funci�n para asignar o retorna el valor fecha_creacion_as sin parametros </summary>
  /// <returns> Devuelve una cadena de caracteres (string) </returns>
  public string fecha_creacion_as
  {  get { return this.a_fecha_creacion_as; }
     set { this.a_fecha_creacion_as = value; }  }

  /// <summary> Funci�n para asignar o retorna el valor id_usuario_creador sin parametros </summary>
  /// <returns> Devuelve una cadena de caracteres (string) </returns>
  public string id_usuario_creador
  {  get { return this.a_id_usuario_creador; }
     set { this.a_id_usuario_creador = value; }  }

  /// <summary> Funci�n para asignar o retorna el valor id_usuario_creador sin parametros </summary>
  /// <returns> Devuelve una cadena de caracteres (string) </returns>

    public Usuario_sistema usuario_creador
    {
        get { return this.a_usuario_creador; }
        set { this.a_usuario_creador = value; }
    }

    /// <summary> Funci�n para asignar o retorna el valor fecha_modificacion_as sin parametros </summary>
    /// <returns> Devuelve una cadena de caracteres (string) </returns>
    public string fecha_modificacion_as
  {  get { return this.a_fecha_modificacion_as; }
     set { this.a_fecha_modificacion_as = value; }  }

  /// <summary> Funci�n para asignar o retorna el valor id_usuario_modificador sin parametros </summary>
  /// <returns> Devuelve una cadena de caracteres (string) </returns>
  public string id_usuario_modificador
  {  get { return this.a_id_usuario_modificador; }
     set { this.a_id_usuario_modificador = value; }  }

  /// <summary> Funci�n para asignar o retorna el valor id_usuario_modificador sin parametros </summary>
  /// <returns> Devuelve una cadena de caracteres (string) </returns>
  public Usuario_sistema usuario_modificador
  {  get { return this.a_usuario_modificador; }
     set { this.a_usuario_modificador = value; }  }

  /// <summary> Funci�n para asignar o retorna el valor fecha_anulacion_as sin parametros </summary>
  /// <returns> Devuelve una cadena de caracteres (string) </returns>
  public string fecha_anulacion_as
  {  get { return this.a_fecha_anulacion_as; }
     set { this.a_fecha_anulacion_as = value; }  }

  /// <summary> Funci�n para asignar o retorna el valor id_usuario_anulador sin parametros </summary>
  /// <returns> Devuelve una cadena de caracteres (string) </returns>
  public string id_usuario_anulador
  {  get { return this.a_id_usuario_anulador; }
     set { this.a_id_usuario_anulador = value; }  }

  /// <summary> Funci�n para asignar o retorna el valor id_usuario_anulador sin parametros </summary>
  /// <returns> Devuelve una cadena de caracteres (string) </returns>
  public Usuario_sistema usuario_anulador
  {  get { return this.a_usuario_anulador; }
     set { this.a_usuario_anulador = value; }  }


} /** FIN DE LA CLASE AS_DETALLE_LINEA */
