/**::::::::::::::::::::::::::::::::::::::::::
 *
 * Clase: OBJETO usuario_sistema
 * @author Gabriel Gutierrez Carcamo <gabriel.gutierrez@emfe.cl>
 * @copyright Sistema de Registro de Equipos (c) 2018
 * @version 0.1 lunes, 19 de marzo de 2018 (10:44 Hr.)
 * 
 * :::::::::::::::::::::::::::::::::::::::::::*/
 
/// <summary>Define un objeto  usuario_sistema que permite almacenarlo en la memoria.</summary>

using System;

public class Usuario_sistema
{
  private string a_id_usuario;
  private string a_id_tipo_usuario;
  private string a_id_area_usuario;
  private string a_id_localizacion_holding;
  private string a_id_empresa_holding;
  private string a_nombre;
  private string a_apellido_paterno;
  private string a_apellido_materno;
  private string a_fono;
  private string a_anexo;
  private string a_clave;
  private string a_pregunta;
  private string a_respuesta;
  private string a_correo;
  private string a_fecha_activa;
  private string a_fecha_caduca;
  private string a_rut;
  private string a_usuario_portal_agendamiento;
  private string a_fecha_creacion;
  private string a_usuario_collahuasi;

  /** atributos objetos */
  private Tipo_usuario a_tipo_usuario;
  private Area_usuario a_area_usuario;
  private Localizacion a_localizacion_holding;
  private Empresa a_empresa_holding;


/** ------------- SE INICIA EL CONSTRUCTOR DE LA CLASE -----------*/
  public Usuario_sistema(string p_id_usuario, string p_id_tipo_usuario, string p_id_area_usuario, string p_id_localizacion_holding, string p_id_empresa_holding, string p_nombre, string p_apellido_paterno, string p_apellido_materno, string p_fono, string p_anexo, string p_clave, string p_pregunta, string p_respuesta, string p_correo, string p_fecha_activa, string p_fecha_caduca, string p_rut, string p_usuario_portal_agendamiento, string p_fecha_creacion, string p_usuario_collahuasi )
  {
    this.a_id_usuario = p_id_usuario;
    this.a_id_tipo_usuario = p_id_tipo_usuario;
    this.a_id_area_usuario = p_id_area_usuario;
    this.a_id_localizacion_holding = p_id_localizacion_holding;
    this.a_id_empresa_holding = p_id_empresa_holding;
    this.a_nombre = p_nombre;
    this.a_apellido_paterno = p_apellido_paterno;
    this.a_apellido_materno = p_apellido_materno;
    this.a_fono = p_fono;
    this.a_anexo = p_anexo;
    this.a_clave = p_clave;
    this.a_pregunta = p_pregunta;
    this.a_respuesta = p_respuesta;
    this.a_correo = p_correo;
    this.a_fecha_activa = p_fecha_activa;
    this.a_fecha_caduca = p_fecha_caduca;
    this.a_rut = p_rut;
    this.a_usuario_portal_agendamiento = p_usuario_portal_agendamiento;
    this.a_fecha_creacion = p_fecha_creacion;
    this.a_usuario_collahuasi = p_usuario_collahuasi;

  }
/** --------------------------------------------------------------*/

/** ------------- SE INICIA EL CONSTRUCTOR DE LA CLASE -----------*/
  public Usuario_sistema() { }
/** --------------------------------------------------------------*/

/** -------- METODO QUE SE INICIA AL ELIMINAR EL OBJETO ----------*/
  ~Usuario_sistema() { }
/** --------------------------------------------------------------*/


  /// <summary> Funci�n para asignar o retorna el valor id_usuario sin parametros </summary>
  /// <returns> Devuelve una cadena de caracteres (string) </returns>
  public string id_usuario
  {  get { return this.a_id_usuario; }
     set { this.a_id_usuario = value; }  }

  /// <summary> Funci�n para asignar o retorna el valor id_tipo_usuario sin parametros </summary>
  /// <returns> Devuelve una cadena de caracteres (string) </returns>
  public string id_tipo_usuario
  {  get { return this.a_id_tipo_usuario; }
     set { this.a_id_tipo_usuario = value; }  }

  /// <summary> Funci�n para asignar o retorna el valor id_tipo_usuario sin parametros </summary>
  /// <returns> Devuelve una cadena de caracteres (string) </returns>
  public Tipo_usuario tipo_usuario
  {  get { return this.a_tipo_usuario; }
     set { this.a_tipo_usuario = value; }  }

  /// <summary> Funci�n para asignar o retorna el valor id_area_usuario sin parametros </summary>
  /// <returns> Devuelve una cadena de caracteres (string) </returns>
  public string id_area_usuario
  {  get { return this.a_id_area_usuario; }
     set { this.a_id_area_usuario = value; }  }

  /// <summary> Funci�n para asignar o retorna el valor id_area_usuario sin parametros </summary>
  /// <returns> Devuelve una cadena de caracteres (string) </returns>
  public Area_usuario area_usuario
  {  get { return this.a_area_usuario; }
     set { this.a_area_usuario = value; }  }

  /// <summary> Funci�n para asignar o retorna el valor id_localizacion_holding sin parametros </summary>
  /// <returns> Devuelve una cadena de caracteres (string) </returns>
  public string id_localizacion_holding
  {  get { return this.a_id_localizacion_holding; }
     set { this.a_id_localizacion_holding = value; }  }

  /// <summary> Funci�n para asignar o retorna el valor id_localizacion_holding sin parametros </summary>
  /// <returns> Devuelve una cadena de caracteres (string) </returns>
  public Localizacion localizacion_holding
  {  get { return this.a_localizacion_holding; }
     set { this.a_localizacion_holding = value; }  }

  /// <summary> Funci�n para asignar o retorna el valor id_empresa_holding sin parametros </summary>
  /// <returns> Devuelve una cadena de caracteres (string) </returns>
  public string id_empresa_holding
  {  get { return this.a_id_empresa_holding; }
     set { this.a_id_empresa_holding = value; }  }

  /// <summary> Funci�n para asignar o retorna el valor id_empresa_holding sin parametros </summary>
  /// <returns> Devuelve una cadena de caracteres (string) </returns>
  public Empresa empresa_holding
  {  get { return this.a_empresa_holding; }
     set { this.a_empresa_holding = value; }  }

  /// <summary> Funci�n para asignar o retorna el valor nombre sin parametros </summary>
  /// <returns> Devuelve una cadena de caracteres (string) </returns>
  public string nombre
  {  get { return this.a_nombre; }
     set { this.a_nombre = value; }  }

  /// <summary> Funci�n para asignar o retorna el valor apellido_paterno sin parametros </summary>
  /// <returns> Devuelve una cadena de caracteres (string) </returns>
  public string apellido_paterno
  {  get { return this.a_apellido_paterno; }
     set { this.a_apellido_paterno = value; }  }

  /// <summary> Funci�n para asignar o retorna el valor apellido_materno sin parametros </summary>
  /// <returns> Devuelve una cadena de caracteres (string) </returns>
  public string apellido_materno
  {  get { return this.a_apellido_materno; }
     set { this.a_apellido_materno = value; }  }

  /// <summary> Funci�n para asignar o retorna el valor fono sin parametros </summary>
  /// <returns> Devuelve una cadena de caracteres (string) </returns>
  public string fono
  {  get { return this.a_fono; }
     set { this.a_fono = value; }  }

  /// <summary> Funci�n para asignar o retorna el valor anexo sin parametros </summary>
  /// <returns> Devuelve una cadena de caracteres (string) </returns>
  public string anexo
  {  get { return this.a_anexo; }
     set { this.a_anexo = value; }  }

  /// <summary> Funci�n para asignar o retorna el valor clave sin parametros </summary>
  /// <returns> Devuelve una cadena de caracteres (string) </returns>
  public string clave
  {  get { return this.a_clave; }
     set { this.a_clave = value; }  }

  /// <summary> Funci�n para asignar o retorna el valor pregunta sin parametros </summary>
  /// <returns> Devuelve una cadena de caracteres (string) </returns>
  public string pregunta
  {  get { return this.a_pregunta; }
     set { this.a_pregunta = value; }  }

  /// <summary> Funci�n para asignar o retorna el valor respuesta sin parametros </summary>
  /// <returns> Devuelve una cadena de caracteres (string) </returns>
  public string respuesta
  {  get { return this.a_respuesta; }
     set { this.a_respuesta = value; }  }

  /// <summary> Funci�n para asignar o retorna el valor correo sin parametros </summary>
  /// <returns> Devuelve una cadena de caracteres (string) </returns>
  public string correo
  {  get { return this.a_correo; }
     set { this.a_correo = value; }  }

  /// <summary> Funci�n para asignar o retorna el valor fecha_activa sin parametros </summary>
  /// <returns> Devuelve una cadena de caracteres (string) </returns>
  public string fecha_activa
  {  get { return this.a_fecha_activa; }
     set { this.a_fecha_activa = value; }  }

  /// <summary> Funci�n para asignar o retorna el valor fecha_caduca sin parametros </summary>
  /// <returns> Devuelve una cadena de caracteres (string) </returns>
  public string fecha_caduca
  {  get { return this.a_fecha_caduca; }
     set { this.a_fecha_caduca = value; }  }

  /// <summary> Funci�n para asignar o retorna el valor rut sin parametros </summary>
  /// <returns> Devuelve una cadena de caracteres (string) </returns>
  public string rut
  {  get { return this.a_rut; }
     set { this.a_rut = value; }  }

  /// <summary> Funci�n para asignar o retorna el valor usuario_portal_agendamiento sin parametros </summary>
  /// <returns> Devuelve una cadena de caracteres (string) </returns>
  public string usuario_portal_agendamiento
  {  get { return this.a_usuario_portal_agendamiento; }
     set { this.a_usuario_portal_agendamiento = value; }  }

  /// <summary> Funci�n para asignar o retorna el valor fecha_creacion sin parametros </summary>
  /// <returns> Devuelve una cadena de caracteres (string) </returns>
  public string fecha_creacion
  {  get { return this.a_fecha_creacion; }
     set { this.a_fecha_creacion = value; }  }

  /// <summary> Funci�n para asignar o retorna el valor usuario_collahuasi sin parametros </summary>
  /// <returns> Devuelve una cadena de caracteres (string) </returns>
  public string usuario_collahuasi
  {  get { return this.a_usuario_collahuasi; }
     set { this.a_usuario_collahuasi = value; }  }


} /** FIN DE LA CLASE USUARIO_SISTEMA */
