/**::::::::::::::::::::::::::::::::::::::::::
 *
 * Clase: OBJETO as_momentanea
 * @author Gabriel Gutierrez Carcamo <gabriel.gutierrez@emfe.cl>
 * @copyright Sistema de Registro de Equipos (c) 2018
 * @version 0.1 lunes, 19 de marzo de 2018 (10:29 Hr.)
 * 
 * :::::::::::::::::::::::::::::::::::::::::::*/
 
/// <summary>Define un objeto  as_momentanea que permite almacenarlo en la memoria.</summary>

using System;

public class As_momentanea
{
  private string a_id_momentanea;
  private string a_fecha_vencimiento;
  private string a_valido;


/** ------------- SE INICIA EL CONSTRUCTOR DE LA CLASE -----------*/
  public As_momentanea(string p_id_momentanea, string p_fecha_vencimiento, string p_valido )
  {
    this.a_id_momentanea = p_id_momentanea;
    this.a_fecha_vencimiento = p_fecha_vencimiento;
    this.a_valido = p_valido;

  }
/** --------------------------------------------------------------*/

/** ------------- SE INICIA EL CONSTRUCTOR DE LA CLASE -----------*/
  public As_momentanea() { }
/** --------------------------------------------------------------*/

/** -------- METODO QUE SE INICIA AL ELIMINAR EL OBJETO ----------*/
  ~As_momentanea() { }
/** --------------------------------------------------------------*/


  /// <summary> Funci�n para asignar o retorna el valor id_momentanea sin parametros </summary>
  /// <returns> Devuelve una cadena de caracteres (string) </returns>
  public string id_momentanea
  {  get { return this.a_id_momentanea; }
     set { this.a_id_momentanea = value; }  }

  /// <summary> Funci�n para asignar o retorna el valor fecha_vencimiento sin parametros </summary>
  /// <returns> Devuelve una cadena de caracteres (string) </returns>
  public string fecha_vencimiento
  {  get { return this.a_fecha_vencimiento; }
     set { this.a_fecha_vencimiento = value; }  }

  /// <summary> Funci�n para asignar o retorna el valor valido sin parametros </summary>
  /// <returns> Devuelve una cadena de caracteres (string) </returns>
  public string valido
  {  get { return this.a_valido; }
     set { this.a_valido = value; }  }


} /** FIN DE LA CLASE AS_MOMENTANEA */
