/**::::::::::::::::::::::::::::::::::::::::::
 *
 * Clase: Lógica para el objeto tipo_usuario
 * @author Gabriel Gutierrez Carcamo <gabriel.gutierrez@emfe.cl>
 * @category Lógica de la aplicación
 * @copyright Sistema de Registro de Equipos (c) 2018
 * @copyright Administra información de las asignaciones de equipos a los usuarios.
 * @version 0.1 lunes, 19 de marzo de 2018 (11:35 Hr.)
 * 
 * :::::::::::::::::::::::::::::::::::::::::::*/

using System.Collections;

/// <summary>Implementa la lógica del negocio para el objeto tipo_usuario donde es responsable la clase sistema/// </summary>



public class Log_sistema_tipo_usuario
{
    public Log_sistema_tipo_usuario() {  }

  /** ------- Destructor que se inicia al liberar un objeto --------*/
    ~Log_sistema_tipo_usuario() { }
  /** --------------------------------------------------------------*/


  /// <summary>Ingresar un objeto tipo_usuario a la base de datos.</summary>
  /// <param name="p_tipo_usuario" >objeto </param>
  /// <returns> Devuelve un mensaje con información para el usuario. </returns>
  public string ingresar_tipo_usuario(Tipo_usuario p_tipo_usuario)
  {
     Fac_sistema_tipo_usuario m_fachada = new Fac_sistema_tipo_usuario("");
     m_fachada.conectar();
     string m_mensaje = "Error: Al tratar de ingresar la información";

     if (!m_fachada.existe_tipo_usuario(p_tipo_usuario))
     {
       int m_respuesta = m_fachada.ingresar_tipo_usuario(p_tipo_usuario);
  
       if (m_respuesta > 0)
             m_mensaje = "Ok, Se han ingresado los datos al sistema";
       else
             m_mensaje = m_fachada.get_error();
     }
     else
           m_mensaje = "Error: Ya existe un tipo_usuario con msimo datos en el sistema.";

     m_fachada.cerrar();

     return(m_mensaje);
  }


  /// <summary>Modificar un objeto tipo_usuario a la base de datos.</summary>
  /// <param name="p_tipo_usuario" >objeto </param>
  /// <returns> Devuelve un mensaje con información para el usuario. </returns>
  public string modificar_tipo_usuario(Tipo_usuario p_tipo_usuario)
  {
     Fac_sistema_tipo_usuario m_fachada = new Fac_sistema_tipo_usuario("");
     m_fachada.conectar();
     string m_mensaje = "Error: Al tratar de modificar la información";

     int m_respuesta = m_fachada.modificar_tipo_usuario(p_tipo_usuario);

     if (m_respuesta > 0) m_mensaje = "Ok, su información ha sido modificada en el sistema";
     else m_mensaje = "Error: " +  m_fachada.get_error();

     m_fachada.cerrar();

     return(m_mensaje);
  }


  /// <summary>Obtiene un objeto tipo_usuario a la base de datos.</summary>
  public Tipo_usuario get_tipo_usuario(string p_id_tipo_usuario)
  {
     Fac_sistema_tipo_usuario m_fachada = new Fac_sistema_tipo_usuario("");
     m_fachada.conectar();

     Tipo_usuario m_tipo_usuario = m_fachada.get_tipo_usuario(p_id_tipo_usuario);
     m_fachada.cerrar();

     return(m_tipo_usuario);

  }

  /// <summary>Retorna una lista de codigo primarios para los objetos tipo_usuario desde la base de datos</summary>
  /// <param name="p_tipo_usuario" >objeto </param>
  /// <returns> Devuelve un objeto tipo_usuario, es caso contrario null</returns>
  public Lista  get_lista_tipo_usuarios(Lista p_lista)
  {
     Fac_sistema_tipo_usuario m_fachada = new Fac_sistema_tipo_usuario("");
     m_fachada.conectar();

     ArrayList m_lista =  m_fachada.get_lista_tipo_usuarios(p_lista);
     int m_largo = m_lista.Count;

     for (int i = 0; i < m_largo; i++)
     {
        string m_clave = (string)m_lista[i];
        Tipo_usuario m_tipo_usuario = m_fachada.get_tipo_usuario(m_clave);

        p_lista.ingresar(m_tipo_usuario);
     }

     m_fachada.cerrar();
     return (p_lista);
  }

} // FIN DE LA CLASE LOGICA LOG_SISTEMA_TIPO_USUARIO */
