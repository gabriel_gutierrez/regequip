/**::::::::::::::::::::::::::::::::::::::::::
 *
 * Clase: Lógica para el objeto localizacion
 * @author Gabriel Gutierrez Carcamo <gabriel.gutierrez@emfe.cl>
 * @category Lógica de la aplicación
 * @copyright Sistema de Registro de Equipos (c) 2018
 * @copyright Administra información de las asignaciones de equipos a los usuarios.
 * @version 0.1 lunes, 19 de marzo de 2018 (12:05 Hr.)
 * 
 * :::::::::::::::::::::::::::::::::::::::::::*/

using System.Collections;

/// <summary>Implementa la lógica del negocio para el objeto localizacion donde es responsable la clase sistema/// </summary>



public class Log_sistema_localizacion
{
    public Log_sistema_localizacion() {  }

  /** ------- Destructor que se inicia al liberar un objeto --------*/
    ~Log_sistema_localizacion() { }
  /** --------------------------------------------------------------*/


  /// <summary>Ingresar un objeto localizacion a la base de datos.</summary>
  /// <param name="p_localizacion" >objeto </param>
  /// <returns> Devuelve un mensaje con información para el usuario. </returns>
  public string ingresar_localizacion(Localizacion p_localizacion)
  {
     Fac_sistema_localizacion m_fachada = new Fac_sistema_localizacion("");
     m_fachada.conectar();
     string m_mensaje = "Error: Al tratar de ingresar la información";

     if (!m_fachada.existe_localizacion(p_localizacion))
     {
       int m_respuesta = m_fachada.ingresar_localizacion(p_localizacion);
  
       if (m_respuesta > 0)
             m_mensaje = "Ok, Se han ingresado los datos al sistema";
       else
             m_mensaje = m_fachada.get_error();
     }
     else
           m_mensaje = "Error: Ya existe un localizacion con msimo datos en el sistema.";

     m_fachada.cerrar();

     return(m_mensaje);
  }


  /// <summary>Modificar un objeto localizacion a la base de datos.</summary>
  /// <param name="p_localizacion" >objeto </param>
  /// <returns> Devuelve un mensaje con información para el usuario. </returns>
  public string modificar_localizacion(Localizacion p_localizacion)
  {
     Fac_sistema_localizacion m_fachada = new Fac_sistema_localizacion("");
     m_fachada.conectar();
     string m_mensaje = "Error: Al tratar de modificar la información";

     int m_respuesta = m_fachada.modificar_localizacion(p_localizacion);

     if (m_respuesta > 0) m_mensaje = "Ok, su información ha sido modificada en el sistema";
     else m_mensaje = "Error: " +  m_fachada.get_error();

     m_fachada.cerrar();

     return(m_mensaje);
  }


  /// <summary>Obtiene un objeto localizacion a la base de datos.</summary>
  public Localizacion get_localizacion(string p_id_localizacion)
  {
     Fac_sistema_localizacion m_fachada = new Fac_sistema_localizacion("");
     m_fachada.conectar();

     Localizacion m_localizacion = m_fachada.get_localizacion(p_id_localizacion);
     m_fachada.cerrar();

     return(m_localizacion);

  }

  /// <summary>Retorna una lista de codigo primarios para los objetos localizacion desde la base de datos</summary>
  /// <param name="p_localizacion" >objeto </param>
  /// <returns> Devuelve un objeto localizacion, es caso contrario null</returns>
  public Lista  get_lista_localizaciones(Lista p_lista)
  {
     Fac_sistema_localizacion m_fachada = new Fac_sistema_localizacion("");
     m_fachada.conectar();

     ArrayList m_lista =  m_fachada.get_lista_localizaciones(p_lista);
     int m_largo = m_lista.Count;

     for (int i = 0; i < m_largo; i++)
     {
        string m_clave = (string)m_lista[i];
        Localizacion m_localizacion = m_fachada.get_localizacion(m_clave);

        p_lista.ingresar(m_localizacion);
     }

     m_fachada.cerrar();
     return (p_lista);
  }

} // FIN DE LA CLASE LOGICA LOG_SISTEMA_LOCALIZACION */
