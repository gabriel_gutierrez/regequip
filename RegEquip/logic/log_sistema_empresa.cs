/**::::::::::::::::::::::::::::::::::::::::::
 *
 * Clase: Lógica para el objeto empresa
 * @author Gabriel Gutierrez Carcamo <gabriel.gutierrez@emfe.cl>
 * @category Lógica de la aplicación
 * @copyright Sistema de Registro de Equipos (c) 2018
 * @copyright Administra información de las asignaciones de equipos a los usuarios.
 * @version 0.1 lunes, 19 de marzo de 2018 (12:10 Hr.)
 * 
 * :::::::::::::::::::::::::::::::::::::::::::*/

using System.Collections;

/// <summary>Implementa la lógica del negocio para el objeto empresa donde es responsable la clase sistema/// </summary>



public class Log_sistema_empresa
{
    public Log_sistema_empresa() {  }

  /** ------- Destructor que se inicia al liberar un objeto --------*/
    ~Log_sistema_empresa() { }
  /** --------------------------------------------------------------*/


  /// <summary>Ingresar un objeto empresa a la base de datos.</summary>
  /// <param name="p_empresa" >objeto </param>
  /// <returns> Devuelve un mensaje con información para el usuario. </returns>
  public string ingresar_empresa(Empresa p_empresa)
  {
     Fac_sistema_empresa m_fachada = new Fac_sistema_empresa("");
     m_fachada.conectar();
     string m_mensaje = "Error: Al tratar de ingresar la información";

     if (!m_fachada.existe_empresa(p_empresa))
     {
       int m_respuesta = m_fachada.ingresar_empresa(p_empresa);
  
       if (m_respuesta > 0)
             m_mensaje = "Ok, Se han ingresado los datos al sistema";
       else
             m_mensaje = m_fachada.get_error();
     }
     else
           m_mensaje = "Error: Ya existe un empresa con msimo datos en el sistema.";

     m_fachada.cerrar();

     return(m_mensaje);
  }


  /// <summary>Modificar un objeto empresa a la base de datos.</summary>
  /// <param name="p_empresa" >objeto </param>
  /// <returns> Devuelve un mensaje con información para el usuario. </returns>
  public string modificar_empresa(Empresa p_empresa)
  {
     Fac_sistema_empresa m_fachada = new Fac_sistema_empresa("");
     m_fachada.conectar();
     string m_mensaje = "Error: Al tratar de modificar la información";

     int m_respuesta = m_fachada.modificar_empresa(p_empresa);

     if (m_respuesta > 0) m_mensaje = "Ok, su información ha sido modificada en el sistema";
     else m_mensaje = "Error: " +  m_fachada.get_error();

     m_fachada.cerrar();

     return(m_mensaje);
  }


  /// <summary>Obtiene un objeto empresa a la base de datos.</summary>
  public Empresa get_empresa(string p_id_empresa)
  {
     Fac_sistema_empresa m_fachada = new Fac_sistema_empresa("");
     m_fachada.conectar();

     Empresa m_empresa = m_fachada.get_empresa(p_id_empresa);
     m_fachada.cerrar();

     return(m_empresa);

  }

  /// <summary>Retorna una lista de codigo primarios para los objetos empresa desde la base de datos</summary>
  /// <param name="p_empresa" >objeto </param>
  /// <returns> Devuelve un objeto empresa, es caso contrario null</returns>
  public Lista  get_lista_empresas(Lista p_lista)
  {
     Fac_sistema_empresa m_fachada = new Fac_sistema_empresa("");
     m_fachada.conectar();

     ArrayList m_lista =  m_fachada.get_lista_empresas(p_lista);
     int m_largo = m_lista.Count;

     for (int i = 0; i < m_largo; i++)
     {
        string m_clave = (string)m_lista[i];
        Empresa m_empresa = m_fachada.get_empresa(m_clave);

        p_lista.ingresar(m_empresa);
     }

     m_fachada.cerrar();
     return (p_lista);
  }

} // FIN DE LA CLASE LOGICA LOG_SISTEMA_EMPRESA */
