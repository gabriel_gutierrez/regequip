/**::::::::::::::::::::::::::::::::::::::::::
 *
 * Clase: OBJETO as_asignacion
 * @author Gabriel Gutierrez Carcamo <gabriel.gutierrez@emfe.cl>
 * @copyright Sistema de Registro de Equipos (c) 2018
 * @version 0.1 lunes, 19 de marzo de 2018 (10:33 Hr.)
 * 
 * :::::::::::::::::::::::::::::::::::::::::::*/

/// <summary>Define un objeto  as_asignacion que permite almacenarlo en la memoria.</summary>

using System;

public class As_asignacion
{
    private string a_id_asignacion;
    private string a_folio;
    private string a_id_usuario_asignado;
    private string a_fecha_asignacion;
    private string a_fecha_recepcion;
    private string a_id_equipo;
    private string a_id_estado;
    private string a_id_momentanea;
    private string a_observacion;
    private string a_id_detalle_linea;
    private string a_id_checklist;
    private string a_valido;

    /** atributos objetos */
    private Usuario_sistema a_usuario_asignado;
    private As_equipo a_equipo;
    private As_estado a_estado;
    private As_momentanea a_momentanea;
    private As_detalle_linea a_detalle_linea;
    private As_checklist a_checklist;


    /** ------------- SE INICIA EL CONSTRUCTOR DE LA CLASE -----------*/
    public As_asignacion(string p_id_asignacion, string p_folio, string p_id_usuario_asignado, string p_fecha_asignacion, string p_fecha_recepcion, string p_id_equipo, string p_id_estado, string p_id_momentanea, string p_observacion, string p_id_detalle_linea, string p_id_checklist, string p_valido)
    {
        this.a_id_asignacion = p_id_asignacion;
        this.a_folio = p_folio;
        this.a_id_usuario_asignado = p_id_usuario_asignado;
        this.a_fecha_asignacion = p_fecha_asignacion;
        this.a_fecha_recepcion = p_fecha_recepcion;
        this.a_id_equipo = p_id_equipo;
        this.a_id_estado = p_id_estado;
        this.a_id_momentanea = p_id_momentanea;
        this.a_observacion = p_observacion;
        this.a_id_detalle_linea = p_id_detalle_linea;
        this.a_id_checklist = p_id_checklist;
        this.a_valido = p_valido;

    }
    /** --------------------------------------------------------------*/

    /** ------------- SE INICIA EL CONSTRUCTOR DE LA CLASE -----------*/
    public As_asignacion() { }
    /** --------------------------------------------------------------*/

    /** -------- METODO QUE SE INICIA AL ELIMINAR EL OBJETO ----------*/
    ~As_asignacion() { }
    /** --------------------------------------------------------------*/


    /// <summary> Funci�n para asignar o retorna el valor id_asignacion sin parametros </summary>
    /// <returns> Devuelve una cadena de caracteres (string) </returns>
    public string id_asignacion
    {
        get { return this.a_id_asignacion; }
        set { this.a_id_asignacion = value; }
    }

    /// <summary> Funci�n para asignar o retorna el valor folio sin parametros </summary>
    /// <returns> Devuelve una cadena de caracteres (string) </returns>
    public string folio
    {
        get { return this.a_folio; }
        set { this.a_folio = value; }
    }

    /// <summary> Funci�n para asignar o retorna el valor id_usuario_asignado sin parametros </summary>
    /// <returns> Devuelve una cadena de caracteres (string) </returns>
    public string id_usuario_asignado
    {
        get { return this.a_id_usuario_asignado; }
        set { this.a_id_usuario_asignado = value; }
    }

    /// <summary> Funci�n para asignar o retorna el valor id_usuario_asignado sin parametros </summary>
    /// <returns> Devuelve una cadena de caracteres (string) </returns>
    public Usuario_sistema usuario_asignado
    {
        get { return this.a_usuario_asignado; }
        set { this.a_usuario_asignado = value; }
    }

    /// <summary> Funci�n para asignar o retorna el valor fecha_asignacion sin parametros </summary>
    /// <returns> Devuelve una cadena de caracteres (string) </returns>
    public string fecha_asignacion
    {
        get { return this.a_fecha_asignacion; }
        set { this.a_fecha_asignacion = value; }
    }

    /// <summary> Funci�n para asignar o retorna el valor fecha_recepcion sin parametros </summary>
    /// <returns> Devuelve una cadena de caracteres (string) </returns>
    public string fecha_recepcion
    {
        get { return this.a_fecha_recepcion; }
        set { this.a_fecha_recepcion = value; }
    }

    /// <summary> Funci�n para asignar o retorna el valor id_equipo sin parametros </summary>
    /// <returns> Devuelve una cadena de caracteres (string) </returns>
    public string id_equipo
    {
        get { return this.a_id_equipo; }
        set { this.a_id_equipo = value; }
    }

    /// <summary> Funci�n para asignar o retorna el valor id_equipo sin parametros </summary>
    /// <returns> Devuelve una cadena de caracteres (string) </returns>
    public As_equipo equipo
    {
        get { return this.a_equipo; }
        set { this.a_equipo = value; }
    }

    /// <summary> Funci�n para asignar o retorna el valor id_estado sin parametros </summary>
    /// <returns> Devuelve una cadena de caracteres (string) </returns>
    public string id_estado
    {
        get { return this.a_id_estado; }
        set { this.a_id_estado = value; }
    }

    /// <summary> Funci�n para asignar o retorna el valor id_estado sin parametros </summary>
    /// <returns> Devuelve una cadena de caracteres (string) </returns>
    public As_estado estado
    {
        get { return this.a_estado; }
        set { this.a_estado = value; }
    }

    /// <summary> Funci�n para asignar o retorna el valor id_momentanea sin parametros </summary>
    /// <returns> Devuelve una cadena de caracteres (string) </returns>
    public string id_momentanea
    {
        get { return this.a_id_momentanea; }
        set { this.a_id_momentanea = value; }
    }

    /// <summary> Funci�n para asignar o retorna el valor id_momentanea sin parametros </summary>
    /// <returns> Devuelve una cadena de caracteres (string) </returns>
    public As_momentanea momentanea
    {
        get { return this.a_momentanea; }
        set { this.a_momentanea = value; }
    }

    /// <summary> Funci�n para asignar o retorna el valor observacion sin parametros </summary>
    /// <returns> Devuelve una cadena de caracteres (string) </returns>
    public string observacion
    {
        get { return this.a_observacion; }
        set { this.a_observacion = value; }
    }

    /// <summary> Funci�n para asignar o retorna el valor id_detalle_linea sin parametros </summary>
    /// <returns> Devuelve una cadena de caracteres (string) </returns>
    public string id_detalle_linea
    {
        get { return this.a_id_detalle_linea; }
        set { this.a_id_detalle_linea = value; }
    }

    /// <summary> Funci�n para asignar o retorna el valor id_detalle_linea sin parametros </summary>
    /// <returns> Devuelve una cadena de caracteres (string) </returns>
    public As_detalle_linea detalle_linea
    {
        get { return this.a_detalle_linea; }
        set { this.a_detalle_linea = value; }
    }

    /// <summary> Funci�n para asignar o retorna el valor checklist sin parametros </summary>
    /// <returns> Devuelve una cadena de caracteres (string) </returns>
    public string id_checklist
    {
        get { return this.a_id_checklist; }
        set { this.a_id_checklist = value; }
    }

    /// <summary> Funci�n para asignar o retorna el valor checklist sin parametros </summary>
    /// <returns> Devuelve una cadena de caracteres (string) </returns>
    public As_checklist checklist
    {
        get { return this.a_checklist; }
        set { this.a_checklist = value; }
    }

    /// <summary> Funci�n para asignar o retorna el valor valido sin parametros </summary>
    /// <returns> Devuelve una cadena de caracteres (string) </returns>
    public string valido
    {
        get { return this.a_valido; }
        set { this.a_valido = value; }
    }


} /** FIN DE LA CLASE AS_ASIGNACION */
