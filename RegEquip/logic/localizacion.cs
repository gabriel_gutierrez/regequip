/**::::::::::::::::::::::::::::::::::::::::::
 *
 * Clase: OBJETO localizacion
 * @author Gabriel Gutierrez Carcamo <gabriel.gutierrez@emfe.cl>
 * @copyright Sistema de Registro de Equipos (c) 2018
 * @version 0.1 lunes, 19 de marzo de 2018 (12:06 Hr.)
 * 
 * :::::::::::::::::::::::::::::::::::::::::::*/
 
/// <summary>Define un objeto  localizacion que permite almacenarlo en la memoria.</summary>

using System;

public class Localizacion
{
  private string a_id_localizacion;
  private string a_id_empresa;
  private string a_id_ciudad;
  private string a_id_estado_localizacion;
  private string a_descripcion;
  private string a_telefono;
  private string a_direccion;
  private string a_coordenadas_geo;
  private string a_id_localizacion_GPS;

  /** atributos objetos */
  private Empresa a_empresa;


/** ------------- SE INICIA EL CONSTRUCTOR DE LA CLASE -----------*/
  public Localizacion(string p_id_localizacion, string p_id_empresa, string p_id_ciudad, string p_id_estado_localizacion, string p_descripcion, string p_telefono, string p_direccion, string p_coordenadas_geo, string p_id_localizacion_GPS )
  {
    this.a_id_localizacion = p_id_localizacion;
    this.a_id_empresa = p_id_empresa;
    this.a_id_ciudad = p_id_ciudad;
    this.a_id_estado_localizacion = p_id_estado_localizacion;
    this.a_descripcion = p_descripcion;
    this.a_telefono = p_telefono;
    this.a_direccion = p_direccion;
    this.a_coordenadas_geo = p_coordenadas_geo;
    this.a_id_localizacion_GPS = p_id_localizacion_GPS;

  }
/** --------------------------------------------------------------*/

/** ------------- SE INICIA EL CONSTRUCTOR DE LA CLASE -----------*/
  public Localizacion() { }
/** --------------------------------------------------------------*/

/** -------- METODO QUE SE INICIA AL ELIMINAR EL OBJETO ----------*/
  ~Localizacion() { }
/** --------------------------------------------------------------*/


  /// <summary> Funci�n para asignar o retorna el valor id_localizacion sin parametros </summary>
  /// <returns> Devuelve una cadena de caracteres (string) </returns>
  public string id_localizacion
  {  get { return this.a_id_localizacion; }
     set { this.a_id_localizacion = value; }  }

  /// <summary> Funci�n para asignar o retorna el valor id_empresa sin parametros </summary>
  /// <returns> Devuelve una cadena de caracteres (string) </returns>
  public string id_empresa
  {  get { return this.a_id_empresa; }
     set { this.a_id_empresa = value; }  }

  /// <summary> Funci�n para asignar o retorna el valor id_empresa sin parametros </summary>
  /// <returns> Devuelve una cadena de caracteres (string) </returns>
  public Empresa empresa
  {  get { return this.a_empresa; }
     set { this.a_empresa = value; }  }

  /// <summary> Funci�n para asignar o retorna el valor id_ciudad sin parametros </summary>
  /// <returns> Devuelve una cadena de caracteres (string) </returns>
  public string id_ciudad
  {  get { return this.a_id_ciudad; }
     set { this.a_id_ciudad = value; }  }
    
  /// <summary> Funci�n para asignar o retorna el valor id_estado_localizacion sin parametros </summary>
  /// <returns> Devuelve una cadena de caracteres (string) </returns>
  public string id_estado_localizacion
  {  get { return this.a_id_estado_localizacion; }
     set { this.a_id_estado_localizacion = value; }  }
    
  /// <summary> Funci�n para asignar o retorna el valor descripcion sin parametros </summary>
  /// <returns> Devuelve una cadena de caracteres (string) </returns>
  public string descripcion
  {  get { return this.a_descripcion; }
     set { this.a_descripcion = value; }  }

  /// <summary> Funci�n para asignar o retorna el valor telefono sin parametros </summary>
  /// <returns> Devuelve una cadena de caracteres (string) </returns>
  public string telefono
  {  get { return this.a_telefono; }
     set { this.a_telefono = value; }  }

  /// <summary> Funci�n para asignar o retorna el valor direccion sin parametros </summary>
  /// <returns> Devuelve una cadena de caracteres (string) </returns>
  public string direccion
  {  get { return this.a_direccion; }
     set { this.a_direccion = value; }  }

  /// <summary> Funci�n para asignar o retorna el valor coordenadas_geo sin parametros </summary>
  /// <returns> Devuelve una cadena de caracteres (string) </returns>
  public string coordenadas_geo
  {  get { return this.a_coordenadas_geo; }
     set { this.a_coordenadas_geo = value; }  }

  /// <summary> Funci�n para asignar o retorna el valor id_localizacion_GPS sin parametros </summary>
  /// <returns> Devuelve una cadena de caracteres (string) </returns>
  public string id_localizacion_GPS
  {  get { return this.a_id_localizacion_GPS; }
     set { this.a_id_localizacion_GPS = value; }  }


} /** FIN DE LA CLASE LOCALIZACION */
