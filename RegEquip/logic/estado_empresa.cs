/**::::::::::::::::::::::::::::::::::::::::::
 *
 * Clase: OBJETO estado_empresa
 * @author Gabriel Gutierrez Carcamo <gabriel.gutierrez@emfe.cl>
 * @copyright Sistema de Registro de Equipos (c) 2018
 * @version 0.1 lunes, 19 de marzo de 2018 (12:15 Hr.)
 * 
 * :::::::::::::::::::::::::::::::::::::::::::*/
 
/// <summary>Define un objeto  estado_empresa que permite almacenarlo en la memoria.</summary>

using System;

public class Estado_empresa
{
  private string a_id_estado;
  private string a_descripcion;


/** ------------- SE INICIA EL CONSTRUCTOR DE LA CLASE -----------*/
  public Estado_empresa(string p_id_estado, string p_descripcion )
  {
    this.a_id_estado = p_id_estado;
    this.a_descripcion = p_descripcion;

  }
/** --------------------------------------------------------------*/

/** ------------- SE INICIA EL CONSTRUCTOR DE LA CLASE -----------*/
  public Estado_empresa() { }
/** --------------------------------------------------------------*/

/** -------- METODO QUE SE INICIA AL ELIMINAR EL OBJETO ----------*/
  ~Estado_empresa() { }
/** --------------------------------------------------------------*/


  /// <summary> Funci�n para asignar o retorna el valor id_estado sin parametros </summary>
  /// <returns> Devuelve una cadena de caracteres (string) </returns>
  public string id_estado
  {  get { return this.a_id_estado; }
     set { this.a_id_estado = value; }  }

  /// <summary> Funci�n para asignar o retorna el valor descripcion sin parametros </summary>
  /// <returns> Devuelve una cadena de caracteres (string) </returns>
  public string descripcion
  {  get { return this.a_descripcion; }
     set { this.a_descripcion = value; }  }


} /** FIN DE LA CLASE ESTADO_EMPRESA */
