/**::::::::::::::::::::::::::::::::::::::::::
 *
 * Clase: OBJETO as_marca
 * @author Gabriel Gutierrez Carcamo <gabriel.gutierrez@emfe.cl>
 * @copyright Sistema de Registro de Equipos (c) 2018
 * @version 0.1 lunes, 19 de marzo de 2018 (10:31 Hr.)
 * 
 * :::::::::::::::::::::::::::::::::::::::::::*/
 
/// <summary>Define un objeto  as_marca que permite almacenarlo en la memoria.</summary>

using System;

public class As_marca
{
  private string a_id_marca;
  private string a_descripcion_marca;
  private string a_id_detalle_linea;
  private string a_valido;

  /** atributos objetos */
  private As_detalle_linea a_detalle_linea;


/** ------------- SE INICIA EL CONSTRUCTOR DE LA CLASE -----------*/
  public As_marca(string p_id_marca, string p_descripcion_marca, string p_id_detalle_linea, string p_valido )
  {
    this.a_id_marca = p_id_marca;
    this.a_descripcion_marca = p_descripcion_marca;
    this.a_id_detalle_linea = p_id_detalle_linea;
    this.a_valido = p_valido;

  }
/** --------------------------------------------------------------*/

/** ------------- SE INICIA EL CONSTRUCTOR DE LA CLASE -----------*/
  public As_marca() { }
/** --------------------------------------------------------------*/

/** -------- METODO QUE SE INICIA AL ELIMINAR EL OBJETO ----------*/
  ~As_marca() { }
/** --------------------------------------------------------------*/


  /// <summary> Funci�n para asignar o retorna el valor id_marca sin parametros </summary>
  /// <returns> Devuelve una cadena de caracteres (string) </returns>
  public string id_marca
  {  get { return this.a_id_marca; }
     set { this.a_id_marca = value; }  }

  /// <summary> Funci�n para asignar o retorna el valor descripcion_marca sin parametros </summary>
  /// <returns> Devuelve una cadena de caracteres (string) </returns>
  public string descripcion_marca
  {  get { return this.a_descripcion_marca; }
     set { this.a_descripcion_marca = value; }  }

  /// <summary> Funci�n para asignar o retorna el valor id_detalle_linea sin parametros </summary>
  /// <returns> Devuelve una cadena de caracteres (string) </returns>
  public string id_detalle_linea
  {  get { return this.a_id_detalle_linea; }
     set { this.a_id_detalle_linea = value; }  }

  /// <summary> Funci�n para asignar o retorna el valor id_detalle_linea sin parametros </summary>
  /// <returns> Devuelve una cadena de caracteres (string) </returns>
  public As_detalle_linea detalle_linea
  {  get { return this.a_detalle_linea; }
     set { this.a_detalle_linea = value; }  }

  /// <summary> Funci�n para asignar o retorna el valor valido sin parametros </summary>
  /// <returns> Devuelve una cadena de caracteres (string) </returns>
  public string valido
  {  get { return this.a_valido; }
     set { this.a_valido = value; }  }


} /** FIN DE LA CLASE AS_MARCA */
