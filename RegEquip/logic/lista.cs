﻿using System.Collections;

/// <summary>
/// Descripción breve de lista
/// </summary>
public class Lista
{
    private ArrayList a_lista;  //ARREGLO  DE TEMPORAL  
    private int a_desde;        //INDICA EL NUMERO INICIAL DE ELEMENTOS A MOSTRAR
    private int a_mostrar;      //INDICA EL TOTAL A MOSTRAR
    private int a_hasta;        //NUMERO DE ELEMENTOS A MOSTRAR EN PANTALLA 
    private int a_total;        //INDICA EL TOTAL DE UNA BUSQUEDA

    private ArrayList a_sql_parametros;
    private ArrayList a_sql_valores;  


    public Lista()
	{
      a_lista          = new ArrayList();
      a_sql_parametros = new ArrayList();
      a_sql_valores    = new ArrayList();
	}

    public ArrayList lista
    {
        get { return this.a_lista; }
        set { this.a_lista = value; }
    }

    public int desde
    {
        get { return this.a_desde; }
        set { this.a_desde = value; }
    }

    public int hasta
    {
        get { return this.a_hasta; }
        set { this.a_hasta = value; }
    }

    public int total
    {
        get { return this.a_total; }
        set { this.a_total = value; }
    }


    public ArrayList sql_parametros
    {
        get { return this.a_sql_parametros; }
        set { this.a_sql_parametros = value; }
    }

    public ArrayList sql_valores
    {
        get { return this.a_sql_valores; }
        set { this.a_sql_valores = value; }
    }

    public void ingresar(object p_objecto) { a_lista.Add(p_objecto); }

}
