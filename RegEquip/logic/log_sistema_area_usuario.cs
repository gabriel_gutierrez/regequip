/**::::::::::::::::::::::::::::::::::::::::::
 *
 * Clase: Lógica para el objeto area_usuario
 * @author Gabriel Gutierrez Carcamo <gabriel.gutierrez@emfe.cl>
 * @category Lógica de la aplicación
 * @copyright Sistema de Registro de Equipos (c) 2018
 * @copyright Administra información de las asignaciones de equipos a los usuarios.
 * @version 0.1 lunes, 19 de marzo de 2018 (11:59 Hr.)
 * 
 * :::::::::::::::::::::::::::::::::::::::::::*/

using System.Collections;

/// <summary>Implementa la lógica del negocio para el objeto area_usuario donde es responsable la clase sistema/// </summary>



public class Log_sistema_area_usuario
{
    public Log_sistema_area_usuario() { }

    /** ------- Destructor que se inicia al liberar un objeto --------*/
    ~Log_sistema_area_usuario() { }
    /** --------------------------------------------------------------*/


    /// <summary>Ingresar un objeto area_usuario a la base de datos.</summary>
    /// <param name="p_area_usuario" >objeto </param>
    /// <returns> Devuelve un mensaje con información para el usuario. </returns>
    public string ingresar_area_usuario(Area_usuario p_area_usuario)
    {
        Fac_sistema_area_usuario m_fachada = new Fac_sistema_area_usuario("");
        m_fachada.conectar();
        string m_mensaje = "Error: Al tratar de ingresar la información";

        if (!m_fachada.existe_area_usuario(p_area_usuario))
        {
            int m_respuesta = m_fachada.ingresar_area_usuario(p_area_usuario);

            if (m_respuesta > 0)
                m_mensaje = "Ok, Se han ingresado los datos al sistema";
            else
                m_mensaje = m_fachada.get_error();
        }
        else
            m_mensaje = "Error: Ya existe un area_usuario con msimo datos en el sistema.";

        m_fachada.cerrar();

        return (m_mensaje);
    }


    /// <summary>Modificar un objeto area_usuario a la base de datos.</summary>
    /// <param name="p_area_usuario" >objeto </param>
    /// <returns> Devuelve un mensaje con información para el usuario. </returns>
    public string modificar_area_usuario(Area_usuario p_area_usuario)
    {
        Fac_sistema_area_usuario m_fachada = new Fac_sistema_area_usuario("");
        m_fachada.conectar();
        string m_mensaje = "Error: Al tratar de modificar la información";

        int m_respuesta = m_fachada.modificar_area_usuario(p_area_usuario);

        if (m_respuesta > 0) m_mensaje = "Ok, su información ha sido modificada en el sistema";
        else m_mensaje = "Error: " + m_fachada.get_error();

        m_fachada.cerrar();

        return (m_mensaje);
    }


    /// <summary>Obtiene un objeto area_usuario a la base de datos.</summary>
    public Area_usuario get_area_usuario(string p_id_area_usuario)
    {
        Fac_sistema_area_usuario m_fachada = new Fac_sistema_area_usuario("");
        m_fachada.conectar();

        Area_usuario m_area_usuario = m_fachada.get_area_usuario(p_id_area_usuario);
        m_fachada.cerrar();

        return (m_area_usuario);

    }

    /// <summary>Retorna una lista de codigo primarios para los objetos area_usuario desde la base de datos</summary>
    /// <param name="p_area_usuario" >objeto </param>
    /// <returns> Devuelve un objeto area_usuario, es caso contrario null</returns>
    public Lista get_lista_area_usuarios(Lista p_lista)
    {
        Fac_sistema_area_usuario m_fachada = new Fac_sistema_area_usuario("");
        m_fachada.conectar();

        ArrayList m_lista = m_fachada.get_lista_area_usuarios(p_lista);
        int m_largo = m_lista.Count;

        for (int i = 0; i < m_largo; i++)
        {
            string m_clave = (string)m_lista[i];
            Area_usuario m_area_usuario = m_fachada.get_area_usuario(m_clave);

            p_lista.ingresar(m_area_usuario);
        }

        m_fachada.cerrar();
        return (p_lista);
    }
    

} // FIN DE LA CLASE LOGICA LOG_SISTEMA_AREA_USUARIO */
