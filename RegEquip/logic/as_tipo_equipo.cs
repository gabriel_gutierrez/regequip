/**::::::::::::::::::::::::::::::::::::::::::
 *
 * Clase: OBJETO as_tipo_equipo
 * @author Gabriel Gutierrez Carcamo <gabriel.gutierrez@emfe.cl>
 * @copyright Sistema de Registro de Equipos (c) 2018
 * @version 0.1 lunes, 19 de marzo de 2018 (10:32 Hr.)
 * 
 * :::::::::::::::::::::::::::::::::::::::::::*/
 
/// <summary>Define un objeto  as_tipo_equipo que permite almacenarlo en la memoria.</summary>

using System;

public class As_tipo_equipo
{
  private string a_id_tipo_equipo;
  private string a_descripcion_tipo_equipo;
  private string a_id_detalle_linea;
  private string a_valido;

  /** atributos objetos */
  private As_detalle_linea a_detalle_linea;


/** ------------- SE INICIA EL CONSTRUCTOR DE LA CLASE -----------*/
  public As_tipo_equipo(string p_id_tipo_equipo, string p_descripcion_tipo_equipo, string p_id_detalle_linea, string p_valido )
  {
    this.a_id_tipo_equipo = p_id_tipo_equipo;
    this.a_descripcion_tipo_equipo = p_descripcion_tipo_equipo;
    this.a_id_detalle_linea = p_id_detalle_linea;
    this.a_valido = p_valido;

  }
/** --------------------------------------------------------------*/

/** ------------- SE INICIA EL CONSTRUCTOR DE LA CLASE -----------*/
  public As_tipo_equipo() { }
/** --------------------------------------------------------------*/

/** -------- METODO QUE SE INICIA AL ELIMINAR EL OBJETO ----------*/
  ~As_tipo_equipo() { }
/** --------------------------------------------------------------*/


  /// <summary> Funci�n para asignar o retorna el valor id_tipo_equipo sin parametros </summary>
  /// <returns> Devuelve una cadena de caracteres (string) </returns>
  public string id_tipo_equipo
  {  get { return this.a_id_tipo_equipo; }
     set { this.a_id_tipo_equipo = value; }  }

  /// <summary> Funci�n para asignar o retorna el valor descripcion_tipo_equipo sin parametros </summary>
  /// <returns> Devuelve una cadena de caracteres (string) </returns>
  public string descripcion_tipo_equipo
  {  get { return this.a_descripcion_tipo_equipo; }
     set { this.a_descripcion_tipo_equipo = value; }  }

  /// <summary> Funci�n para asignar o retorna el valor id_detalle_linea sin parametros </summary>
  /// <returns> Devuelve una cadena de caracteres (string) </returns>
  public string id_detalle_linea
  {  get { return this.a_id_detalle_linea; }
     set { this.a_id_detalle_linea = value; }  }

  /// <summary> Funci�n para asignar o retorna el valor id_detalle_linea sin parametros </summary>
  /// <returns> Devuelve una cadena de caracteres (string) </returns>
  public As_detalle_linea detalle_linea
  {  get { return this.a_detalle_linea; }
     set { this.a_detalle_linea = value; }  }

  /// <summary> Funci�n para asignar o retorna el valor valido sin parametros </summary>
  /// <returns> Devuelve una cadena de caracteres (string) </returns>
  public string valido
  {  get { return this.a_valido; }
     set { this.a_valido = value; }  }


} /** FIN DE LA CLASE AS_TIPO_EQUIPO */
