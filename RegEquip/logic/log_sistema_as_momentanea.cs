/**::::::::::::::::::::::::::::::::::::::::::
 *
 * Clase: Lógica para el objeto as_momentanea
 * @author Gabriel Gutierrez Carcamo <gabriel.gutierrez@emfe.cl>
 * @category Lógica de la aplicación
 * @copyright Sistema de Registro de Equipos (c) 2018
 * @copyright Administra información de las asignaciones de equipos a los usuarios.
 * @version 0.1 lunes, 19 de marzo de 2018 (10:29 Hr.)
 * 
 * :::::::::::::::::::::::::::::::::::::::::::*/

using System.Collections;

/// <summary>Implementa la lógica del negocio para el objeto as_momentanea donde es responsable la clase sistema/// </summary>



public class Log_sistema_as_momentanea
{
    public Log_sistema_as_momentanea() {  }

  /** ------- Destructor que se inicia al liberar un objeto --------*/
    ~Log_sistema_as_momentanea() { }
  /** --------------------------------------------------------------*/


  /// <summary>Ingresar un objeto as_momentanea a la base de datos.</summary>
  /// <param name="p_as_momentanea" >objeto </param>
  /// <returns> Devuelve un mensaje con información para el usuario. </returns>
  public string ingresar_as_momentanea(As_momentanea p_as_momentanea)
  {
     Fac_sistema_as_momentanea m_fachada = new Fac_sistema_as_momentanea("");
     m_fachada.conectar();
     string m_mensaje = "Error: Al tratar de ingresar la información";

     if (!m_fachada.existe_as_momentanea(p_as_momentanea))
     {
       int m_respuesta = m_fachada.ingresar_as_momentanea(p_as_momentanea);
  
       if (m_respuesta > 0)
             m_mensaje = "Ok, Se han ingresado los datos al sistema";
       else
             m_mensaje = m_fachada.get_error();
     }
     else
           m_mensaje = "Error: Ya existe un as_momentanea con msimo datos en el sistema.";

     m_fachada.cerrar();

     return(m_mensaje);
  }


  /// <summary>Modificar un objeto as_momentanea a la base de datos.</summary>
  /// <param name="p_as_momentanea" >objeto </param>
  /// <returns> Devuelve un mensaje con información para el usuario. </returns>
  public string modificar_as_momentanea(As_momentanea p_as_momentanea)
  {
     Fac_sistema_as_momentanea m_fachada = new Fac_sistema_as_momentanea("");
     m_fachada.conectar();
     string m_mensaje = "Error: Al tratar de modificar la información";

     int m_respuesta = m_fachada.modificar_as_momentanea(p_as_momentanea);

     if (m_respuesta > 0) m_mensaje = "Ok, su información ha sido modificada en el sistema";
     else m_mensaje = "Error: " +  m_fachada.get_error();

     m_fachada.cerrar();

     return(m_mensaje);
  }


  /// <summary>Obtiene un objeto as_momentanea a la base de datos.</summary>
  public As_momentanea get_as_momentanea(string p_id_momentanea)
  {
     Fac_sistema_as_momentanea m_fachada = new Fac_sistema_as_momentanea("");
     m_fachada.conectar();

     As_momentanea m_as_momentanea = m_fachada.get_as_momentanea(p_id_momentanea);
     m_fachada.cerrar();

     return(m_as_momentanea);

  }

  /// <summary>Retorna una lista de codigo primarios para los objetos as_momentanea desde la base de datos</summary>
  /// <param name="p_as_momentanea" >objeto </param>
  /// <returns> Devuelve un objeto as_momentanea, es caso contrario null</returns>
  public Lista  get_lista_as_momentaneas(Lista p_lista)
  {
     Fac_sistema_as_momentanea m_fachada = new Fac_sistema_as_momentanea("");
     m_fachada.conectar();

     ArrayList m_lista =  m_fachada.get_lista_as_momentaneas(p_lista);
     int m_largo = m_lista.Count;

     for (int i = 0; i < m_largo; i++)
     {
        string m_clave = (string)m_lista[i];
        As_momentanea m_as_momentanea = m_fachada.get_as_momentanea(m_clave);

        p_lista.ingresar(m_as_momentanea);
     }

     m_fachada.cerrar();
     return (p_lista);
  }

} // FIN DE LA CLASE LOGICA LOG_SISTEMA_AS_MOMENTANEA */
