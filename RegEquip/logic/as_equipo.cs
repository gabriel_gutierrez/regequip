/**::::::::::::::::::::::::::::::::::::::::::
 *
 * Clase: OBJETO as_equipo
 * @author Gabriel Gutierrez Carcamo <gabriel.gutierrez@emfe.cl>
 * @copyright Sistema de Registro de Equipos (c) 2018
 * @version 0.1 lunes, 19 de marzo de 2018 (10:32 Hr.)
 * 
 * :::::::::::::::::::::::::::::::::::::::::::*/
 
/// <summary>Define un objeto  as_equipo que permite almacenarlo en la memoria.</summary>

using System;

public class As_equipo
{
  private string a_id_equipo;
  private string a_id_tipo_equipo;
  private string a_id_marca;
  private string a_id_modelo;
  private string a_mac_ethernet;
  private string a_ip_ethernet_telefono;
  private string a_mac_wifi;
  private string a_ip_wifi;
  private string a_identificador;
  private string a_observacion;
  private string a_id_detalle_linea;
  private string a_valido;

  /** atributos objetos */
  private As_tipo_equipo a_tipo_equipo;
  private As_marca a_marca;
  private As_modelo a_modelo;
  private As_detalle_linea a_detalle_linea;


/** ------------- SE INICIA EL CONSTRUCTOR DE LA CLASE -----------*/
  public As_equipo(string p_id_equipo, string p_id_tipo_equipo, string p_id_marca, string p_id_modelo, string p_mac_ethernet, string p_ip_ethernet_telefono, string p_mac_wifi, string p_ip_wifi, string p_identificador, string p_observacion, string p_id_detalle_linea, string p_valido )
  {
    this.a_id_equipo = p_id_equipo;
    this.a_id_tipo_equipo = p_id_tipo_equipo;
    this.a_id_marca = p_id_marca;
    this.a_id_modelo = p_id_modelo;
    this.a_mac_ethernet = p_mac_ethernet;
    this.a_ip_ethernet_telefono = p_ip_ethernet_telefono;
    this.a_mac_wifi = p_mac_wifi;
    this.a_ip_wifi = p_ip_wifi;
    this.a_identificador = p_identificador;
    this.a_observacion = p_observacion;
    this.a_id_detalle_linea = p_id_detalle_linea;
    this.a_valido = p_valido;

  }
/** --------------------------------------------------------------*/

/** ------------- SE INICIA EL CONSTRUCTOR DE LA CLASE -----------*/
  public As_equipo() { }
/** --------------------------------------------------------------*/

/** -------- METODO QUE SE INICIA AL ELIMINAR EL OBJETO ----------*/
  ~As_equipo() { }
/** --------------------------------------------------------------*/


  /// <summary> Funci�n para asignar o retorna el valor id_equipo sin parametros </summary>
  /// <returns> Devuelve una cadena de caracteres (string) </returns>
  public string id_equipo
  {  get { return this.a_id_equipo; }
     set { this.a_id_equipo = value; }  }

  /// <summary> Funci�n para asignar o retorna el valor id_tipo_equipo sin parametros </summary>
  /// <returns> Devuelve una cadena de caracteres (string) </returns>
  public string id_tipo_equipo
  {  get { return this.a_id_tipo_equipo; }
     set { this.a_id_tipo_equipo = value; }  }

  /// <summary> Funci�n para asignar o retorna el valor id_tipo_equipo sin parametros </summary>
  /// <returns> Devuelve una cadena de caracteres (string) </returns>
  public As_tipo_equipo tipo_equipo
  {  get { return this.a_tipo_equipo; }
     set { this.a_tipo_equipo = value; }  }

  /// <summary> Funci�n para asignar o retorna el valor id_marca sin parametros </summary>
  /// <returns> Devuelve una cadena de caracteres (string) </returns>
  public string id_marca
  {  get { return this.a_id_marca; }
     set { this.a_id_marca = value; }  }

  /// <summary> Funci�n para asignar o retorna el valor id_marca sin parametros </summary>
  /// <returns> Devuelve una cadena de caracteres (string) </returns>
  public As_marca marca
  {  get { return this.a_marca; }
     set { this.a_marca = value; }  }

  /// <summary> Funci�n para asignar o retorna el valor id_modelo sin parametros </summary>
  /// <returns> Devuelve una cadena de caracteres (string) </returns>
  public string id_modelo
  {  get { return this.a_id_modelo; }
     set { this.a_id_modelo = value; }  }

  /// <summary> Funci�n para asignar o retorna el valor id_modelo sin parametros </summary>
  /// <returns> Devuelve una cadena de caracteres (string) </returns>
  public As_modelo modelo
  {  get { return this.a_modelo; }
     set { this.a_modelo = value; }  }

  /// <summary> Funci�n para asignar o retorna el valor mac_ethernet sin parametros </summary>
  /// <returns> Devuelve una cadena de caracteres (string) </returns>
  public string mac_ethernet
  {  get { return this.a_mac_ethernet; }
     set { this.a_mac_ethernet = value; }  }

  /// <summary> Funci�n para asignar o retorna el valor ip_ethernet_telefono sin parametros </summary>
  /// <returns> Devuelve una cadena de caracteres (string) </returns>
  public string ip_ethernet_telefono
  {  get { return this.a_ip_ethernet_telefono; }
     set { this.a_ip_ethernet_telefono = value; }  }

  /// <summary> Funci�n para asignar o retorna el valor mac_wifi sin parametros </summary>
  /// <returns> Devuelve una cadena de caracteres (string) </returns>
  public string mac_wifi
  {  get { return this.a_mac_wifi; }
     set { this.a_mac_wifi = value; }  }

  /// <summary> Funci�n para asignar o retorna el valor ip_wifi sin parametros </summary>
  /// <returns> Devuelve una cadena de caracteres (string) </returns>
  public string ip_wifi
  {  get { return this.a_ip_wifi; }
     set { this.a_ip_wifi = value; }  }

  /// <summary> Funci�n para asignar o retorna el valor identificador sin parametros </summary>
  /// <returns> Devuelve una cadena de caracteres (string) </returns>
  public string identificador
  {  get { return this.a_identificador; }
     set { this.a_identificador = value; }  }

  /// <summary> Funci�n para asignar o retorna el valor observacion sin parametros </summary>
  /// <returns> Devuelve una cadena de caracteres (string) </returns>
  public string observacion
  {  get { return this.a_observacion; }
     set { this.a_observacion = value; }  }

  /// <summary> Funci�n para asignar o retorna el valor id_detalle_linea sin parametros </summary>
  /// <returns> Devuelve una cadena de caracteres (string) </returns>
  public string id_detalle_linea
  {  get { return this.a_id_detalle_linea; }
     set { this.a_id_detalle_linea = value; }  }

  /// <summary> Funci�n para asignar o retorna el valor id_detalle_linea sin parametros </summary>
  /// <returns> Devuelve una cadena de caracteres (string) </returns>
  public As_detalle_linea detalle_linea
  {  get { return this.a_detalle_linea; }
     set { this.a_detalle_linea = value; }  }

  /// <summary> Funci�n para asignar o retorna el valor valido sin parametros </summary>
  /// <returns> Devuelve una cadena de caracteres (string) </returns>
  public string valido
  {  get { return this.a_valido; }
     set { this.a_valido = value; }  }


} /** FIN DE LA CLASE AS_EQUIPO */
