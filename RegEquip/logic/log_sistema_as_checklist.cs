/**::::::::::::::::::::::::::::::::::::::::::
 *
 * Clase: Lógica para el objeto as_checklist
 * @author Gabriel Gutierrez Carcamo <gabriel.gutierrez@emfe.cl>
 * @category Lógica de la aplicación
 * @copyright Sistema de Registro de Equipos (c) 2018
 * @copyright Administra información de las asignaciones de equipos a los usuarios.
 * @version 0.1 lunes, 19 de marzo de 2018 (10:33 Hr.)
 * 
 * :::::::::::::::::::::::::::::::::::::::::::*/

using System.Collections;

/// <summary>Implementa la lógica del negocio para el objeto as_checklist donde es responsable la clase sistema/// </summary>



public class Log_sistema_as_checklist
{
    public Log_sistema_as_checklist() {  }

  /** ------- Destructor que se inicia al liberar un objeto --------*/
    ~Log_sistema_as_checklist() { }
  /** --------------------------------------------------------------*/


  /// <summary>Ingresar un objeto as_checklist a la base de datos.</summary>
  /// <param name="p_as_checklist" >objeto </param>
  /// <returns> Devuelve un mensaje con información para el usuario. </returns>
  public string ingresar_as_checklist(As_checklist p_as_checklist)
  {
     Fac_sistema_as_checklist m_fachada = new Fac_sistema_as_checklist("");
     m_fachada.conectar();
     string m_mensaje = "Error: Al tratar de ingresar la información";

     if (!m_fachada.existe_as_checklist(p_as_checklist))
     {
       int m_respuesta = m_fachada.ingresar_as_checklist(p_as_checklist);
  
       if (m_respuesta > 0)
             m_mensaje = "Ok, Se han ingresado los datos al sistema";
       else
             m_mensaje = m_fachada.get_error();
     }
     else
           m_mensaje = "Error: Ya existe un as_checklist con msimo datos en el sistema.";

     m_fachada.cerrar();

     return(m_mensaje);
  }


  /// <summary>Modificar un objeto as_checklist a la base de datos.</summary>
  /// <param name="p_as_checklist" >objeto </param>
  /// <returns> Devuelve un mensaje con información para el usuario. </returns>
  public string modificar_as_checklist(As_checklist p_as_checklist)
  {
     Fac_sistema_as_checklist m_fachada = new Fac_sistema_as_checklist("");
     m_fachada.conectar();
     string m_mensaje = "Error: Al tratar de modificar la información";

     int m_respuesta = m_fachada.modificar_as_checklist(p_as_checklist);

     if (m_respuesta > 0) m_mensaje = "Ok, su información ha sido modificada en el sistema";
     else m_mensaje = "Error: " +  m_fachada.get_error();

     m_fachada.cerrar();

     return(m_mensaje);
  }


  /// <summary>Obtiene un objeto as_checklist a la base de datos.</summary>
  public As_checklist get_as_checklist(string p_id_checklist)
  {
     Fac_sistema_as_checklist m_fachada = new Fac_sistema_as_checklist("");
     m_fachada.conectar();

     As_checklist m_as_checklist = m_fachada.get_as_checklist(p_id_checklist);
     m_fachada.cerrar();

     return(m_as_checklist);

  }

  /// <summary>Retorna una lista de codigo primarios para los objetos as_checklist desde la base de datos</summary>
  /// <param name="p_as_checklist" >objeto </param>
  /// <returns> Devuelve un objeto as_checklist, es caso contrario null</returns>
  public Lista  get_lista_as_checklistes(Lista p_lista)
  {
     Fac_sistema_as_checklist m_fachada = new Fac_sistema_as_checklist("");
     m_fachada.conectar();

     ArrayList m_lista =  m_fachada.get_lista_as_checklistes(p_lista);
     int m_largo = m_lista.Count;

     for (int i = 0; i < m_largo; i++)
     {
        string m_clave = (string)m_lista[i];
        As_checklist m_as_checklist = m_fachada.get_as_checklist(m_clave);

        p_lista.ingresar(m_as_checklist);
     }

     m_fachada.cerrar();
     return (p_lista);
  }

} // FIN DE LA CLASE LOGICA LOG_SISTEMA_AS_CHECKLIST */
