/**::::::::::::::::::::::::::::::::::::::::::
 *
 * Clase: OBJETO area_usuario
 * @author Gabriel Gutierrez Carcamo <gabriel.gutierrez@emfe.cl>
 * @copyright Sistema de Registro de Equipos (c) 2018
 * @version 0.1 lunes, 19 de marzo de 2018 (11:59 Hr.)
 * 
 * :::::::::::::::::::::::::::::::::::::::::::*/
 
/// <summary>Define un objeto  area_usuario que permite almacenarlo en la memoria.</summary>

using System;

public class Area_usuario
{
  private string a_id_area_usuario;
  private string a_descripcion;


/** ------------- SE INICIA EL CONSTRUCTOR DE LA CLASE -----------*/
  public Area_usuario(string p_id_area_usuario, string p_descripcion )
  {
    this.a_id_area_usuario = p_id_area_usuario;
    this.a_descripcion = p_descripcion;

  }
/** --------------------------------------------------------------*/

/** ------------- SE INICIA EL CONSTRUCTOR DE LA CLASE -----------*/
  public Area_usuario() { }
/** --------------------------------------------------------------*/

/** -------- METODO QUE SE INICIA AL ELIMINAR EL OBJETO ----------*/
  ~Area_usuario() { }
/** --------------------------------------------------------------*/


  /// <summary> Funci�n para asignar o retorna el valor id_area_usuario sin parametros </summary>
  /// <returns> Devuelve una cadena de caracteres (string) </returns>
  public string id_area_usuario
  {  get { return this.a_id_area_usuario; }
     set { this.a_id_area_usuario = value; }  }

  /// <summary> Funci�n para asignar o retorna el valor descripcion sin parametros </summary>
  /// <returns> Devuelve una cadena de caracteres (string) </returns>
  public string descripcion
  {  get { return this.a_descripcion; }
     set { this.a_descripcion = value; }  }


} /** FIN DE LA CLASE AREA_USUARIO */
