/**::::::::::::::::::::::::::::::::::::::::::
 *
 * Clase: OBJETO as_estado
 * @author Gabriel Gutierrez Carcamo <gabriel.gutierrez@emfe.cl>
 * @copyright Sistema de Registro de Equipos (c) 2018
 * @version 0.1 lunes, 19 de marzo de 2018 (10:31 Hr.)
 * 
 * :::::::::::::::::::::::::::::::::::::::::::*/
 
/// <summary>Define un objeto  as_estado que permite almacenarlo en la memoria.</summary>

using System;

public class As_estado
{
  private string a_id_estado;
  private string a_descripcion_es;
  private string a_valido;


/** ------------- SE INICIA EL CONSTRUCTOR DE LA CLASE -----------*/
  public As_estado(string p_id_estado, string p_descripcion_es, string p_valido )
  {
    this.a_id_estado = p_id_estado;
    this.a_descripcion_es = p_descripcion_es;
    this.a_valido = p_valido;

  }
/** --------------------------------------------------------------*/

/** ------------- SE INICIA EL CONSTRUCTOR DE LA CLASE -----------*/
  public As_estado() { }
/** --------------------------------------------------------------*/

/** -------- METODO QUE SE INICIA AL ELIMINAR EL OBJETO ----------*/
  ~As_estado() { }
/** --------------------------------------------------------------*/


  /// <summary> Funci�n para asignar o retorna el valor id_estado sin parametros </summary>
  /// <returns> Devuelve una cadena de caracteres (string) </returns>
  public string id_estado
  {  get { return this.a_id_estado; }
     set { this.a_id_estado = value; }  }

  /// <summary> Funci�n para asignar o retorna el valor descripcion_es sin parametros </summary>
  /// <returns> Devuelve una cadena de caracteres (string) </returns>
  public string descripcion_es
  {  get { return this.a_descripcion_es; }
     set { this.a_descripcion_es = value; }  }

  /// <summary> Funci�n para asignar o retorna el valor valido sin parametros </summary>
  /// <returns> Devuelve una cadena de caracteres (string) </returns>
  public string valido
  {  get { return this.a_valido; }
     set { this.a_valido = value; }  }


} /** FIN DE LA CLASE AS_ESTADO */
