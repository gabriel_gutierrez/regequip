/**::::::::::::::::::::::::::::::::::::::::::
 *
 * Clase: L�gica para el objeto usuario_sistema
 * @author Gabriel Gutierrez Carcamo <gabriel.gutierrez@emfe.cl>
 * @category L�gica de la aplicaci�n
 * @copyright Sistema de Registro de Equipos (c) 2018
 * @copyright Administra informaci�n de las asignaciones de equipos a los usuarios.
 * @version 0.1 lunes, 19 de marzo de 2018 (10:44 Hr.)
 * 
 * :::::::::::::::::::::::::::::::::::::::::::*/

using System;
using System.Data;
using System.Configuration;
using System.Collections.Generic;
using System.Collections;

/// <summary>Implementa la l�gica del negocio para el objeto usuario_sistema donde es responsable la clase sistema/// </summary>



public class Log_sistema_usuario_sistema
{
    public Log_sistema_usuario_sistema() { }

    /** ------- Destructor que se inicia al liberar un objeto --------*/
    ~Log_sistema_usuario_sistema() { }
    /** --------------------------------------------------------------*/


    /// <summary>Ingresar un objeto usuario_sistema a la base de datos.</summary>
    /// <param name="p_usuario_sistema" >objeto </param>
    /// <returns> Devuelve un mensaje con informaci�n para el usuario. </returns>
    public string ingresar_usuario_sistema(Usuario_sistema p_usuario_sistema)
    {
        Fac_sistema_usuario_sistema m_fachada = new Fac_sistema_usuario_sistema("");
        m_fachada.conectar();
        string m_mensaje = "Error: Al tratar de ingresar la informaci�n";

        if (!m_fachada.existe_usuario_sistema(p_usuario_sistema))
        {
            int m_respuesta = m_fachada.ingresar_usuario_sistema(p_usuario_sistema);

            if (m_respuesta > 0)
                m_mensaje = "Ok, Se han ingresado los datos al sistema";
            else
                m_mensaje = m_fachada.get_error();
        }
        else
            m_mensaje = "Error: Ya existe un usuario_sistema con msimo datos en el sistema.";

        m_fachada.cerrar();

        return (m_mensaje);
    }


    /// <summary>Modificar un objeto usuario_sistema a la base de datos.</summary>
    /// <param name="p_usuario_sistema" >objeto </param>
    /// <returns> Devuelve un mensaje con informaci�n para el usuario. </returns>
    public string modificar_usuario_sistema(Usuario_sistema p_usuario_sistema)
    {
        Fac_sistema_usuario_sistema m_fachada = new Fac_sistema_usuario_sistema("");
        m_fachada.conectar();
        string m_mensaje = "Error: Al tratar de modificar la informaci�n";

        int m_respuesta = m_fachada.modificar_usuario_sistema(p_usuario_sistema);

        if (m_respuesta > 0) m_mensaje = "Ok, su informaci�n ha sido modificada en el sistema";
        else m_mensaje = "Error: " + m_fachada.get_error();

        m_fachada.cerrar();

        return (m_mensaje);
    }


    /// <summary>Obtiene un objeto usuario_sistema a la base de datos.</summary>
    public Usuario_sistema get_usuario_sistema(string p_id_usuario)
    {
        Fac_sistema_usuario_sistema m_fachada = new Fac_sistema_usuario_sistema("");
        m_fachada.conectar();

        Usuario_sistema m_usuario_sistema = m_fachada.get_usuario_sistema(p_id_usuario);
        m_fachada.cerrar();

        return (m_usuario_sistema);

    }

    /// <summary>Retorna una lista de codigo primarios para los objetos usuario_sistema desde la base de datos</summary>
    /// <param name="p_usuario_sistema" >objeto </param>
    /// <returns> Devuelve un objeto usuario_sistema, es caso contrario null</returns>

    public ArrayList get_lista_usuario_sistemas()
    {
        return get_lista_usuario_sistemas(new Lista());
    }

    public DataSet get_lista_usuario_sistemas_dt()
    {
        return get_lista_usuario_sistemas_dt(new Lista(), false);
    }

    public ArrayList get_lista_usuario_sistemas(Lista p_lista)
    {
        Fac_sistema_usuario_sistema m_fachada = new Fac_sistema_usuario_sistema("");
        m_fachada.conectar();

        ArrayList m_lista = m_fachada.get_lista_usuario_sistemas(p_lista, "");

        m_fachada.cerrar();

        return (m_lista);
    }

    public DataSet get_lista_usuario_sistemas_dt(Lista p_lista, bool ordenar)
    {
        Fac_sistema_usuario_sistema m_fachada = new Fac_sistema_usuario_sistema("");
        m_fachada.conectar();

        DataSet m_lista = (ordenar) ? m_fachada.get_lista_usuario_sistemas_dt(p_lista, "APELLIDOS") : m_fachada.get_lista_usuario_sistemas_dt(p_lista, "");

        m_fachada.cerrar();

        return (m_lista);
    }

    /// <summary>Verifica si el usuario esta autorizado en el sistema</summary>
    /// <param name="p_login" >string Nombre del usuario</param>
    /// <param name="p_pass" >string Contrase�a del usuario</param>
    /// <returns> Devuelve un objeto usuario, en caso contrario un null. </returns>
    public Usuario_sistema autentificar(string p_login, string p_pass)
    {
        Fac_sistema_usuario_sistema m_fachada = new Fac_sistema_usuario_sistema("");
        Usuario_sistema m_usuario;
        m_fachada.conectar();

        m_usuario = m_fachada.obtieneUsuario(p_login, p_pass);

        // valida si el usuario a�n esta vigente
        if (m_usuario.fecha_caduca == null)
        {
            // el usuario esta deshabilitado 
            m_usuario = null;
        }

        m_fachada.cerrar();

        return (m_usuario);
    }


    /// <summary>Retorna un objeto SISTEMA</summary>
    /*public Sistema get_sistema()
    {
        Fac_sistema_usuario_sistema m_fachada = new Fac_sistema_usuario_sistema("");
        m_fachada.conectar();

        m_sistema = m_fachada.get_sistema();
        m_fachada.cerrar();

        if (is_object(m_sistema)) return (m_sistema);
        else return (null);

    }*/

} // FIN DE LA CLASE LOGICA LOG_SISTEMA_USUARIO_SISTEMA */
