/**::::::::::::::::::::::::::::::::::::::::::
 *
 * Clase: Lógica para el objeto estado_empresa
 * @author Gabriel Gutierrez Carcamo <gabriel.gutierrez@emfe.cl>
 * @category Lógica de la aplicación
 * @copyright Sistema de Registro de Equipos (c) 2018
 * @copyright Administra información de las asignaciones de equipos a los usuarios.
 * @version 0.1 lunes, 19 de marzo de 2018 (12:15 Hr.)
 * 
 * :::::::::::::::::::::::::::::::::::::::::::*/

using System.Collections;

/// <summary>Implementa la lógica del negocio para el objeto estado_empresa donde es responsable la clase sistema/// </summary>



public class Log_sistema_estado_empresa
{
    public Log_sistema_estado_empresa() {  }

  /** ------- Destructor que se inicia al liberar un objeto --------*/
    ~Log_sistema_estado_empresa() { }
  /** --------------------------------------------------------------*/


  /// <summary>Ingresar un objeto estado_empresa a la base de datos.</summary>
  /// <param name="p_estado_empresa" >objeto </param>
  /// <returns> Devuelve un mensaje con información para el usuario. </returns>
  public string ingresar_estado_empresa(Estado_empresa p_estado_empresa)
  {
     Fac_sistema_estado_empresa m_fachada = new Fac_sistema_estado_empresa("");
     m_fachada.conectar();
     string m_mensaje = "Error: Al tratar de ingresar la información";

     if (!m_fachada.existe_estado_empresa(p_estado_empresa))
     {
       int m_respuesta = m_fachada.ingresar_estado_empresa(p_estado_empresa);
  
       if (m_respuesta > 0)
             m_mensaje = "Ok, Se han ingresado los datos al sistema";
       else
             m_mensaje = m_fachada.get_error();
     }
     else
           m_mensaje = "Error: Ya existe un estado_empresa con msimo datos en el sistema.";

     m_fachada.cerrar();

     return(m_mensaje);
  }


  /// <summary>Modificar un objeto estado_empresa a la base de datos.</summary>
  /// <param name="p_estado_empresa" >objeto </param>
  /// <returns> Devuelve un mensaje con información para el usuario. </returns>
  public string modificar_estado_empresa(Estado_empresa p_estado_empresa)
  {
     Fac_sistema_estado_empresa m_fachada = new Fac_sistema_estado_empresa("");
     m_fachada.conectar();
     string m_mensaje = "Error: Al tratar de modificar la información";

     int m_respuesta = m_fachada.modificar_estado_empresa(p_estado_empresa);

     if (m_respuesta > 0) m_mensaje = "Ok, su información ha sido modificada en el sistema";
     else m_mensaje = "Error: " +  m_fachada.get_error();

     m_fachada.cerrar();

     return(m_mensaje);
  }


  /// <summary>Obtiene un objeto estado_empresa a la base de datos.</summary>
  public Estado_empresa get_estado_empresa(string p_id_estado)
  {
     Fac_sistema_estado_empresa m_fachada = new Fac_sistema_estado_empresa("");
     m_fachada.conectar();

     Estado_empresa m_estado_empresa = m_fachada.get_estado_empresa(p_id_estado);
     m_fachada.cerrar();

     return(m_estado_empresa);

  }

  /// <summary>Retorna una lista de codigo primarios para los objetos estado_empresa desde la base de datos</summary>
  /// <param name="p_estado_empresa" >objeto </param>
  /// <returns> Devuelve un objeto estado_empresa, es caso contrario null</returns>
  public Lista  get_lista_estado_empresas(Lista p_lista)
  {
     Fac_sistema_estado_empresa m_fachada = new Fac_sistema_estado_empresa("");
     m_fachada.conectar();

     ArrayList m_lista =  m_fachada.get_lista_estado_empresas(p_lista);
     int m_largo = m_lista.Count;

     for (int i = 0; i < m_largo; i++)
     {
        string m_clave = (string)m_lista[i];
        Estado_empresa m_estado_empresa = m_fachada.get_estado_empresa(m_clave);

        p_lista.ingresar(m_estado_empresa);
     }

     m_fachada.cerrar();
     return (p_lista);
  }

} // FIN DE LA CLASE LOGICA LOG_SISTEMA_ESTADO_EMPRESA */
