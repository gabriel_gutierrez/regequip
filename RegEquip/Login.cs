﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace RegEquip
{
    public partial class Login : Form
    {
        public Login()
        {
            InitializeComponent();
        }

        private void Login_Load(object sender, EventArgs e)
        {

        }

        private void textBox1_Leave(object sender, EventArgs e)
        {

        }

        private void txtRut_Leave(object sender, EventArgs e)
        {
            //validarRut();       
        }

        private void btnAcepta_Click(object sender, EventArgs e)
        {
            bool rutValido = true;// validarRut();
            if (rutValido)
            {
                //Log_sistema_usuario_sistema log_usuario = new Log_sistema_usuario_sistema();
                //Usuario_sistema m_usuario = log_usuario.autentificar(txtRut.Text.Substring(0,txtRut.TextLength - 1), txtClave.Text);
                if (rutValido)//(m_usuario != null)
                {
                    this.Hide();
                    //Principal open = new Principal(m_usuario.id_usuario);
                    Principal open = new Principal("2003");
                    open.ShowDialog();
                    this.Show();
                }else
                {
                    MessageBox.Show("Credenciales invalidas. \nFavor verificar.", "Error de credenciales", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                }
            }
            
        }

        private void btnCancela_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }

        private bool validarRut()
        {
            bool valido = false;
            string rut = txtRut.Text;
            try
            {
                rut = rut.ToUpper();
                rut = rut.Replace(".", "");
                rut = rut.Replace("-", "");
                int rutAux = int.Parse(rut.Substring(0, rut.Length - 1));

                char dv = char.Parse(rut.Substring(rut.Length - 1, 1));

                int m = 0, s = 1;
                for (; rutAux != 0; rutAux /= 10)
                {
                    s = (s + rutAux % 10 * (9 - m++ % 6)) % 11;
                }
                if (dv == (char)(s != 0 ? s + 47 : 75))
                {
                    valido = true;
                }

                if (!valido)
                {
                    MessageBox.Show("Rut Incorrecto", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
                return valido;
            }
            catch (Exception)
            {
                MessageBox.Show("Ingrese datos de usuario", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return false;
            }

        }

        private void txtClave_KeyPress(object sender, KeyPressEventArgs e)
        {
            if(e.KeyChar == 13)
            {
                btnAcepta_Click(sender, e);
            }
        }
    }
}
