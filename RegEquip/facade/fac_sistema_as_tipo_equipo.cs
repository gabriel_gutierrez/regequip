/**::::::::::::::::::::::::::::::::::::::::::
 *
 * Clase: Fachada base de datos para el objeto as_tipo_equipo
 * Clase responsable: sistema
 * @author Gabriel Gutierrez Carcamo <gabriel.gutierrez@emfe.cl>
 * @category Fachada base de datos de la aplicaci�n
 * @copyright Sistema de Registro de Equipos (c) 2018
 * @copyright 
 * @version 0.1 lunes, 19 de marzo de 2018 (10:32 Hr.)
 * 
 * :::::::::::::::::::::::::::::::::::::::::::*/

/** Realiza el mapeo desde la base de datos relacional a objeto as_tipo_equipo */

using System.Data.SqlClient;
using System.Text;
using System.Collections;
using System.Data;

public class Fac_sistema_as_tipo_equipo : FacadeSQLServer2005
{
    public Fac_sistema_as_tipo_equipo(string p_string_conexion) : base(p_string_conexion) { }


    /// <summary>Ingresar un objeto As_tipo_equipo a la base de datos</summary>
    /// <param name="p_as_tipo_equipo" >objeto as_tipo_equipo</param> 
    /// <returns> Devuelve un verdadero si se ingreso correctamente, en caso contrario un falso.</returns>
    public int ingresar_as_tipo_equipo(As_tipo_equipo p_as_tipo_equipo)
    {
        int m_reader;
        this.a_sql = new StringBuilder();
        this.a_sql.AppendLine("INSERT INTO as_tipo_equipo(id_tipo_equipo_pk, descripcion_tipo_equipo, id_detalle_linea_fk, valido) ");
        this.a_sql.AppendLine("VALUES (@id_tipo_equipo_pk, @descripcion_tipo_equipo, @id_detalle_linea_fk, @valido);");

        ArrayList m_parametros = new ArrayList();

        m_parametros.Add("@id_tipo_equipo_pk");
        m_parametros.Add("@descripcion_tipo_equipo");
        m_parametros.Add("@id_detalle_linea_fk");
        m_parametros.Add("@valido");


        ArrayList m_valores = new ArrayList();

        m_valores.Add(p_as_tipo_equipo.id_tipo_equipo);
        m_valores.Add(p_as_tipo_equipo.descripcion_tipo_equipo);
        m_valores.Add(p_as_tipo_equipo.id_detalle_linea);
        m_valores.Add(p_as_tipo_equipo.valido);


        m_reader = (int)this.ejecutar_query(this.a_sql.ToString(), m_parametros, m_valores);
        return m_reader;
    }


    /// <summary>Modificar un objeto As_tipo_equipo en la base de datos </summary>
    /// <param name="p_as_tipo_equipo" >objeto as_tipo_equipo</param> 
    /// <returns> Devuelve la cantidad de fila afectadas.</returns>
    public int modificar_as_tipo_equipo(As_tipo_equipo p_as_tipo_equipo)
    {
        int m_reader;
        this.a_sql = new StringBuilder();
        this.a_sql.AppendLine("UPDATE as_tipo_equipo SET ");


        this.a_sql.AppendLine("descripcion_tipo_equipo = @descripcion_tipo_equipo, ");
        this.a_sql.AppendLine("id_detalle_linea = @id_detalle_linea, ");
        this.a_sql.AppendLine("valido = @valido ");


        this.a_sql.AppendLine(" WHERE ");
        this.a_sql.AppendLine("id_tipo_equipo_pk = @id_tipo_equipo_pk ");


        ArrayList m_parametros = new ArrayList();


        m_parametros.Add("@id_tipo_equipo_pk");
        m_parametros.Add("@descripcion_tipo_equipo");
        m_parametros.Add("@id_detalle_linea_fk");
        m_parametros.Add("@valido");


        ArrayList m_valores = new ArrayList();


        m_valores.Add(p_as_tipo_equipo.id_tipo_equipo);
        m_valores.Add(p_as_tipo_equipo.descripcion_tipo_equipo);
        m_valores.Add(p_as_tipo_equipo.id_detalle_linea);
        m_valores.Add(p_as_tipo_equipo.valido);


        m_reader = (int)this.ejecutar_query(this.a_sql.ToString(), m_parametros, m_valores);
        return m_reader;
    }


    /// <summary>Retorna un objeto As_tipo_equipo desde la base de datos  </summary>
    /// <returns> Devuelve un objeto As_tipo_equipo</returns>
    public As_tipo_equipo get_as_tipo_equipo(string p_id_tipo_equipo)
    {
        SqlDataReader m_reader;
        this.a_sql = new StringBuilder();
        this.a_sql.AppendLine("SELECT * FROM as_tipo_equipo WHERE ");
        this.a_sql.AppendLine("id_tipo_equipo_pk = @id_tipo_equipo_pk ");


        ArrayList m_parametros = new ArrayList();

        m_parametros.Add("@id_tipo_equipo_pk");


        ArrayList m_valores = new ArrayList();

        m_valores.Add(p_id_tipo_equipo);



        m_reader = (SqlDataReader)this.ejecutar_query(this.a_sql.ToString(), m_parametros, m_valores);

        if (m_reader.Read())
        {
            As_tipo_equipo m_as_tipo_equipo = new As_tipo_equipo();
            m_as_tipo_equipo.id_tipo_equipo = m_reader["id_tipo_equipo_pk"].ToString();
            m_as_tipo_equipo.descripcion_tipo_equipo = m_reader["descripcion_tipo_equipo"].ToString();
            m_as_tipo_equipo.id_detalle_linea = m_reader["id_detalle_linea_fk"].ToString();
            m_as_tipo_equipo.valido = m_reader["valido"].ToString();

            m_reader.Dispose();

            return (m_as_tipo_equipo);
        }
        else
        {
            m_reader.Dispose();
            return (null);
        }
    }


    /// <summary>Retorna una lista de codigo primarios para los objetos As_tipo_equipo desde la base de datos </summary>
    /// <returns> Devuelve una lista de c�digo claves del objeto </returns>
    public ArrayList get_lista_as_tipo_equipos(Lista p_lista)
    {
        SqlDataReader m_reader;
        this.a_sql = new StringBuilder();

        this.a_sql.AppendLine("SELECT * FROM as_tipo_equipo");

        if (p_lista.sql_parametros.Count > 0)
        {
            int m_largo = p_lista.sql_parametros.Count;
            string m_where = " WHERE ";

            for (int i = 0; i < m_largo; i++)
            {
                m_where = m_where + p_lista.sql_parametros[i] + " LIKE @" + p_lista.sql_parametros[i] + " ";

                if (i < (m_largo - 1))
                    m_where = m_where + " AND ";

                p_lista.sql_parametros[i] = "@" + p_lista.sql_parametros[i];
            }
            this.a_sql.AppendLine(m_where);
        }

        m_reader = (SqlDataReader)this.ejecutar_query(this.a_sql.ToString(), p_lista.sql_parametros, p_lista.sql_valores);
        ArrayList m_lista = new ArrayList();

        while (m_reader.Read())
        {
            m_lista.Add(m_reader["id_tipo_equipo_pk"].ToString());
        }
        m_reader.Dispose();

        return m_lista;
    }

    public DataSet get_lista_as_tipo_equipos_ds(Lista p_lista)
    {
        SqlDataReader m_reader;
        this.a_sql = new StringBuilder();

        this.a_sql.AppendLine("SELECT * FROM as_tipo_equipo");

        if (p_lista.sql_parametros.Count > 0)
        {
            int m_largo = p_lista.sql_parametros.Count;
            string m_where = " WHERE ";

            for (int i = 0; i < m_largo; i++)
            {
                m_where = m_where + p_lista.sql_parametros[i] + " LIKE @" + p_lista.sql_parametros[i] + " ";

                if (i < (m_largo - 1))
                    m_where = m_where + " AND ";

                p_lista.sql_parametros[i] = "@" + p_lista.sql_parametros[i];
            }
            this.a_sql.AppendLine(m_where);
        }
        this.a_sql.AppendLine(" ORDER BY descripcion_tipo_equipo");

        m_reader = (SqlDataReader)this.ejecutar_query(this.a_sql.ToString(), p_lista.sql_parametros, p_lista.sql_valores);
        DataTable dt = new DataTable();
        DataSet m_lista = new DataSet();

        if (m_reader != null)
        {
            dt.Load(m_reader);
            m_lista.Tables.Add(dt);
            m_reader.Close();
        }
        
        m_reader.Dispose();

        return m_lista;
    }


    public bool existe_as_tipo_equipo(As_tipo_equipo p_as_tipo_equipo)
    {
        SqlDataReader m_reader;
        this.a_sql = new StringBuilder();

        this.a_sql.AppendLine("SELECT * FROM as_tipo_equipo WHERE ");
        this.a_sql.AppendLine("id_tipo_equipo_pk = @id_tipo_equipo_pk ");


        return (false);
    }

} // FIN DE LA CLASE FAC_ SISTEMA_AS_TIPO_EQUIPO
