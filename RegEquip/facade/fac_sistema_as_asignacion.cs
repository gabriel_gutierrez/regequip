/**::::::::::::::::::::::::::::::::::::::::::
 *
 * Clase: Fachada base de datos para el objeto as_asignacion
 * Clase responsable: sistema
 * @author Gabriel Gutierrez Carcamo <gabriel.gutierrez@emfe.cl>
 * @category Fachada base de datos de la aplicaci�n
 * @copyright Sistema de Registro de Equipos (c) 2018
 * @copyright 
 * @version 0.1 lunes, 19 de marzo de 2018 (10:33 Hr.)
 * 
 * :::::::::::::::::::::::::::::::::::::::::::*/

/** Realiza el mapeo desde la base de datos relacional a objeto as_asignacion */
using System.Data.SqlClient;
using System.Text;
using System.Collections;


public class Fac_sistema_as_asignacion : FacadeSQLServer2005
{
    public Fac_sistema_as_asignacion(string p_string_conexion) : base(p_string_conexion) { }


    /// <summary>Ingresar un objeto As_asignacion a la base de datos</summary>
    /// <param name="p_as_asignacion" >objeto as_asignacion</param> 
    /// <returns> Devuelve un verdadero si se ingreso correctamente, en caso contrario un falso.</returns>
    public int ingresar_as_asignacion(As_asignacion p_as_asignacion)
    {
        int m_reader;
        this.a_sql = new StringBuilder();
        this.a_sql.AppendLine("INSERT INTO as_asignacion(id_asignacion_pk, folio, id_usuario_asignado_fk, fecha_asignacion, fecha_recepcion, id_equipo_fk, id_estado_fk, id_momentanea_fk, observacion, id_detalle_linea_fk, id_checklist_fk valido) ");
        this.a_sql.AppendLine("VALUES (@id_asignacion_pk, @folio, @id_usuario_asignado_fk, @fecha_asignacion, @fecha_recepcion, @id_equipo_fk, @id_estado_fk, @id_momentanea_fk, @observacion, @id_detalle_linea_fk, @id_checklist_fk, @valido);");

        ArrayList m_parametros = new ArrayList();

        m_parametros.Add("@id_asignacion_pk");
        m_parametros.Add("@folio");
        m_parametros.Add("@id_usuario_asignado_fk");
        m_parametros.Add("@fecha_asignacion");
        m_parametros.Add("@fecha_recepcion");
        m_parametros.Add("@id_equipo_fk");
        m_parametros.Add("@id_estado_fk");
        m_parametros.Add("@id_momentanea_fk");
        m_parametros.Add("@observacion");
        m_parametros.Add("@id_detalle_linea_fk");
        m_parametros.Add("@id_checklist_fk");
        m_parametros.Add("@valido");


        ArrayList m_valores = new ArrayList();

        m_valores.Add(p_as_asignacion.id_asignacion);
        m_valores.Add(p_as_asignacion.folio);
        m_valores.Add(p_as_asignacion.id_usuario_asignado);
        m_valores.Add(p_as_asignacion.fecha_asignacion);
        m_valores.Add(p_as_asignacion.fecha_recepcion);
        m_valores.Add(p_as_asignacion.id_equipo);
        m_valores.Add(p_as_asignacion.id_estado);
        m_valores.Add(p_as_asignacion.id_momentanea);
        m_valores.Add(p_as_asignacion.observacion);
        m_valores.Add(p_as_asignacion.id_detalle_linea);
        m_valores.Add(p_as_asignacion.id_checklist);
        m_valores.Add(p_as_asignacion.valido);


        m_reader = (int)this.ejecutar_query(this.a_sql.ToString(), m_parametros, m_valores);
        return m_reader;
    }


    /// <summary>Modificar un objeto As_asignacion en la base de datos </summary>
    /// <param name="p_as_asignacion" >objeto as_asignacion</param> 
    /// <returns> Devuelve la cantidad de fila afectadas.</returns>
    public int modificar_as_asignacion(As_asignacion p_as_asignacion)
    {
        int m_reader;
        this.a_sql = new StringBuilder();
        this.a_sql.AppendLine("UPDATE as_asignacion SET ");


        this.a_sql.AppendLine("id_usuario_asignado = @id_usuario_asignado, ");
        this.a_sql.AppendLine("folio = @folio, ");
        this.a_sql.AppendLine("fecha_asignacion = @fecha_asignacion, ");
        this.a_sql.AppendLine("fecha_recepcion = @fecha_recepcion, ");
        this.a_sql.AppendLine("id_equipo = @id_equipo, ");
        this.a_sql.AppendLine("id_estado = @id_estado, ");
        this.a_sql.AppendLine("id_momentanea = @id_momentanea, ");
        this.a_sql.AppendLine("observacion = @observacion, ");
        this.a_sql.AppendLine("id_detalle_linea = @id_detalle_linea, ");
        this.a_sql.AppendLine("id_checklist = @id_checklist, ");
        this.a_sql.AppendLine("valido = @valido ");


        this.a_sql.AppendLine(" WHERE ");
        this.a_sql.AppendLine("id_asignacion_pk = @id_asignacion_pk ");


        ArrayList m_parametros = new ArrayList();


        m_parametros.Add("@id_asignacion_pk");
        m_parametros.Add("@folio");
        m_parametros.Add("@id_usuario_asignado_fk");
        m_parametros.Add("@fecha_asignacion");
        m_parametros.Add("@fecha_recepcion");
        m_parametros.Add("@id_equipo_fk");
        m_parametros.Add("@id_estado_fk");
        m_parametros.Add("@id_momentanea_fk");
        m_parametros.Add("@observacion");
        m_parametros.Add("@id_detalle_linea_fk");
        m_parametros.Add("@id_checklist_fk");
        m_parametros.Add("@valido");


        ArrayList m_valores = new ArrayList();


        m_valores.Add(p_as_asignacion.id_asignacion);
        m_valores.Add(p_as_asignacion.folio);
        m_valores.Add(p_as_asignacion.id_usuario_asignado);
        m_valores.Add(p_as_asignacion.fecha_asignacion);
        m_valores.Add(p_as_asignacion.fecha_recepcion);
        m_valores.Add(p_as_asignacion.id_equipo);
        m_valores.Add(p_as_asignacion.id_estado);
        m_valores.Add(p_as_asignacion.id_momentanea);
        m_valores.Add(p_as_asignacion.observacion);
        m_valores.Add(p_as_asignacion.id_detalle_linea);
        m_valores.Add(p_as_asignacion.id_checklist);
        m_valores.Add(p_as_asignacion.valido);


        m_reader = (int)this.ejecutar_query(this.a_sql.ToString(), m_parametros, m_valores);
        return m_reader;
    }


    /// <summary>Retorna un objeto As_asignacion desde la base de datos  </summary>
    /// <returns> Devuelve un objeto As_asignacion</returns>
    public As_asignacion get_as_asignacion(string p_id_asignacion)
    {
        SqlDataReader m_reader;
        this.a_sql = new StringBuilder();
        this.a_sql.AppendLine("SELECT * FROM as_asignacion WHERE ");
        this.a_sql.AppendLine("id_asignacion_pk = @id_asignacion_pk ");


        ArrayList m_parametros = new ArrayList();

        m_parametros.Add("@id_asignacion_pk");


        ArrayList m_valores = new ArrayList();

        m_valores.Add(p_id_asignacion);



        m_reader = (SqlDataReader)this.ejecutar_query(this.a_sql.ToString(), m_parametros, m_valores);

        if (m_reader.Read())
        {
            As_asignacion m_as_asignacion = new As_asignacion();
            m_as_asignacion.id_asignacion = m_reader["id_asignacion_pk"].ToString();
            m_as_asignacion.folio = m_reader["folio"].ToString();
            m_as_asignacion.id_usuario_asignado = m_reader["id_usuario_asignado_fk"].ToString();
            m_as_asignacion.fecha_asignacion = m_reader["fecha_asignacion"].ToString();
            m_as_asignacion.fecha_recepcion = m_reader["fecha_recepcion"].ToString();
            m_as_asignacion.id_equipo = m_reader["id_equipo_fk"].ToString();
            m_as_asignacion.id_estado = m_reader["id_estado_fk"].ToString();
            m_as_asignacion.id_momentanea = m_reader["id_momentanea_fk"].ToString();
            m_as_asignacion.observacion = m_reader["observacion"].ToString();
            m_as_asignacion.id_detalle_linea = m_reader["id_detalle_linea_fk"].ToString();
            m_as_asignacion.id_detalle_linea = m_reader["id_checklist_fk"].ToString();
            m_as_asignacion.valido = m_reader["valido"].ToString();

            m_reader.Dispose();

            return (m_as_asignacion);
        }
        else
        {
            m_reader.Dispose();
            return (null);
        }
    }


    /// <summary>Retorna una lista de codigo primarios para los objetos As_asignacion desde la base de datos </summary>
    /// <returns> Devuelve una lista de c�digo claves del objeto </returns>
    public ArrayList get_lista_as_asignaciones(Lista p_lista)
    {
        SqlDataReader m_reader;
        this.a_sql = new StringBuilder();

        this.a_sql.AppendLine("SELECT * FROM as_asignacion");

        if (p_lista.sql_parametros.Count > 0)
        {
            int m_largo = p_lista.sql_parametros.Count;
            string m_where = " WHERE ";

            for (int i = 0; i < m_largo; i++)
            {
                m_where = m_where + p_lista.sql_parametros[i] + " LIKE @" + p_lista.sql_parametros[i] + " ";

                if (i < (m_largo - 1))
                    m_where = m_where + " AND ";

                p_lista.sql_parametros[i] = "@" + p_lista.sql_parametros[i];
            }
            this.a_sql.AppendLine(m_where);
        }

        m_reader = (SqlDataReader)this.ejecutar_query(this.a_sql.ToString(), p_lista.sql_parametros, p_lista.sql_valores);
        ArrayList m_lista = new ArrayList();

        while (m_reader.Read())
        {
            m_lista.Add(m_reader["id_asignacion_pk"].ToString());
        }
        m_reader.Dispose();

        return m_lista;
    }


    public bool existe_as_asignacion(As_asignacion p_as_asignacion)
    {
        SqlDataReader m_reader;
        this.a_sql = new StringBuilder();

        this.a_sql.AppendLine("SELECT * FROM as_asignacion WHERE ");
        this.a_sql.AppendLine("id_asignacion_pk = @id_asignacion_pk ");


        return (false);
    }

} // FIN DE LA CLASE FAC_ SISTEMA_AS_ASIGNACION
