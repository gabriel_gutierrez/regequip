/**::::::::::::::::::::::::::::::::::::::::::
 *
 * Clase: Fachada base de datos para el objeto as_detalle_linea
 * Clase responsable: sistema
 * @author Gabriel Gutierrez Carcamo <gabriel.gutierrez@emfe.cl>
 * @category Fachada base de datos de la aplicaci�n
 * @copyright Sistema de Registro de Equipos (c) 2018
 * @copyright 
 * @version 0.1 lunes, 19 de marzo de 2018 (10:30 Hr.)
 * 
 * :::::::::::::::::::::::::::::::::::::::::::*/

/** Realiza el mapeo desde la base de datos relacional a objeto as_detalle_linea */

using System.Data.SqlClient;
using System.Text;
using System.Collections;


public class  Fac_sistema_as_detalle_linea : FacadeSQLServer2005
{
 public Fac_sistema_as_detalle_linea(string p_string_conexion) : base(p_string_conexion) { } 


  /// <summary>Ingresar un objeto As_detalle_linea a la base de datos</summary>
  /// <param name="p_as_detalle_linea" >objeto as_detalle_linea</param> 
  /// <returns> Devuelve un verdadero si se ingreso correctamente, en caso contrario un falso.</returns>
  public int ingresar_as_detalle_linea(As_detalle_linea p_as_detalle_linea)
  {
       int m_reader;
       this.a_sql = new StringBuilder();
       this.a_sql.AppendLine("INSERT INTO as_detalle_linea(id_detalle_linea_pk, fecha_creacion_as, id_usuario_creador_fk, fecha_modificacion_as, id_usuario_modificador_fk, fecha_anulacion_as, id_usuario_anulador_fk) ");
       this.a_sql.AppendLine("VALUES (@id_detalle_linea_pk, @fecha_creacion_as, @id_usuario_creador_fk, @fecha_modificacion_as, @id_usuario_modificador_fk, @fecha_anulacion_as, @id_usuario_anulador_fk);");

       ArrayList m_parametros = new ArrayList();

        m_parametros.Add("@id_detalle_linea_pk");
        m_parametros.Add("@fecha_creacion_as");
        m_parametros.Add("@id_usuario_creador_fk");
        m_parametros.Add("@fecha_modificacion_as");
        m_parametros.Add("@id_usuario_modificador_fk");
        m_parametros.Add("@fecha_anulacion_as");
        m_parametros.Add("@id_usuario_anulador_fk");


       ArrayList m_valores = new ArrayList();

       m_valores.Add(p_as_detalle_linea.id_detalle_linea);
       m_valores.Add(p_as_detalle_linea.fecha_creacion_as);
       m_valores.Add(p_as_detalle_linea.id_usuario_creador);
       m_valores.Add(p_as_detalle_linea.fecha_modificacion_as);
       m_valores.Add(p_as_detalle_linea.id_usuario_modificador);
       m_valores.Add(p_as_detalle_linea.fecha_anulacion_as);
       m_valores.Add(p_as_detalle_linea.id_usuario_anulador);


       m_reader = (int)this.ejecutar_query(this.a_sql.ToString(), m_parametros, m_valores);
       return m_reader;
  }


  /// <summary>Modificar un objeto As_detalle_linea en la base de datos </summary>
  /// <param name="p_as_detalle_linea" >objeto as_detalle_linea</param> 
  /// <returns> Devuelve la cantidad de fila afectadas.</returns>
  public int modificar_as_detalle_linea(As_detalle_linea p_as_detalle_linea)
  {
       int m_reader;
       this.a_sql = new StringBuilder();
       this.a_sql.AppendLine("UPDATE as_detalle_linea SET ");


       this.a_sql.AppendLine("fecha_creacion_as = @fecha_creacion_as, ");
       this.a_sql.AppendLine("id_usuario_creador = @id_usuario_creador, ");
       this.a_sql.AppendLine("fecha_modificacion_as = @fecha_modificacion_as, ");
       this.a_sql.AppendLine("id_usuario_modificador = @id_usuario_modificador, ");
       this.a_sql.AppendLine("fecha_anulacion_as = @fecha_anulacion_as, ");
       this.a_sql.AppendLine("id_usuario_anulador = @id_usuario_anulador ");


       this.a_sql.AppendLine(" WHERE ");
       this.a_sql.AppendLine("id_detalle_linea_pk = @id_detalle_linea_pk ");


       ArrayList m_parametros = new ArrayList();


        m_parametros.Add("@id_detalle_linea_pk");
        m_parametros.Add("@fecha_creacion_as");
        m_parametros.Add("@id_usuario_creador_fk");
        m_parametros.Add("@fecha_modificacion_as");
        m_parametros.Add("@id_usuario_modificador_fk");
        m_parametros.Add("@fecha_anulacion_as");
        m_parametros.Add("@id_usuario_anulador_fk");


       ArrayList m_valores = new ArrayList();


       m_valores.Add(p_as_detalle_linea.id_detalle_linea);
       m_valores.Add(p_as_detalle_linea.fecha_creacion_as);
       m_valores.Add(p_as_detalle_linea.id_usuario_creador);
       m_valores.Add(p_as_detalle_linea.fecha_modificacion_as);
       m_valores.Add(p_as_detalle_linea.id_usuario_modificador);
       m_valores.Add(p_as_detalle_linea.fecha_anulacion_as);
       m_valores.Add(p_as_detalle_linea.id_usuario_anulador);


       m_reader = (int)this.ejecutar_query(this.a_sql.ToString(), m_parametros, m_valores);
       return m_reader;
  }


  /// <summary>Retorna un objeto As_detalle_linea desde la base de datos  </summary>
  /// <returns> Devuelve un objeto As_detalle_linea</returns>
  public As_detalle_linea get_as_detalle_linea(string p_id_detalle_linea)
  {
      SqlDataReader m_reader;
      this.a_sql = new StringBuilder();
      this.a_sql.AppendLine("SELECT * FROM as_detalle_linea WHERE ");
       this.a_sql.AppendLine("id_detalle_linea_pk = @id_detalle_linea_pk ");


       ArrayList m_parametros = new ArrayList();

        m_parametros.Add("@id_detalle_linea_pk");


       ArrayList m_valores = new ArrayList();

       m_valores.Add(p_id_detalle_linea);



      m_reader = (SqlDataReader)this.ejecutar_query(this.a_sql.ToString(), m_parametros, m_valores);

     if (m_reader.Read())
     {
       As_detalle_linea m_as_detalle_linea= new As_detalle_linea();
       m_as_detalle_linea.id_detalle_linea = m_reader["id_detalle_linea_pk"].ToString();
       m_as_detalle_linea.fecha_creacion_as = m_reader["fecha_creacion_as"].ToString();
       m_as_detalle_linea.id_usuario_creador = m_reader["id_usuario_creador_fk"].ToString();
       m_as_detalle_linea.fecha_modificacion_as = m_reader["fecha_modificacion_as"].ToString();
       m_as_detalle_linea.id_usuario_modificador = m_reader["id_usuario_modificador_fk"].ToString();
       m_as_detalle_linea.fecha_anulacion_as = m_reader["fecha_anulacion_as"].ToString();
       m_as_detalle_linea.id_usuario_anulador = m_reader["id_usuario_anulador_fk"].ToString();

        m_reader.Dispose();

        return( m_as_detalle_linea);
     }
     else
     {
        m_reader.Dispose();
        return (null);
     }
  }


  /// <summary>Retorna una lista de codigo primarios para los objetos As_detalle_linea desde la base de datos </summary>
  /// <returns> Devuelve una lista de c�digo claves del objeto </returns>
  public ArrayList get_lista_as_detalle_lineas(Lista p_lista)
  {
    SqlDataReader m_reader;
    this.a_sql = new StringBuilder();

    this.a_sql.AppendLine("SELECT * FROM as_detalle_linea");

    if (p_lista.sql_parametros.Count > 0)
       {
                int m_largo = p_lista.sql_parametros.Count;
                string m_where = " WHERE ";

                for (int i = 0; i < m_largo; i++)
                {
              m_where = m_where + p_lista.sql_parametros[i] + " LIKE @" + p_lista.sql_parametros[i] + " ";

              if (i < (m_largo-1) )
                  m_where = m_where + " AND ";

              p_lista.sql_parametros[i] = "@" + p_lista.sql_parametros[i];
                }
              this.a_sql.AppendLine(m_where);
       }

       m_reader = (SqlDataReader)this.ejecutar_query(this.a_sql.ToString(), p_lista.sql_parametros, p_lista.sql_valores);
       ArrayList m_lista = new ArrayList();

       while (m_reader.Read())
       {
       m_lista.Add(m_reader["id_detalle_linea_pk"].ToString());
       }
       m_reader.Dispose();

       return m_lista;
  }


  public bool existe_as_detalle_linea(As_detalle_linea p_as_detalle_linea)
  {
    SqlDataReader m_reader;
    this.a_sql = new StringBuilder();
    
    this.a_sql.AppendLine("SELECT * FROM as_detalle_linea WHERE ");
       this.a_sql.AppendLine("id_detalle_linea_pk = @id_detalle_linea_pk ");

    
    return(false);
  }

} // FIN DE LA CLASE FAC_ SISTEMA_AS_DETALLE_LINEA
