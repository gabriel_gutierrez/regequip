/**::::::::::::::::::::::::::::::::::::::::::
 *
 * Clase: Fachada base de datos para el objeto as_momentanea
 * Clase responsable: sistema
 * @author Gabriel Gutierrez Carcamo <gabriel.gutierrez@emfe.cl>
 * @category Fachada base de datos de la aplicaci�n
 * @copyright Sistema de Registro de Equipos (c) 2018
 * @copyright 
 * @version 0.1 lunes, 19 de marzo de 2018 (10:29 Hr.)
 * 
 * :::::::::::::::::::::::::::::::::::::::::::*/

/** Realiza el mapeo desde la base de datos relacional a objeto as_momentanea */

using System.Data.SqlClient;
using System.Text;
using System.Collections;


public class  Fac_sistema_as_momentanea : FacadeSQLServer2005
{
 public Fac_sistema_as_momentanea(string p_string_conexion) : base(p_string_conexion) { } 


  /// <summary>Ingresar un objeto As_momentanea a la base de datos</summary>
  /// <param name="p_as_momentanea" >objeto as_momentanea</param> 
  /// <returns> Devuelve un verdadero si se ingreso correctamente, en caso contrario un falso.</returns>
  public int ingresar_as_momentanea(As_momentanea p_as_momentanea)
  {
       int m_reader;
       this.a_sql = new StringBuilder();
       this.a_sql.AppendLine("INSERT INTO as_momentanea(id_momentanea_pk, fecha_vencimiento, valido) ");
       this.a_sql.AppendLine("VALUES (@id_momentanea_pk, @fecha_vencimiento, @valido);");

       ArrayList m_parametros = new ArrayList();

        m_parametros.Add("@id_momentanea_pk");
        m_parametros.Add("@fecha_vencimiento");
        m_parametros.Add("@valido");


       ArrayList m_valores = new ArrayList();

       m_valores.Add(p_as_momentanea.id_momentanea);
       m_valores.Add(p_as_momentanea.fecha_vencimiento);
       m_valores.Add(p_as_momentanea.valido);


       m_reader = (int)this.ejecutar_query(this.a_sql.ToString(), m_parametros, m_valores);
       return m_reader;
  }


  /// <summary>Modificar un objeto As_momentanea en la base de datos </summary>
  /// <param name="p_as_momentanea" >objeto as_momentanea</param> 
  /// <returns> Devuelve la cantidad de fila afectadas.</returns>
  public int modificar_as_momentanea(As_momentanea p_as_momentanea)
  {
       int m_reader;
       this.a_sql = new StringBuilder();
       this.a_sql.AppendLine("UPDATE as_momentanea SET ");


       this.a_sql.AppendLine("fecha_vencimiento = @fecha_vencimiento, ");
       this.a_sql.AppendLine("valido = @valido ");


       this.a_sql.AppendLine(" WHERE ");
       this.a_sql.AppendLine("id_momentanea_pk = @id_momentanea_pk ");


       ArrayList m_parametros = new ArrayList();


        m_parametros.Add("@id_momentanea_pk");
        m_parametros.Add("@fecha_vencimiento");
        m_parametros.Add("@valido");


       ArrayList m_valores = new ArrayList();


       m_valores.Add(p_as_momentanea.id_momentanea);
       m_valores.Add(p_as_momentanea.fecha_vencimiento);
       m_valores.Add(p_as_momentanea.valido);


       m_reader = (int)this.ejecutar_query(this.a_sql.ToString(), m_parametros, m_valores);
       return m_reader;
  }


  /// <summary>Retorna un objeto As_momentanea desde la base de datos  </summary>
  /// <returns> Devuelve un objeto As_momentanea</returns>
  public As_momentanea get_as_momentanea(string p_id_momentanea)
  {
      SqlDataReader m_reader;
      this.a_sql = new StringBuilder();
      this.a_sql.AppendLine("SELECT * FROM as_momentanea WHERE ");
       this.a_sql.AppendLine("id_momentanea_pk = @id_momentanea_pk ");


       ArrayList m_parametros = new ArrayList();

        m_parametros.Add("@id_momentanea_pk");


       ArrayList m_valores = new ArrayList();

       m_valores.Add(p_id_momentanea);



      m_reader = (SqlDataReader)this.ejecutar_query(this.a_sql.ToString(), m_parametros, m_valores);

     if (m_reader.Read())
     {
       As_momentanea m_as_momentanea= new As_momentanea();
       m_as_momentanea.id_momentanea = m_reader["id_momentanea_pk"].ToString();
       m_as_momentanea.fecha_vencimiento = m_reader["fecha_vencimiento"].ToString();
       m_as_momentanea.valido = m_reader["valido"].ToString();

        m_reader.Dispose();

        return( m_as_momentanea);
     }
     else
     {
        m_reader.Dispose();
        return (null);
     }
  }


  /// <summary>Retorna una lista de codigo primarios para los objetos As_momentanea desde la base de datos </summary>
  /// <returns> Devuelve una lista de c�digo claves del objeto </returns>
  public ArrayList get_lista_as_momentaneas(Lista p_lista)
  {
    SqlDataReader m_reader;
    this.a_sql = new StringBuilder();

    this.a_sql.AppendLine("SELECT * FROM as_momentanea");

    if (p_lista.sql_parametros.Count > 0)
       {
                int m_largo = p_lista.sql_parametros.Count;
                string m_where = " WHERE ";

                for (int i = 0; i < m_largo; i++)
                {
              m_where = m_where + p_lista.sql_parametros[i] + " LIKE @" + p_lista.sql_parametros[i] + " ";

              if (i < (m_largo-1) )
                  m_where = m_where + " AND ";

              p_lista.sql_parametros[i] = "@" + p_lista.sql_parametros[i];
                }
              this.a_sql.AppendLine(m_where);
       }

       m_reader = (SqlDataReader)this.ejecutar_query(this.a_sql.ToString(), p_lista.sql_parametros, p_lista.sql_valores);
       ArrayList m_lista = new ArrayList();

       while (m_reader.Read())
       {
       m_lista.Add(m_reader["id_momentanea_pk"].ToString());
       }
       m_reader.Dispose();

       return m_lista;
  }


  public bool existe_as_momentanea(As_momentanea p_as_momentanea)
  {
    SqlDataReader m_reader;
    this.a_sql = new StringBuilder();
    
    this.a_sql.AppendLine("SELECT * FROM as_momentanea WHERE ");
       this.a_sql.AppendLine("id_momentanea_pk = @id_momentanea_pk ");

    
    return(false);
  }

} // FIN DE LA CLASE FAC_ SISTEMA_AS_MOMENTANEA
