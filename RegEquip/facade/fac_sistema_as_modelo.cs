/**::::::::::::::::::::::::::::::::::::::::::
 *
 * Clase: Fachada base de datos para el objeto as_modelo
 * Clase responsable: sistema
 * @author Gabriel Gutierrez Carcamo <gabriel.gutierrez@emfe.cl>
 * @category Fachada base de datos de la aplicaci�n
 * @copyright Sistema de Registro de Equipos (c) 2018
 * @copyright 
 * @version 0.1 lunes, 19 de marzo de 2018 (10:31 Hr.)
 * 
 * :::::::::::::::::::::::::::::::::::::::::::*/

/** Realiza el mapeo desde la base de datos relacional a objeto as_modelo */

using System.Data.SqlClient;
using System.Text;
using System.Collections;
using System.Data;

public class Fac_sistema_as_modelo : FacadeSQLServer2005
{
    public Fac_sistema_as_modelo(string p_string_conexion) : base(p_string_conexion) { }


    /// <summary>Ingresar un objeto As_modelo a la base de datos</summary>
    /// <param name="p_as_modelo" >objeto as_modelo</param> 
    /// <returns> Devuelve un verdadero si se ingreso correctamente, en caso contrario un falso.</returns>
    public int ingresar_as_modelo(As_modelo p_as_modelo)
    {
        int m_reader;
        this.a_sql = new StringBuilder();
        this.a_sql.AppendLine("INSERT INTO as_modelo(id_modelo_pk, descripcion_modelo, id_detalle_linea_fk, valido) ");
        this.a_sql.AppendLine("VALUES (@id_modelo_pk, @descripcion_modelo, @id_detalle_linea_fk, @valido);");

        ArrayList m_parametros = new ArrayList();

        m_parametros.Add("@id_modelo_pk");
        m_parametros.Add("@descripcion_modelo");
        m_parametros.Add("@id_detalle_linea_fk");
        m_parametros.Add("@valido");


        ArrayList m_valores = new ArrayList();

        m_valores.Add(p_as_modelo.id_modelo);
        m_valores.Add(p_as_modelo.descripcion_modelo);
        m_valores.Add(p_as_modelo.id_detalle_linea);
        m_valores.Add(p_as_modelo.valido);


        m_reader = (int)this.ejecutar_query(this.a_sql.ToString(), m_parametros, m_valores);
        return m_reader;
    }


    /// <summary>Modificar un objeto As_modelo en la base de datos </summary>
    /// <param name="p_as_modelo" >objeto as_modelo</param> 
    /// <returns> Devuelve la cantidad de fila afectadas.</returns>
    public int modificar_as_modelo(As_modelo p_as_modelo)
    {
        int m_reader;
        this.a_sql = new StringBuilder();
        this.a_sql.AppendLine("UPDATE as_modelo SET ");


        this.a_sql.AppendLine("descripcion_modelo = @descripcion_modelo, ");
        this.a_sql.AppendLine("id_detalle_linea = @id_detalle_linea, ");
        this.a_sql.AppendLine("valido = @valido ");


        this.a_sql.AppendLine(" WHERE ");
        this.a_sql.AppendLine("id_modelo_pk = @id_modelo_pk ");


        ArrayList m_parametros = new ArrayList();


        m_parametros.Add("@id_modelo_pk");
        m_parametros.Add("@descripcion_modelo");
        m_parametros.Add("@id_detalle_linea_fk");
        m_parametros.Add("@valido");


        ArrayList m_valores = new ArrayList();


        m_valores.Add(p_as_modelo.id_modelo);
        m_valores.Add(p_as_modelo.descripcion_modelo);
        m_valores.Add(p_as_modelo.id_detalle_linea);
        m_valores.Add(p_as_modelo.valido);


        m_reader = (int)this.ejecutar_query(this.a_sql.ToString(), m_parametros, m_valores);
        return m_reader;
    }


    /// <summary>Retorna un objeto As_modelo desde la base de datos  </summary>
    /// <returns> Devuelve un objeto As_modelo</returns>
    public As_modelo get_as_modelo(string p_id_modelo)
    {
        SqlDataReader m_reader;
        this.a_sql = new StringBuilder();
        this.a_sql.AppendLine("SELECT * FROM as_modelo WHERE ");
        this.a_sql.AppendLine("id_modelo_pk = @id_modelo_pk ");


        ArrayList m_parametros = new ArrayList();

        m_parametros.Add("@id_modelo_pk");


        ArrayList m_valores = new ArrayList();

        m_valores.Add(p_id_modelo);



        m_reader = (SqlDataReader)this.ejecutar_query(this.a_sql.ToString(), m_parametros, m_valores);

        if (m_reader.Read())
        {
            As_modelo m_as_modelo = new As_modelo();
            m_as_modelo.id_modelo = m_reader["id_modelo_pk"].ToString();
            m_as_modelo.descripcion_modelo = m_reader["descripcion_modelo"].ToString();
            m_as_modelo.id_detalle_linea = m_reader["id_detalle_linea_fk"].ToString();
            m_as_modelo.valido = m_reader["valido"].ToString();

            m_reader.Dispose();

            return (m_as_modelo);
        }
        else
        {
            m_reader.Dispose();
            return (null);
        }
    }


    /// <summary>Retorna una lista de codigo primarios para los objetos As_modelo desde la base de datos </summary>
    /// <returns> Devuelve una lista de c�digo claves del objeto </returns>
    public ArrayList get_lista_as_modelos(Lista p_lista)
    {
        SqlDataReader m_reader;
        this.a_sql = new StringBuilder();

        this.a_sql.AppendLine("SELECT * FROM as_modelo");

        if (p_lista.sql_parametros.Count > 0)
        {
            int m_largo = p_lista.sql_parametros.Count;
            string m_where = " WHERE ";

            for (int i = 0; i < m_largo; i++)
            {
                m_where = m_where + p_lista.sql_parametros[i] + " LIKE @" + p_lista.sql_parametros[i] + " ";

                if (i < (m_largo - 1))
                    m_where = m_where + " AND ";

                p_lista.sql_parametros[i] = "@" + p_lista.sql_parametros[i];
            }
            this.a_sql.AppendLine(m_where);
        }

        m_reader = (SqlDataReader)this.ejecutar_query(this.a_sql.ToString(), p_lista.sql_parametros, p_lista.sql_valores);
        ArrayList m_lista = new ArrayList();

        while (m_reader.Read())
        {
            m_lista.Add(m_reader["id_modelo_pk"].ToString());
        }
        m_reader.Dispose();

        return m_lista;
    }

    public DataSet get_lista_as_modelos_ds(Lista p_lista)
    {
        SqlDataReader m_reader;
        this.a_sql = new StringBuilder();

        this.a_sql.AppendLine("SELECT * FROM as_modelo");

        if (p_lista.sql_parametros.Count > 0)
        {
            int m_largo = p_lista.sql_parametros.Count;
            string m_where = " WHERE ";

            for (int i = 0; i < m_largo; i++)
            {
                m_where = m_where + p_lista.sql_parametros[i] + " LIKE @" + p_lista.sql_parametros[i] + " ";

                if (i < (m_largo - 1))
                    m_where = m_where + " AND ";

                p_lista.sql_parametros[i] = "@" + p_lista.sql_parametros[i];
            }
            this.a_sql.AppendLine(m_where);
        }
        this.a_sql.AppendLine(" ORDER BY descripcion_modelo");

        m_reader = (SqlDataReader)this.ejecutar_query(this.a_sql.ToString(), p_lista.sql_parametros, p_lista.sql_valores);
        DataTable dt = new DataTable();
        DataSet m_lista = new DataSet();

        if (m_reader != null)
        {
            dt.Load(m_reader);
            m_lista.Tables.Add(dt);
        }
        m_reader.Dispose();

        return m_lista;
    }


    public bool existe_as_modelo(As_modelo p_as_modelo)
    {
        SqlDataReader m_reader;
        this.a_sql = new StringBuilder();

        this.a_sql.AppendLine("SELECT * FROM as_modelo WHERE ");
        this.a_sql.AppendLine("id_modelo_pk = @id_modelo_pk ");


        return (false);
    }

} // FIN DE LA CLASE FAC_ SISTEMA_AS_MODELO
