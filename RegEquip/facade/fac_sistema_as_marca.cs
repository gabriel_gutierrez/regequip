/**::::::::::::::::::::::::::::::::::::::::::
 *
 * Clase: Fachada base de datos para el objeto as_marca
 * Clase responsable: sistema
 * @author Gabriel Gutierrez Carcamo <gabriel.gutierrez@emfe.cl>
 * @category Fachada base de datos de la aplicaci�n
 * @copyright Sistema de Registro de Equipos (c) 2018
 * @copyright 
 * @version 0.1 lunes, 19 de marzo de 2018 (10:31 Hr.)
 * 
 * :::::::::::::::::::::::::::::::::::::::::::*/

/** Realiza el mapeo desde la base de datos relacional a objeto as_marca */

using System.Data.SqlClient;
using System.Text;
using System.Collections;
using System.Data;

public class  Fac_sistema_as_marca : FacadeSQLServer2005
{
 public Fac_sistema_as_marca(string p_string_conexion) : base(p_string_conexion) { } 


  /// <summary>Ingresar un objeto As_marca a la base de datos</summary>
  /// <param name="p_as_marca" >objeto as_marca</param> 
  /// <returns> Devuelve un verdadero si se ingreso correctamente, en caso contrario un falso.</returns>
  public int ingresar_as_marca(As_marca p_as_marca)
  {
       int m_reader;
       this.a_sql = new StringBuilder();
       this.a_sql.AppendLine("INSERT INTO as_marca(id_marca_pk, descripcion_marca, id_detalle_linea_fk, valido) ");
       this.a_sql.AppendLine("VALUES (@id_marca_pk, @descripcion_marca, @id_detalle_linea_fk, @valido);");

       ArrayList m_parametros = new ArrayList();

        m_parametros.Add("@id_marca_pk");
        m_parametros.Add("@descripcion_marca");
        m_parametros.Add("@id_detalle_linea_fk");
        m_parametros.Add("@valido");


       ArrayList m_valores = new ArrayList();

       m_valores.Add(p_as_marca.id_marca);
       m_valores.Add(p_as_marca.descripcion_marca);
       m_valores.Add(p_as_marca.id_detalle_linea);
       m_valores.Add(p_as_marca.valido);


       m_reader = (int)this.ejecutar_query(this.a_sql.ToString(), m_parametros, m_valores);
       return m_reader;
  }


  /// <summary>Modificar un objeto As_marca en la base de datos </summary>
  /// <param name="p_as_marca" >objeto as_marca</param> 
  /// <returns> Devuelve la cantidad de fila afectadas.</returns>
  public int modificar_as_marca(As_marca p_as_marca)
  {
       int m_reader;
       this.a_sql = new StringBuilder();
       this.a_sql.AppendLine("UPDATE as_marca SET ");


       this.a_sql.AppendLine("descripcion_marca = @descripcion_marca, ");
       this.a_sql.AppendLine("id_detalle_linea = @id_detalle_linea, ");
       this.a_sql.AppendLine("valido = @valido ");


       this.a_sql.AppendLine(" WHERE ");
       this.a_sql.AppendLine("id_marca_pk = @id_marca_pk ");


       ArrayList m_parametros = new ArrayList();


        m_parametros.Add("@id_marca_pk");
        m_parametros.Add("@descripcion_marca");
        m_parametros.Add("@id_detalle_linea_fk");
        m_parametros.Add("@valido");


       ArrayList m_valores = new ArrayList();


       m_valores.Add(p_as_marca.id_marca);
       m_valores.Add(p_as_marca.descripcion_marca);
       m_valores.Add(p_as_marca.id_detalle_linea);
       m_valores.Add(p_as_marca.valido);


       m_reader = (int)this.ejecutar_query(this.a_sql.ToString(), m_parametros, m_valores);
       return m_reader;
  }


  /// <summary>Retorna un objeto As_marca desde la base de datos  </summary>
  /// <returns> Devuelve un objeto As_marca</returns>
  public As_marca get_as_marca(string p_id_marca)
  {
      SqlDataReader m_reader;
      this.a_sql = new StringBuilder();
      this.a_sql.AppendLine("SELECT * FROM as_marca WHERE ");
       this.a_sql.AppendLine("id_marca_pk = @id_marca_pk ");


       ArrayList m_parametros = new ArrayList();

        m_parametros.Add("@id_marca_pk");


       ArrayList m_valores = new ArrayList();

       m_valores.Add(p_id_marca);



      m_reader = (SqlDataReader)this.ejecutar_query(this.a_sql.ToString(), m_parametros, m_valores);

     if (m_reader.Read())
     {
       As_marca m_as_marca= new As_marca();
       m_as_marca.id_marca = m_reader["id_marca_pk"].ToString();
       m_as_marca.descripcion_marca = m_reader["descripcion_marca"].ToString();
       m_as_marca.id_detalle_linea = m_reader["id_detalle_linea_fk"].ToString();
       m_as_marca.valido = m_reader["valido"].ToString();

        m_reader.Dispose();

        return( m_as_marca);
     }
     else
     {
        m_reader.Dispose();
        return (null);
     }
  }


  /// <summary>Retorna una lista de codigo primarios para los objetos As_marca desde la base de datos </summary>
  /// <returns> Devuelve una lista de c�digo claves del objeto </returns>
  public ArrayList get_lista_as_marcas(Lista p_lista)
  {
    SqlDataReader m_reader;
    this.a_sql = new StringBuilder();

    this.a_sql.AppendLine("SELECT * FROM as_marca");

    if (p_lista.sql_parametros.Count > 0)
       {
                int m_largo = p_lista.sql_parametros.Count;
                string m_where = " WHERE ";

                for (int i = 0; i < m_largo; i++)
                {
              m_where = m_where + p_lista.sql_parametros[i] + " LIKE @" + p_lista.sql_parametros[i] + " ";

              if (i < (m_largo-1) )
                  m_where = m_where + " AND ";

              p_lista.sql_parametros[i] = "@" + p_lista.sql_parametros[i];
                }
              this.a_sql.AppendLine(m_where);
       }

       m_reader = (SqlDataReader)this.ejecutar_query(this.a_sql.ToString(), p_lista.sql_parametros, p_lista.sql_valores);
       ArrayList m_lista = new ArrayList();

       while (m_reader.Read())
       {
       m_lista.Add(m_reader["id_marca_pk"].ToString());
       }
       m_reader.Dispose();

       return m_lista;
  }

    public DataSet get_lista_as_marcas_ds(Lista p_lista)
    {
        SqlDataReader m_reader;
        this.a_sql = new StringBuilder();

        this.a_sql.AppendLine("SELECT * FROM as_marca");

        if (p_lista.sql_parametros.Count > 0)
        {
            int m_largo = p_lista.sql_parametros.Count;
            string m_where = " WHERE ";

            for (int i = 0; i < m_largo; i++)
            {
                m_where = m_where + p_lista.sql_parametros[i] + " LIKE @" + p_lista.sql_parametros[i] + " ";

                if (i < (m_largo - 1))
                    m_where = m_where + " AND ";

                p_lista.sql_parametros[i] = "@" + p_lista.sql_parametros[i];
            }
            this.a_sql.AppendLine(m_where);
        }
        this.a_sql.AppendLine(" ORDER BY descripcion_marca");

        m_reader = (SqlDataReader)this.ejecutar_query(this.a_sql.ToString(), p_lista.sql_parametros, p_lista.sql_valores);
        DataTable dt = new DataTable();
        DataSet m_lista = new DataSet();

        if (m_reader != null)
        {
            dt.Load(m_reader);
            m_lista.Tables.Add(dt);
            m_reader.Close();
        }
        
        m_reader.Dispose();

        return m_lista;
    }


    public bool existe_as_marca(As_marca p_as_marca)
  {
    SqlDataReader m_reader;
    this.a_sql = new StringBuilder();
    
    this.a_sql.AppendLine("SELECT * FROM as_marca WHERE ");
       this.a_sql.AppendLine("id_marca_pk = @id_marca_pk ");

    
    return(false);
  }

} // FIN DE LA CLASE FAC_ SISTEMA_AS_MARCA
