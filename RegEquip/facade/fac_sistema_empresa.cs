/**::::::::::::::::::::::::::::::::::::::::::
 *
 * Clase: Fachada base de datos para el objeto empresa
 * Clase responsable: sistema
 * @author Gabriel Gutierrez Carcamo <gabriel.gutierrez@emfe.cl>
 * @category Fachada base de datos de la aplicaci�n
 * @copyright Sistema de Registro de Equipos (c) 2018
 * @copyright 
 * @version 0.1 lunes, 19 de marzo de 2018 (12:10 Hr.)
 * 
 * :::::::::::::::::::::::::::::::::::::::::::*/

/** Realiza el mapeo desde la base de datos relacional a objeto empresa */

using System.Data.SqlClient;
using System.Text;
using System.Collections;


public class  Fac_sistema_empresa : FacadeSQLServer2005
{
 public Fac_sistema_empresa(string p_string_conexion) : base(p_string_conexion) { } 


  /// <summary>Ingresar un objeto Empresa a la base de datos</summary>
  /// <param name="p_empresa" >objeto empresa</param> 
  /// <returns> Devuelve un verdadero si se ingreso correctamente, en caso contrario un falso.</returns>
  public int ingresar_empresa(Empresa p_empresa)
  {
       int m_reader;
       this.a_sql = new StringBuilder();
       this.a_sql.AppendLine("INSERT INTO empresa(id_empresa_pk, id_estado_fk, descripcion, direccion, razon_social, rut, telefono, sitioweb, url_imagen, esExportada, codigoSAP1) ");
       this.a_sql.AppendLine("VALUES (@id_empresa_pk, @id_estado_fk, @descripcion, @direccion, @razon_social, @rut, @telefono, @sitioweb, @url_imagen, @esExportada, @codigoSAP1);");

       ArrayList m_parametros = new ArrayList();

        m_parametros.Add("@id_empresa_pk");
        m_parametros.Add("@id_estado_fk");
        m_parametros.Add("@descripcion");
        m_parametros.Add("@direccion");
        m_parametros.Add("@razon_social");
        m_parametros.Add("@rut");
        m_parametros.Add("@telefono");
        m_parametros.Add("@sitioweb");
        m_parametros.Add("@url_imagen");
        m_parametros.Add("@esExportada");
        m_parametros.Add("@codigoSAP1");


       ArrayList m_valores = new ArrayList();

       m_valores.Add(p_empresa.id_empresa);
       m_valores.Add(p_empresa.id_estado);
       m_valores.Add(p_empresa.descripcion);
       m_valores.Add(p_empresa.direccion);
       m_valores.Add(p_empresa.razon_social);
       m_valores.Add(p_empresa.rut);
       m_valores.Add(p_empresa.telefono);
       m_valores.Add(p_empresa.sitioweb);
       m_valores.Add(p_empresa.url_imagen);
       m_valores.Add(p_empresa.esExportada);
       m_valores.Add(p_empresa.codigoSAP1);


       m_reader = (int)this.ejecutar_query(this.a_sql.ToString(), m_parametros, m_valores);
       return m_reader;
  }


  /// <summary>Modificar un objeto Empresa en la base de datos </summary>
  /// <param name="p_empresa" >objeto empresa</param> 
  /// <returns> Devuelve la cantidad de fila afectadas.</returns>
  public int modificar_empresa(Empresa p_empresa)
  {
       int m_reader;
       this.a_sql = new StringBuilder();
       this.a_sql.AppendLine("UPDATE empresa SET ");


       this.a_sql.AppendLine("id_estado = @id_estado, ");
       this.a_sql.AppendLine("descripcion = @descripcion, ");
       this.a_sql.AppendLine("direccion = @direccion, ");
       this.a_sql.AppendLine("razon_social = @razon_social, ");
       this.a_sql.AppendLine("rut = @rut, ");
       this.a_sql.AppendLine("telefono = @telefono, ");
       this.a_sql.AppendLine("sitioweb = @sitioweb, ");
       this.a_sql.AppendLine("url_imagen = @url_imagen, ");
       this.a_sql.AppendLine("esExportada = @esExportada, ");
       this.a_sql.AppendLine("codigoSAP1 = @codigoSAP1 ");


       this.a_sql.AppendLine(" WHERE ");
       this.a_sql.AppendLine("id_empresa_pk = @id_empresa_pk ");


       ArrayList m_parametros = new ArrayList();


        m_parametros.Add("@id_empresa_pk");
        m_parametros.Add("@id_estado_fk");
        m_parametros.Add("@descripcion");
        m_parametros.Add("@direccion");
        m_parametros.Add("@razon_social");
        m_parametros.Add("@rut");
        m_parametros.Add("@telefono");
        m_parametros.Add("@sitioweb");
        m_parametros.Add("@url_imagen");
        m_parametros.Add("@esExportada");
        m_parametros.Add("@codigoSAP1");


       ArrayList m_valores = new ArrayList();


       m_valores.Add(p_empresa.id_empresa);
       m_valores.Add(p_empresa.id_estado);
       m_valores.Add(p_empresa.descripcion);
       m_valores.Add(p_empresa.direccion);
       m_valores.Add(p_empresa.razon_social);
       m_valores.Add(p_empresa.rut);
       m_valores.Add(p_empresa.telefono);
       m_valores.Add(p_empresa.sitioweb);
       m_valores.Add(p_empresa.url_imagen);
       m_valores.Add(p_empresa.esExportada);
       m_valores.Add(p_empresa.codigoSAP1);


       m_reader = (int)this.ejecutar_query(this.a_sql.ToString(), m_parametros, m_valores);
       return m_reader;
  }


  /// <summary>Retorna un objeto Empresa desde la base de datos  </summary>
  /// <returns> Devuelve un objeto Empresa</returns>
  public Empresa get_empresa(string p_id_empresa)
  {
      SqlDataReader m_reader;
      this.a_sql = new StringBuilder();
      this.a_sql.AppendLine("SELECT * FROM empresa WHERE ");
       this.a_sql.AppendLine("id_empresa_pk = @id_empresa_pk ");


       ArrayList m_parametros = new ArrayList();

        m_parametros.Add("@id_empresa_pk");


       ArrayList m_valores = new ArrayList();

       m_valores.Add(p_id_empresa);



      m_reader = (SqlDataReader)this.ejecutar_query(this.a_sql.ToString(), m_parametros, m_valores);

     if (m_reader.Read())
     {
       Empresa m_empresa= new Empresa();
       m_empresa.id_empresa = m_reader["id_empresa_pk"].ToString();
       m_empresa.id_estado = m_reader["id_estado_fk"].ToString();
       m_empresa.descripcion = m_reader["descripcion"].ToString();
       m_empresa.direccion = m_reader["direccion"].ToString();
       m_empresa.razon_social = m_reader["razon_social"].ToString();
       m_empresa.rut = m_reader["rut"].ToString();
       m_empresa.telefono = m_reader["telefono"].ToString();
       m_empresa.sitioweb = m_reader["sitioweb"].ToString();
       m_empresa.url_imagen = m_reader["url_imagen"].ToString();
       m_empresa.esExportada = m_reader["esExportada"].ToString();
       m_empresa.codigoSAP1 = m_reader["codigoSAP1"].ToString();

        m_reader.Dispose();

        return( m_empresa);
     }
     else
     {
        m_reader.Dispose();
        return (null);
     }
  }


  /// <summary>Retorna una lista de codigo primarios para los objetos Empresa desde la base de datos </summary>
  /// <returns> Devuelve una lista de c�digo claves del objeto </returns>
  public ArrayList get_lista_empresas(Lista p_lista)
  {
    SqlDataReader m_reader;
    this.a_sql = new StringBuilder();

    this.a_sql.AppendLine("SELECT * FROM empresa");

    if (p_lista.sql_parametros.Count > 0)
       {
                int m_largo = p_lista.sql_parametros.Count;
                string m_where = " WHERE ";

                for (int i = 0; i < m_largo; i++)
                {
              m_where = m_where + p_lista.sql_parametros[i] + " LIKE @" + p_lista.sql_parametros[i] + " ";

              if (i < (m_largo-1) )
                  m_where = m_where + " AND ";

              p_lista.sql_parametros[i] = "@" + p_lista.sql_parametros[i];
                }
              this.a_sql.AppendLine(m_where);
       }

       m_reader = (SqlDataReader)this.ejecutar_query(this.a_sql.ToString(), p_lista.sql_parametros, p_lista.sql_valores);
       ArrayList m_lista = new ArrayList();

       while (m_reader.Read())
       {
       m_lista.Add(m_reader["id_empresa_pk"].ToString());
       }
       m_reader.Dispose();

       return m_lista;
  }


  public bool existe_empresa(Empresa p_empresa)
  {
    SqlDataReader m_reader;
    this.a_sql = new StringBuilder();
    
    this.a_sql.AppendLine("SELECT * FROM empresa WHERE ");
       this.a_sql.AppendLine("id_empresa_pk = @id_empresa_pk ");

    
    return(false);
  }

} // FIN DE LA CLASE FAC_ SISTEMA_EMPRESA
