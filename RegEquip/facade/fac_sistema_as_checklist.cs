/**::::::::::::::::::::::::::::::::::::::::::
 *
 * Clase: Fachada base de datos para el objeto as_checklist
 * Clase responsable: sistema
 * @author Gabriel Gutierrez Carcamo <gabriel.gutierrez@emfe.cl>
 * @category Fachada base de datos de la aplicaci�n
 * @copyright Sistema de Registro de Equipos (c) 2018
 * @copyright 
 * @version 0.1 lunes, 19 de marzo de 2018 (10:33 Hr.)
 * 
 * :::::::::::::::::::::::::::::::::::::::::::*/

/** Realiza el mapeo desde la base de datos relacional a objeto as_checklist */

using System.Data.SqlClient;
using System.Text;
using System.Collections;


public class  Fac_sistema_as_checklist : FacadeSQLServer2005
{
 public Fac_sistema_as_checklist(string p_string_conexion) : base(p_string_conexion) { } 


  /// <summary>Ingresar un objeto As_checklist a la base de datos</summary>
  /// <param name="p_as_checklist" >objeto as_checklist</param> 
  /// <returns> Devuelve un verdadero si se ingreso correctamente, en caso contrario un falso.</returns>
  public int ingresar_as_checklist(As_checklist p_as_checklist)
  {
       int m_reader;
       this.a_sql = new StringBuilder();
       this.a_sql.AppendLine("INSERT INTO as_checklist(id_checklist_pk, chk_a, chk_b, chk_c, chk_d, chk_e, chk_f, chk_g, chk_h, chk_i, chk_j) ");
       this.a_sql.AppendLine("VALUES (@id_checklist_pk, @chk_a, @chk_b, @chk_c, @chk_d, @chk_e, @chk_f, @chk_g, @chk_h, @chk_i, @chk_j);");

       ArrayList m_parametros = new ArrayList();

        m_parametros.Add("@id_checklist_pk");
        m_parametros.Add("@chk_a");
        m_parametros.Add("@chk_b");
        m_parametros.Add("@chk_c");
        m_parametros.Add("@chk_d");
        m_parametros.Add("@chk_e");
        m_parametros.Add("@chk_f");
        m_parametros.Add("@chk_g");
        m_parametros.Add("@chk_h");
        m_parametros.Add("@chk_i");
        m_parametros.Add("@chk_j");


       ArrayList m_valores = new ArrayList();

       m_valores.Add(p_as_checklist.id_checklist);
       m_valores.Add(p_as_checklist.chk_a);
       m_valores.Add(p_as_checklist.chk_b);
       m_valores.Add(p_as_checklist.chk_c);
       m_valores.Add(p_as_checklist.chk_d);
       m_valores.Add(p_as_checklist.chk_e);
       m_valores.Add(p_as_checklist.chk_f);
       m_valores.Add(p_as_checklist.chk_g);
       m_valores.Add(p_as_checklist.chk_h);
       m_valores.Add(p_as_checklist.chk_i);
       m_valores.Add(p_as_checklist.chk_j);


       m_reader = (int)this.ejecutar_query(this.a_sql.ToString(), m_parametros, m_valores);
       return m_reader;
  }


  /// <summary>Modificar un objeto As_checklist en la base de datos </summary>
  /// <param name="p_as_checklist" >objeto as_checklist</param> 
  /// <returns> Devuelve la cantidad de fila afectadas.</returns>
  public int modificar_as_checklist(As_checklist p_as_checklist)
  {
       int m_reader;
       this.a_sql = new StringBuilder();
       this.a_sql.AppendLine("UPDATE as_checklist SET ");
               
       this.a_sql.AppendLine("chk_a = @chk_a, ");
       this.a_sql.AppendLine("chk_b = @chk_b, ");
       this.a_sql.AppendLine("chk_c = @chk_c, ");
       this.a_sql.AppendLine("chk_d = @chk_d, ");
       this.a_sql.AppendLine("chk_e = @chk_e, ");
       this.a_sql.AppendLine("chk_f = @chk_f, ");
       this.a_sql.AppendLine("chk_g = @chk_g, ");
       this.a_sql.AppendLine("chk_h = @chk_h, ");
       this.a_sql.AppendLine("chk_i = @chk_i, ");
       this.a_sql.AppendLine("chk_j = @chk_j ");


       this.a_sql.AppendLine(" WHERE ");
       this.a_sql.AppendLine("id_checklist_pk = @id_checklist_pk ");


       ArrayList m_parametros = new ArrayList();


        m_parametros.Add("@id_checklist_pk");
        m_parametros.Add("@chk_a");
        m_parametros.Add("@chk_b");
        m_parametros.Add("@chk_c");
        m_parametros.Add("@chk_d");
        m_parametros.Add("@chk_e");
        m_parametros.Add("@chk_f");
        m_parametros.Add("@chk_g");
        m_parametros.Add("@chk_h");
        m_parametros.Add("@chk_i");
        m_parametros.Add("@chk_j");


       ArrayList m_valores = new ArrayList();


       m_valores.Add(p_as_checklist.id_checklist);
       m_valores.Add(p_as_checklist.chk_a);
       m_valores.Add(p_as_checklist.chk_b);
       m_valores.Add(p_as_checklist.chk_c);
       m_valores.Add(p_as_checklist.chk_d);
       m_valores.Add(p_as_checklist.chk_e);
       m_valores.Add(p_as_checklist.chk_f);
       m_valores.Add(p_as_checklist.chk_g);
       m_valores.Add(p_as_checklist.chk_h);
       m_valores.Add(p_as_checklist.chk_i);
       m_valores.Add(p_as_checklist.chk_j);


       m_reader = (int)this.ejecutar_query(this.a_sql.ToString(), m_parametros, m_valores);
       return m_reader;
  }


  /// <summary>Retorna un objeto As_checklist desde la base de datos  </summary>
  /// <returns> Devuelve un objeto As_checklist</returns>
  public As_checklist get_as_checklist(string p_id_checklist)
  {
      SqlDataReader m_reader;
      this.a_sql = new StringBuilder();
      this.a_sql.AppendLine("SELECT * FROM as_checklist WHERE ");
       this.a_sql.AppendLine("id_checklist_pk = @id_checklist_pk ");


       ArrayList m_parametros = new ArrayList();

        m_parametros.Add("@id_checklist_pk");


       ArrayList m_valores = new ArrayList();

       m_valores.Add(p_id_checklist);



      m_reader = (SqlDataReader)this.ejecutar_query(this.a_sql.ToString(), m_parametros, m_valores);

     if (m_reader.Read())
     {
       As_checklist m_as_checklist= new As_checklist();
       m_as_checklist.id_checklist = m_reader["id_checklist_pk"].ToString();
       m_as_checklist.chk_a = m_reader["chk_a"].ToString();
       m_as_checklist.chk_b = m_reader["chk_b"].ToString();
       m_as_checklist.chk_c = m_reader["chk_c"].ToString();
       m_as_checklist.chk_d = m_reader["chk_d"].ToString();
       m_as_checklist.chk_e = m_reader["chk_e"].ToString();
       m_as_checklist.chk_f = m_reader["chk_f"].ToString();
       m_as_checklist.chk_g = m_reader["chk_g"].ToString();
       m_as_checklist.chk_h = m_reader["chk_h"].ToString();
       m_as_checklist.chk_i = m_reader["chk_i"].ToString();
       m_as_checklist.chk_j = m_reader["chk_j"].ToString();

        m_reader.Dispose();

        return( m_as_checklist);
     }
     else
     {
        m_reader.Dispose();
        return (null);
     }
  }


  /// <summary>Retorna una lista de codigo primarios para los objetos As_checklist desde la base de datos </summary>
  /// <returns> Devuelve una lista de c�digo claves del objeto </returns>
  public ArrayList get_lista_as_checklistes(Lista p_lista)
  {
    SqlDataReader m_reader;
    this.a_sql = new StringBuilder();

    this.a_sql.AppendLine("SELECT * FROM as_checklist");

    if (p_lista.sql_parametros.Count > 0)
       {
                int m_largo = p_lista.sql_parametros.Count;
                string m_where = " WHERE ";

                for (int i = 0; i < m_largo; i++)
                {
              m_where = m_where + p_lista.sql_parametros[i] + " LIKE @" + p_lista.sql_parametros[i] + " ";

              if (i < (m_largo-1) )
                  m_where = m_where + " AND ";

              p_lista.sql_parametros[i] = "@" + p_lista.sql_parametros[i];
                }
              this.a_sql.AppendLine(m_where);
       }

       m_reader = (SqlDataReader)this.ejecutar_query(this.a_sql.ToString(), p_lista.sql_parametros, p_lista.sql_valores);
       ArrayList m_lista = new ArrayList();

       while (m_reader.Read())
       {
       m_lista.Add(m_reader["id_checklist_pk"].ToString());
       }
       m_reader.Dispose();

       return m_lista;
  }


  public bool existe_as_checklist(As_checklist p_as_checklist)
  {
    SqlDataReader m_reader;
    this.a_sql = new StringBuilder();
    
    this.a_sql.AppendLine("SELECT * FROM as_checklist WHERE ");
       this.a_sql.AppendLine("id_checklist_pk = @id_checklist_pk ");

    
    return(false);
  }

} // FIN DE LA CLASE FAC_ SISTEMA_AS_CHECKLIST
