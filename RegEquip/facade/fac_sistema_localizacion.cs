/**::::::::::::::::::::::::::::::::::::::::::
 *
 * Clase: Fachada base de datos para el objeto localizacion
 * Clase responsable: sistema
 * @author Gabriel Gutierrez Carcamo <gabriel.gutierrez@emfe.cl>
 * @category Fachada base de datos de la aplicaci�n
 * @copyright Sistema de Registro de Equipos (c) 2018
 * @copyright 
 * @version 0.1 lunes, 19 de marzo de 2018 (12:06 Hr.)
 * 
 * :::::::::::::::::::::::::::::::::::::::::::*/

/** Realiza el mapeo desde la base de datos relacional a objeto localizacion */

using System.Data.SqlClient;
using System.Text;
using System.Collections;


public class  Fac_sistema_localizacion : FacadeSQLServer2005
{
 public Fac_sistema_localizacion(string p_string_conexion) : base(p_string_conexion) { } 


  /// <summary>Ingresar un objeto Localizacion a la base de datos</summary>
  /// <param name="p_localizacion" >objeto localizacion</param> 
  /// <returns> Devuelve un verdadero si se ingreso correctamente, en caso contrario un falso.</returns>
  public int ingresar_localizacion(Localizacion p_localizacion)
  {
       int m_reader;
       this.a_sql = new StringBuilder();
       this.a_sql.AppendLine("INSERT INTO localizacion(id_localizacion_pk, id_empresa_fk, id_ciudad_fk, id_estado_localizacion_fk, descripcion, telefono, direccion, coordenadas_geo, id_localizacion_GPS_fk) ");
       this.a_sql.AppendLine("VALUES (@id_localizacion_pk, @id_empresa_fk, @id_ciudad_fk, @id_estado_localizacion_fk, @descripcion, @telefono, @direccion, @coordenadas_geo, @id_localizacion_GPS_fk);");

       ArrayList m_parametros = new ArrayList();

        m_parametros.Add("@id_localizacion_pk");
        m_parametros.Add("@id_empresa_fk");
        m_parametros.Add("@id_ciudad_fk");
        m_parametros.Add("@id_estado_localizacion_fk");
        m_parametros.Add("@descripcion");
        m_parametros.Add("@telefono");
        m_parametros.Add("@direccion");
        m_parametros.Add("@coordenadas_geo");
        m_parametros.Add("@id_localizacion_GPS_fk");


       ArrayList m_valores = new ArrayList();

       m_valores.Add(p_localizacion.id_localizacion);
       m_valores.Add(p_localizacion.id_empresa);
       m_valores.Add(p_localizacion.id_ciudad);
       m_valores.Add(p_localizacion.id_estado_localizacion);
       m_valores.Add(p_localizacion.descripcion);
       m_valores.Add(p_localizacion.telefono);
       m_valores.Add(p_localizacion.direccion);
       m_valores.Add(p_localizacion.coordenadas_geo);
       m_valores.Add(p_localizacion.id_localizacion_GPS);


       m_reader = (int)this.ejecutar_query(this.a_sql.ToString(), m_parametros, m_valores);
       return m_reader;
  }


  /// <summary>Modificar un objeto Localizacion en la base de datos </summary>
  /// <param name="p_localizacion" >objeto localizacion</param> 
  /// <returns> Devuelve la cantidad de fila afectadas.</returns>
  public int modificar_localizacion(Localizacion p_localizacion)
  {
       int m_reader;
       this.a_sql = new StringBuilder();
       this.a_sql.AppendLine("UPDATE localizacion SET ");


       this.a_sql.AppendLine("id_empresa = @id_empresa, ");
       this.a_sql.AppendLine("id_ciudad = @id_ciudad, ");
       this.a_sql.AppendLine("id_estado_localizacion = @id_estado_localizacion, ");
       this.a_sql.AppendLine("descripcion = @descripcion, ");
       this.a_sql.AppendLine("telefono = @telefono, ");
       this.a_sql.AppendLine("direccion = @direccion, ");
       this.a_sql.AppendLine("coordenadas_geo = @coordenadas_geo, ");
       this.a_sql.AppendLine("id_localizacion_GPS = @id_localizacion_GPS ");


       this.a_sql.AppendLine(" WHERE ");
       this.a_sql.AppendLine("id_localizacion_pk = @id_localizacion_pk ");


       ArrayList m_parametros = new ArrayList();


        m_parametros.Add("@id_localizacion_pk");
        m_parametros.Add("@id_empresa_fk");
        m_parametros.Add("@id_ciudad_fk");
        m_parametros.Add("@id_estado_localizacion_fk");
        m_parametros.Add("@descripcion");
        m_parametros.Add("@telefono");
        m_parametros.Add("@direccion");
        m_parametros.Add("@coordenadas_geo");
        m_parametros.Add("@id_localizacion_GPS_fk");


       ArrayList m_valores = new ArrayList();


       m_valores.Add(p_localizacion.id_localizacion);
       m_valores.Add(p_localizacion.id_empresa);
       m_valores.Add(p_localizacion.id_ciudad);
       m_valores.Add(p_localizacion.id_estado_localizacion);
       m_valores.Add(p_localizacion.descripcion);
       m_valores.Add(p_localizacion.telefono);
       m_valores.Add(p_localizacion.direccion);
       m_valores.Add(p_localizacion.coordenadas_geo);
       m_valores.Add(p_localizacion.id_localizacion_GPS);


       m_reader = (int)this.ejecutar_query(this.a_sql.ToString(), m_parametros, m_valores);
       return m_reader;
  }


  /// <summary>Retorna un objeto Localizacion desde la base de datos  </summary>
  /// <returns> Devuelve un objeto Localizacion</returns>
  public Localizacion get_localizacion(string p_id_localizacion)
  {
      SqlDataReader m_reader;
      this.a_sql = new StringBuilder();
      this.a_sql.AppendLine("SELECT * FROM localizacion WHERE ");
       this.a_sql.AppendLine("id_localizacion_pk = @id_localizacion_pk ");


       ArrayList m_parametros = new ArrayList();

        m_parametros.Add("@id_localizacion_pk");


       ArrayList m_valores = new ArrayList();

       m_valores.Add(p_id_localizacion);



      m_reader = (SqlDataReader)this.ejecutar_query(this.a_sql.ToString(), m_parametros, m_valores);

     if (m_reader.Read())
     {
       Localizacion m_localizacion= new Localizacion();
       m_localizacion.id_localizacion = m_reader["id_localizacion_pk"].ToString();
       m_localizacion.id_empresa = m_reader["id_empresa_fk"].ToString();
       m_localizacion.id_ciudad = m_reader["id_ciudad_fk"].ToString();
       m_localizacion.id_estado_localizacion = m_reader["id_estado_localizacion_fk"].ToString();
       m_localizacion.descripcion = m_reader["descripcion"].ToString();
       m_localizacion.telefono = m_reader["telefono"].ToString();
       m_localizacion.direccion = m_reader["direccion"].ToString();
       m_localizacion.coordenadas_geo = m_reader["coordenadas_geo"].ToString();
       m_localizacion.id_localizacion_GPS = m_reader["id_localizacion_GPS_fk"].ToString();

        m_reader.Dispose();

        return( m_localizacion);
     }
     else
     {
        m_reader.Dispose();
        return (null);
     }
  }


  /// <summary>Retorna una lista de codigo primarios para los objetos Localizacion desde la base de datos </summary>
  /// <returns> Devuelve una lista de c�digo claves del objeto </returns>
  public ArrayList get_lista_localizaciones(Lista p_lista)
  {
    SqlDataReader m_reader;
    this.a_sql = new StringBuilder();

    this.a_sql.AppendLine("SELECT * FROM localizacion");

    if (p_lista.sql_parametros.Count > 0)
       {
                int m_largo = p_lista.sql_parametros.Count;
                string m_where = " WHERE ";

                for (int i = 0; i < m_largo; i++)
                {
              m_where = m_where + p_lista.sql_parametros[i] + " LIKE @" + p_lista.sql_parametros[i] + " ";

              if (i < (m_largo-1) )
                  m_where = m_where + " AND ";

              p_lista.sql_parametros[i] = "@" + p_lista.sql_parametros[i];
                }
              this.a_sql.AppendLine(m_where);
       }

       m_reader = (SqlDataReader)this.ejecutar_query(this.a_sql.ToString(), p_lista.sql_parametros, p_lista.sql_valores);
       ArrayList m_lista = new ArrayList();

       while (m_reader.Read())
       {
       m_lista.Add(m_reader["id_localizacion_pk"].ToString());
       }
       m_reader.Dispose();

       return m_lista;
  }


  public bool existe_localizacion(Localizacion p_localizacion)
  {
    SqlDataReader m_reader;
    this.a_sql = new StringBuilder();
    
    this.a_sql.AppendLine("SELECT * FROM localizacion WHERE ");
       this.a_sql.AppendLine("id_localizacion_pk = @id_localizacion_pk ");

    
    return(false);
  }

} // FIN DE LA CLASE FAC_ SISTEMA_LOCALIZACION
