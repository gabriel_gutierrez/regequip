/**::::::::::::::::::::::::::::::::::::::::::
 *
 * Clase: Fachada base de datos para el objeto usuario_sistema
 * Clase responsable: sistema
 * @author Gabriel Gutierrez Carcamo <gabriel.gutierrez@emfe.cl>
 * @category Fachada base de datos de la aplicaci�n
 * @copyright Sistema de Registro de Equipos (c) 2018
 * @copyright 
 * @version 0.1 lunes, 19 de marzo de 2018 (10:44 Hr.)
 * 
 * :::::::::::::::::::::::::::::::::::::::::::*/

/** Realiza el mapeo desde la base de datos relacional a objeto usuario_sistema */

using System.Data;
using System.Data.SqlClient;
using System.Text;
using System.Collections;


public class Fac_sistema_usuario_sistema : FacadeSQLServer2005
{
    public Fac_sistema_usuario_sistema(string p_string_conexion) : base(p_string_conexion) { }


    /// <summary>Ingresar un objeto Usuario_sistema a la base de datos</summary>
    /// <param name="p_usuario_sistema" >objeto usuario_sistema</param> 
    /// <returns> Devuelve un verdadero si se ingreso correctamente, en caso contrario un falso.</returns>
    public int ingresar_usuario_sistema(Usuario_sistema p_usuario_sistema)
    {
        int m_reader;
        this.a_sql = new StringBuilder();
        this.a_sql.AppendLine("INSERT INTO usuario_sistema(id_usuario_pk, id_tipo_usuario_fk, id_area_usuario_fk, id_localizacion_holding_fk, id_empresa_holding_fk, nombre, apellido_paterno, apellido_materno, fono, anexo, clave, pregunta, respuesta, correo, fecha_activa, fecha_caduca, rut, usuario_portal_agendamiento, fecha_creacion, usuario_collahuasi) ");
        this.a_sql.AppendLine("VALUES (@id_usuario_pk, @id_tipo_usuario_fk, @id_area_usuario_fk, @id_localizacion_holding_fk, @id_empresa_holding_fk, @nombre, @apellido_paterno, @apellido_materno, @fono, @anexo, @clave, @pregunta, @respuesta, @correo, @fecha_activa, @fecha_caduca, @rut, @usuario_portal_agendamiento, @fecha_creacion, @usuario_collahuasi);");

        ArrayList m_parametros = new ArrayList();

        m_parametros.Add("@id_usuario_pk");
        m_parametros.Add("@id_tipo_usuario_fk");
        m_parametros.Add("@id_area_usuario_fk");
        m_parametros.Add("@id_localizacion_holding_fk");
        m_parametros.Add("@id_empresa_holding_fk");
        m_parametros.Add("@nombre");
        m_parametros.Add("@apellido_paterno");
        m_parametros.Add("@apellido_materno");
        m_parametros.Add("@fono");
        m_parametros.Add("@anexo");
        m_parametros.Add("@clave");
        m_parametros.Add("@pregunta");
        m_parametros.Add("@respuesta");
        m_parametros.Add("@correo");
        m_parametros.Add("@fecha_activa");
        m_parametros.Add("@fecha_caduca");
        m_parametros.Add("@rut");
        m_parametros.Add("@usuario_portal_agendamiento");
        m_parametros.Add("@fecha_creacion");
        m_parametros.Add("@usuario_collahuasi");


        ArrayList m_valores = new ArrayList();

        m_valores.Add(p_usuario_sistema.id_usuario);
        m_valores.Add(p_usuario_sistema.id_tipo_usuario);
        m_valores.Add(p_usuario_sistema.id_area_usuario);
        m_valores.Add(p_usuario_sistema.id_localizacion_holding);
        m_valores.Add(p_usuario_sistema.id_empresa_holding);
        m_valores.Add(p_usuario_sistema.nombre);
        m_valores.Add(p_usuario_sistema.apellido_paterno);
        m_valores.Add(p_usuario_sistema.apellido_materno);
        m_valores.Add(p_usuario_sistema.fono);
        m_valores.Add(p_usuario_sistema.anexo);
        m_valores.Add(p_usuario_sistema.clave);
        m_valores.Add(p_usuario_sistema.pregunta);
        m_valores.Add(p_usuario_sistema.respuesta);
        m_valores.Add(p_usuario_sistema.correo);
        m_valores.Add(p_usuario_sistema.fecha_activa);
        m_valores.Add(p_usuario_sistema.fecha_caduca);
        m_valores.Add(p_usuario_sistema.rut);
        m_valores.Add(p_usuario_sistema.usuario_portal_agendamiento);
        m_valores.Add(p_usuario_sistema.fecha_creacion);
        m_valores.Add(p_usuario_sistema.usuario_collahuasi);


        m_reader = (int)this.ejecutar_query(this.a_sql.ToString(), m_parametros, m_valores);
        return m_reader;
    }


    /// <summary>Modificar un objeto Usuario_sistema en la base de datos </summary>
    /// <param name="p_usuario_sistema" >objeto usuario_sistema</param> 
    /// <returns> Devuelve la cantidad de fila afectadas.</returns>
    public int modificar_usuario_sistema(Usuario_sistema p_usuario_sistema)
    {
        int m_reader;
        this.a_sql = new StringBuilder();
        this.a_sql.AppendLine("UPDATE usuario_sistema SET ");


        this.a_sql.AppendLine("id_tipo_usuario = @id_tipo_usuario, ");
        this.a_sql.AppendLine("id_area_usuario = @id_area_usuario, ");
        this.a_sql.AppendLine("id_localizacion_holding = @id_localizacion_holding, ");
        this.a_sql.AppendLine("id_empresa_holding = @id_empresa_holding, ");
        this.a_sql.AppendLine("nombre = @nombre, ");
        this.a_sql.AppendLine("apellido_paterno = @apellido_paterno, ");
        this.a_sql.AppendLine("apellido_materno = @apellido_materno, ");
        this.a_sql.AppendLine("fono = @fono, ");
        this.a_sql.AppendLine("anexo = @anexo, ");
        this.a_sql.AppendLine("clave = @clave, ");
        this.a_sql.AppendLine("pregunta = @pregunta, ");
        this.a_sql.AppendLine("respuesta = @respuesta, ");
        this.a_sql.AppendLine("correo = @correo, ");
        this.a_sql.AppendLine("fecha_activa = @fecha_activa, ");
        this.a_sql.AppendLine("fecha_caduca = @fecha_caduca, ");
        this.a_sql.AppendLine("rut = @rut, ");
        this.a_sql.AppendLine("usuario_portal_agendamiento = @usuario_portal_agendamiento, ");
        this.a_sql.AppendLine("fecha_creacion = @fecha_creacion, ");
        this.a_sql.AppendLine("usuario_collahuasi = @usuario_collahuasi ");


        this.a_sql.AppendLine(" WHERE ");
        this.a_sql.AppendLine("id_usuario_pk = @id_usuario_pk ");


        ArrayList m_parametros = new ArrayList();


        m_parametros.Add("@id_usuario_pk");
        m_parametros.Add("@id_tipo_usuario_fk");
        m_parametros.Add("@id_area_usuario_fk");
        m_parametros.Add("@id_localizacion_holding_fk");
        m_parametros.Add("@id_empresa_holding_fk");
        m_parametros.Add("@nombre");
        m_parametros.Add("@apellido_paterno");
        m_parametros.Add("@apellido_materno");
        m_parametros.Add("@fono");
        m_parametros.Add("@anexo");
        m_parametros.Add("@clave");
        m_parametros.Add("@pregunta");
        m_parametros.Add("@respuesta");
        m_parametros.Add("@correo");
        m_parametros.Add("@fecha_activa");
        m_parametros.Add("@fecha_caduca");
        m_parametros.Add("@rut");
        m_parametros.Add("@usuario_portal_agendamiento");
        m_parametros.Add("@fecha_creacion");
        m_parametros.Add("@usuario_collahuasi");


        ArrayList m_valores = new ArrayList();


        m_valores.Add(p_usuario_sistema.id_usuario);
        m_valores.Add(p_usuario_sistema.id_tipo_usuario);
        m_valores.Add(p_usuario_sistema.id_area_usuario);
        m_valores.Add(p_usuario_sistema.id_localizacion_holding);
        m_valores.Add(p_usuario_sistema.id_empresa_holding);
        m_valores.Add(p_usuario_sistema.nombre);
        m_valores.Add(p_usuario_sistema.apellido_paterno);
        m_valores.Add(p_usuario_sistema.apellido_materno);
        m_valores.Add(p_usuario_sistema.fono);
        m_valores.Add(p_usuario_sistema.anexo);
        m_valores.Add(p_usuario_sistema.clave);
        m_valores.Add(p_usuario_sistema.pregunta);
        m_valores.Add(p_usuario_sistema.respuesta);
        m_valores.Add(p_usuario_sistema.correo);
        m_valores.Add(p_usuario_sistema.fecha_activa);
        m_valores.Add(p_usuario_sistema.fecha_caduca);
        m_valores.Add(p_usuario_sistema.rut);
        m_valores.Add(p_usuario_sistema.usuario_portal_agendamiento);
        m_valores.Add(p_usuario_sistema.fecha_creacion);
        m_valores.Add(p_usuario_sistema.usuario_collahuasi);


        m_reader = (int)this.ejecutar_query(this.a_sql.ToString(), m_parametros, m_valores);
        return m_reader;
    }


    /// <summary>Retorna un objeto Usuario_sistema desde la base de datos  </summary>
    /// <returns> Devuelve un objeto Usuario_sistema</returns>
    public Usuario_sistema get_usuario_sistema(string p_id_usuario)
    {
        SqlDataReader m_reader;
        this.a_sql = new StringBuilder();
        this.a_sql.AppendLine("SELECT * FROM usuario_sistema WHERE ");
        this.a_sql.AppendLine("id_usuario_pk = @id_usuario_pk ");


        ArrayList m_parametros = new ArrayList();

        m_parametros.Add("@id_usuario_pk");


        ArrayList m_valores = new ArrayList();

        m_valores.Add(p_id_usuario);



        m_reader = (SqlDataReader)this.ejecutar_query(this.a_sql.ToString(), m_parametros, m_valores);

        if (m_reader.Read())
        {
            Usuario_sistema m_usuario_sistema = new Usuario_sistema();
            m_usuario_sistema.id_usuario = m_reader["id_usuario_pk"].ToString();
            m_usuario_sistema.id_tipo_usuario = m_reader["id_tipo_usuario_fk"].ToString();
            m_usuario_sistema.id_area_usuario = m_reader["id_area_usuario_fk"].ToString();
            m_usuario_sistema.id_localizacion_holding = m_reader["id_localizacion_holding_fk"].ToString();
            m_usuario_sistema.id_empresa_holding = m_reader["id_empresa_holding_fk"].ToString();
            m_usuario_sistema.nombre = m_reader["nombre"].ToString();
            m_usuario_sistema.apellido_paterno = m_reader["apellido_paterno"].ToString();
            m_usuario_sistema.apellido_materno = m_reader["apellido_materno"].ToString();
            m_usuario_sistema.fono = m_reader["fono"].ToString();
            m_usuario_sistema.anexo = m_reader["anexo"].ToString();
            m_usuario_sistema.clave = m_reader["clave"].ToString();
            m_usuario_sistema.pregunta = m_reader["pregunta"].ToString();
            m_usuario_sistema.respuesta = m_reader["respuesta"].ToString();
            m_usuario_sistema.correo = m_reader["correo"].ToString();
            m_usuario_sistema.fecha_activa = m_reader["fecha_activa"].ToString();
            m_usuario_sistema.fecha_caduca = m_reader["fecha_caduca"].ToString();
            m_usuario_sistema.rut = m_reader["rut"].ToString();
            m_usuario_sistema.usuario_portal_agendamiento = m_reader["usuario_portal_agendamiento"].ToString();
            m_usuario_sistema.fecha_creacion = m_reader["fecha_creacion"].ToString();
            m_usuario_sistema.usuario_collahuasi = m_reader["usuario_collahuasi"].ToString();

            m_reader.Dispose();

            return (m_usuario_sistema);
        }
        else
        {
            m_reader.Dispose();
            return (null);
        }
    }


    /// <summary>Retorna una lista de codigo primarios para los objetos Usuario_sistema desde la base de datos </summary>
    /// <returns> Devuelve una lista de c�digo claves del objeto </returns>
    public ArrayList get_lista_usuario_sistemas(Lista p_lista, string ordenadoPor)
    {
        SqlDataReader m_reader;
        string ordenConsulta = "";
        this.a_sql = new StringBuilder();

        if (ordenadoPor == "APELLIDOS")
        {
            ordenConsulta = "Order by apellido_paterno, apellido_materno, nombre";
        }

        this.a_sql.AppendLine("SELECT * FROM usuario_sistema ");
        this.a_sql.AppendLine("where usuario_sistema.fecha_caduca IS NULL and usuario_portal_agendamiento is null ");

        if (p_lista.sql_parametros.Count > 0)
        {
            int m_largo = p_lista.sql_parametros.Count;
            string m_where = " and ";

            for (int i = 0; i < m_largo; i++)
            {
                m_where = m_where + p_lista.sql_parametros[i] + " =     @" + p_lista.sql_parametros[i] + " ";

                if (i < (m_largo - 1))
                    m_where = m_where + " AND ";

                p_lista.sql_parametros[i] = "@" + p_lista.sql_parametros[i];
            }
            this.a_sql.AppendLine(m_where);
        }

        m_reader = (SqlDataReader)this.ejecutar_query(this.a_sql.ToString() + ordenConsulta, p_lista.sql_parametros, p_lista.sql_valores);
        ArrayList m_lista = new ArrayList();

        while (m_reader.Read())
        {
            Usuario_sistema m_usuario_sistema = llenarUsuario(m_reader);
            m_lista.Add(m_usuario_sistema);
        }
        m_reader.Close(); m_reader.Dispose();

        return m_lista;
    }

    public DataSet get_lista_usuario_sistemas_dt(Lista p_lista, string ordenadoPor)
    {
        SqlDataReader m_reader;
        string ordenConsulta = "";
        this.a_sql = new StringBuilder();

        if (ordenadoPor == "APELLIDOS")
        {
            ordenConsulta = "Order by apellido_paterno, apellido_materno, nombre";
        }

        this.a_sql.AppendLine("SELECT id_usuario_pk, CONCAT(nombre, ' ', apellido_paterno, ' ', apellido_materno) AS nombre_completo FROM usuario_sistema ");
        this.a_sql.AppendLine("where usuario_sistema.fecha_caduca IS NULL and usuario_portal_agendamiento is null ");

        if (p_lista.sql_parametros.Count > 0)
        {
            int m_largo = p_lista.sql_parametros.Count;
            string m_where = " and ";

            for (int i = 0; i < m_largo; i++)
            {
                m_where = m_where + p_lista.sql_parametros[i] + " =     @" + p_lista.sql_parametros[i] + " ";

                if (i < (m_largo - 1))
                    m_where = m_where + " AND ";

                p_lista.sql_parametros[i] = "@" + p_lista.sql_parametros[i];
            }
            this.a_sql.AppendLine(m_where);
        }

        m_reader = (SqlDataReader)this.ejecutar_query(this.a_sql.ToString() + ordenConsulta, p_lista.sql_parametros, p_lista.sql_valores);
        DataTable dt = new DataTable();
        DataSet m_lista = new DataSet();
        
        if(m_reader != null)
        {
            dt.Load(m_reader);
            m_lista.Tables.Add(dt);
        }

        m_reader.Close();
        m_reader.Dispose();

        return m_lista;
    }
    private static Usuario_sistema llenarUsuario(SqlDataReader m_reader)
    {
        Usuario_sistema m_usuario_sistema = new Usuario_sistema();
        m_usuario_sistema.id_usuario = m_reader["id_usuario_pk"].ToString();
        m_usuario_sistema.id_tipo_usuario = m_reader["id_tipo_usuario_fk"].ToString();
        m_usuario_sistema.id_area_usuario = m_reader["id_area_usuario_fk"].ToString();
        m_usuario_sistema.id_localizacion_holding = m_reader["id_localizacion_holding_fk"].ToString();
        m_usuario_sistema.id_empresa_holding = m_reader["id_empresa_holding_fk"].ToString();
        m_usuario_sistema.nombre = m_reader["nombre"].ToString();
        m_usuario_sistema.apellido_paterno = m_reader["apellido_paterno"].ToString();
        m_usuario_sistema.apellido_materno = m_reader["apellido_materno"].ToString();
        m_usuario_sistema.fono = m_reader["fono"].ToString();
        m_usuario_sistema.anexo = m_reader["anexo"].ToString();
        m_usuario_sistema.clave = m_reader["clave"].ToString();
        m_usuario_sistema.pregunta = m_reader["pregunta"].ToString();
        m_usuario_sistema.respuesta = m_reader["respuesta"].ToString();
        m_usuario_sistema.correo = m_reader["correo"].ToString();
        m_usuario_sistema.fecha_activa = m_reader["fecha_activa"].ToString();
        m_usuario_sistema.fecha_caduca = m_reader["fecha_caduca"].ToString();
        m_usuario_sistema.rut = m_reader["rut"].ToString();
        m_usuario_sistema.usuario_collahuasi = m_reader["usuario_collahuasi"].ToString();
        return m_usuario_sistema;
    }

    public bool existe_usuario_sistema(Usuario_sistema p_usuario_sistema)
    {
        SqlDataReader m_reader;
        this.a_sql = new StringBuilder();

        this.a_sql.AppendLine("SELECT * FROM usuario_sistema WHERE ");
        this.a_sql.AppendLine("id_usuario_pk = @id_usuario_pk ");

        return (false);
    }

    public Usuario_sistema obtieneUsuario(string p_login, string p_pass)
    {
        SqlDataReader m_reader;
        Usuario_sistema m_usuario = new Usuario_sistema();
        this.a_sql = new StringBuilder();
        this.a_sql.AppendLine("SELECT * FROM usuario_sistema WHERE rut = @login_pk");

        ArrayList m_parametros = new ArrayList();
        m_parametros.Add("@login_pk");

        ArrayList m_valores = new ArrayList();
        m_valores.Add(p_login);

        m_reader = (SqlDataReader)this.ejecutar_query(this.a_sql.ToString(), m_parametros, m_valores);

        if (m_reader.Read())
        {
            Util m_convertir = new Util();
            string m_password = m_convertir.encriptar(p_pass);
            string m_idUsuario = m_reader["id_usuario_pk"].ToString();
            string m_passwordDB = m_reader["clave"].ToString();
            m_reader.Close(); m_reader.Dispose();
            if (m_password.CompareTo(m_passwordDB) == 0)
            {
                m_usuario = this.get_usuario_sistema(m_idUsuario);
            }
            // m_usuario = this.get_usuario_sistema(m_idUsuario);
        }
        else
        {
            m_reader.Close(); m_reader.Dispose();
        }

        return (m_usuario); // no encontro al usuario

    }

} // FIN DE LA CLASE FAC_ SISTEMA_USUARIO_SISTEMA
