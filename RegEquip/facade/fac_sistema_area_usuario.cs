/**::::::::::::::::::::::::::::::::::::::::::
 *
 * Clase: Fachada base de datos para el objeto area_usuario
 * Clase responsable: sistema
 * @author Gabriel Gutierrez Carcamo <gabriel.gutierrez@emfe.cl>
 * @category Fachada base de datos de la aplicaci�n
 * @copyright Sistema de Registro de Equipos (c) 2018
 * @copyright 
 * @version 0.1 lunes, 19 de marzo de 2018 (12:00 Hr.)
 * 
 * :::::::::::::::::::::::::::::::::::::::::::*/

/** Realiza el mapeo desde la base de datos relacional a objeto area_usuario */

using System.Data.SqlClient;
using System.Text;
using System.Collections;


public class  Fac_sistema_area_usuario : FacadeSQLServer2005
{
 public Fac_sistema_area_usuario(string p_string_conexion) : base(p_string_conexion) { } 


  /// <summary>Ingresar un objeto Area_usuario a la base de datos</summary>
  /// <param name="p_area_usuario" >objeto area_usuario</param> 
  /// <returns> Devuelve un verdadero si se ingreso correctamente, en caso contrario un falso.</returns>
  public int ingresar_area_usuario(Area_usuario p_area_usuario)
  {
       int m_reader;
       this.a_sql = new StringBuilder();
       this.a_sql.AppendLine("INSERT INTO area_usuario(id_area_usuario_pk, descripcion) ");
       this.a_sql.AppendLine("VALUES (@id_area_usuario_pk, @descripcion);");

       ArrayList m_parametros = new ArrayList();

        m_parametros.Add("@id_area_usuario_pk");
        m_parametros.Add("@descripcion");


       ArrayList m_valores = new ArrayList();

       m_valores.Add(p_area_usuario.id_area_usuario);
       m_valores.Add(p_area_usuario.descripcion);


       m_reader = (int)this.ejecutar_query(this.a_sql.ToString(), m_parametros, m_valores);
       return m_reader;
  }


  /// <summary>Modificar un objeto Area_usuario en la base de datos </summary>
  /// <param name="p_area_usuario" >objeto area_usuario</param> 
  /// <returns> Devuelve la cantidad de fila afectadas.</returns>
  public int modificar_area_usuario(Area_usuario p_area_usuario)
  {
       int m_reader;
       this.a_sql = new StringBuilder();
       this.a_sql.AppendLine("UPDATE area_usuario SET ");


       this.a_sql.AppendLine("descripcion = @descripcion ");


       this.a_sql.AppendLine(" WHERE ");
       this.a_sql.AppendLine("id_area_usuario_pk = @id_area_usuario_pk ");


       ArrayList m_parametros = new ArrayList();


        m_parametros.Add("@id_area_usuario_pk");
        m_parametros.Add("@descripcion");


       ArrayList m_valores = new ArrayList();


       m_valores.Add(p_area_usuario.id_area_usuario);
       m_valores.Add(p_area_usuario.descripcion);


       m_reader = (int)this.ejecutar_query(this.a_sql.ToString(), m_parametros, m_valores);
       return m_reader;
  }


  /// <summary>Retorna un objeto Area_usuario desde la base de datos  </summary>
  /// <returns> Devuelve un objeto Area_usuario</returns>
  public Area_usuario get_area_usuario(string p_id_area_usuario)
  {
      SqlDataReader m_reader;
      this.a_sql = new StringBuilder();
      this.a_sql.AppendLine("SELECT * FROM area_usuario WHERE ");
       this.a_sql.AppendLine("id_area_usuario_pk = @id_area_usuario_pk ");


       ArrayList m_parametros = new ArrayList();

        m_parametros.Add("@id_area_usuario_pk");


       ArrayList m_valores = new ArrayList();

       m_valores.Add(p_id_area_usuario);



      m_reader = (SqlDataReader)this.ejecutar_query(this.a_sql.ToString(), m_parametros, m_valores);

     if (m_reader.Read())
     {
       Area_usuario m_area_usuario= new Area_usuario();
       m_area_usuario.id_area_usuario = m_reader["id_area_usuario_pk"].ToString();
       m_area_usuario.descripcion = m_reader["descripcion"].ToString();

        m_reader.Dispose();

        return( m_area_usuario);
     }
     else
     {
        m_reader.Dispose();
        return (null);
     }
  }


  /// <summary>Retorna una lista de codigo primarios para los objetos Area_usuario desde la base de datos </summary>
  /// <returns> Devuelve una lista de c�digo claves del objeto </returns>
  public ArrayList get_lista_area_usuarios(Lista p_lista)
  {
    SqlDataReader m_reader;
    this.a_sql = new StringBuilder();

    this.a_sql.AppendLine("SELECT * FROM area_usuario");

    if (p_lista.sql_parametros.Count > 0)
       {
                int m_largo = p_lista.sql_parametros.Count;
                string m_where = " WHERE ";

                for (int i = 0; i < m_largo; i++)
                {
              m_where = m_where + p_lista.sql_parametros[i] + " LIKE @" + p_lista.sql_parametros[i] + " ";

              if (i < (m_largo-1) )
                  m_where = m_where + " AND ";

              p_lista.sql_parametros[i] = "@" + p_lista.sql_parametros[i];
                }
              this.a_sql.AppendLine(m_where);
       }

       m_reader = (SqlDataReader)this.ejecutar_query(this.a_sql.ToString(), p_lista.sql_parametros, p_lista.sql_valores);
       ArrayList m_lista = new ArrayList();

       while (m_reader.Read())
       {
       m_lista.Add(m_reader["id_area_usuario_pk"].ToString());
       }
       m_reader.Dispose();

       return m_lista;
  }


  public bool existe_area_usuario(Area_usuario p_area_usuario)
  {
    SqlDataReader m_reader;
    this.a_sql = new StringBuilder();
    
    this.a_sql.AppendLine("SELECT * FROM area_usuario WHERE ");
       this.a_sql.AppendLine("id_area_usuario_pk = @id_area_usuario_pk ");

    
    return(false);
  }

} // FIN DE LA CLASE FAC_ SISTEMA_AREA_USUARIO
