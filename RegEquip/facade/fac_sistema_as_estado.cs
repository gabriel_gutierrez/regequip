/**::::::::::::::::::::::::::::::::::::::::::
 *
 * Clase: Fachada base de datos para el objeto as_estado
 * Clase responsable: sistema
 * @author Gabriel Gutierrez Carcamo <gabriel.gutierrez@emfe.cl>
 * @category Fachada base de datos de la aplicaci�n
 * @copyright Sistema de Registro de Equipos (c) 2018
 * @copyright 
 * @version 0.1 lunes, 19 de marzo de 2018 (10:31 Hr.)
 * 
 * :::::::::::::::::::::::::::::::::::::::::::*/

/** Realiza el mapeo desde la base de datos relacional a objeto as_estado */

using System.Data.SqlClient;
using System.Text;
using System.Collections;


public class  Fac_sistema_as_estado : FacadeSQLServer2005
{
 public Fac_sistema_as_estado(string p_string_conexion) : base(p_string_conexion) { } 


  /// <summary>Ingresar un objeto As_estado a la base de datos</summary>
  /// <param name="p_as_estado" >objeto as_estado</param> 
  /// <returns> Devuelve un verdadero si se ingreso correctamente, en caso contrario un falso.</returns>
  public int ingresar_as_estado(As_estado p_as_estado)
  {
       int m_reader;
       this.a_sql = new StringBuilder();
       this.a_sql.AppendLine("INSERT INTO as_estado(id_estado_pk, descripcion_es, valido) ");
       this.a_sql.AppendLine("VALUES (@id_estado_pk, @descripcion_es, @valido);");

       ArrayList m_parametros = new ArrayList();

        m_parametros.Add("@id_estado_pk");
        m_parametros.Add("@descripcion_es");
        m_parametros.Add("@valido");


       ArrayList m_valores = new ArrayList();

       m_valores.Add(p_as_estado.id_estado);
       m_valores.Add(p_as_estado.descripcion_es);
       m_valores.Add(p_as_estado.valido);


       m_reader = (int)this.ejecutar_query(this.a_sql.ToString(), m_parametros, m_valores);
       return m_reader;
  }


  /// <summary>Modificar un objeto As_estado en la base de datos </summary>
  /// <param name="p_as_estado" >objeto as_estado</param> 
  /// <returns> Devuelve la cantidad de fila afectadas.</returns>
  public int modificar_as_estado(As_estado p_as_estado)
  {
       int m_reader;
       this.a_sql = new StringBuilder();
       this.a_sql.AppendLine("UPDATE as_estado SET ");


       this.a_sql.AppendLine("descripcion_es = @descripcion_es, ");
       this.a_sql.AppendLine("valido = @valido ");


       this.a_sql.AppendLine(" WHERE ");
       this.a_sql.AppendLine("id_estado_pk = @id_estado_pk ");


       ArrayList m_parametros = new ArrayList();


        m_parametros.Add("@id_estado_pk");
        m_parametros.Add("@descripcion_es");
        m_parametros.Add("@valido");


       ArrayList m_valores = new ArrayList();


       m_valores.Add(p_as_estado.id_estado);
       m_valores.Add(p_as_estado.descripcion_es);
       m_valores.Add(p_as_estado.valido);


       m_reader = (int)this.ejecutar_query(this.a_sql.ToString(), m_parametros, m_valores);
       return m_reader;
  }


  /// <summary>Retorna un objeto As_estado desde la base de datos  </summary>
  /// <returns> Devuelve un objeto As_estado</returns>
  public As_estado get_as_estado(string p_id_estado)
  {
      SqlDataReader m_reader;
      this.a_sql = new StringBuilder();
      this.a_sql.AppendLine("SELECT * FROM as_estado WHERE ");
       this.a_sql.AppendLine("id_estado_pk = @id_estado_pk ");


       ArrayList m_parametros = new ArrayList();

        m_parametros.Add("@id_estado_pk");


       ArrayList m_valores = new ArrayList();

       m_valores.Add(p_id_estado);



      m_reader = (SqlDataReader)this.ejecutar_query(this.a_sql.ToString(), m_parametros, m_valores);

     if (m_reader.Read())
     {
       As_estado m_as_estado= new As_estado();
       m_as_estado.id_estado = m_reader["id_estado_pk"].ToString();
       m_as_estado.descripcion_es = m_reader["descripcion_es"].ToString();
       m_as_estado.valido = m_reader["valido"].ToString();

        m_reader.Dispose();

        return( m_as_estado);
     }
     else
     {
        m_reader.Dispose();
        return (null);
     }
  }


  /// <summary>Retorna una lista de codigo primarios para los objetos As_estado desde la base de datos </summary>
  /// <returns> Devuelve una lista de c�digo claves del objeto </returns>
  public ArrayList get_lista_as_estados(Lista p_lista)
  {
    SqlDataReader m_reader;
    this.a_sql = new StringBuilder();

    this.a_sql.AppendLine("SELECT * FROM as_estado");

    if (p_lista.sql_parametros.Count > 0)
       {
                int m_largo = p_lista.sql_parametros.Count;
                string m_where = " WHERE ";

                for (int i = 0; i < m_largo; i++)
                {
              m_where = m_where + p_lista.sql_parametros[i] + " LIKE @" + p_lista.sql_parametros[i] + " ";

              if (i < (m_largo-1) )
                  m_where = m_where + " AND ";

              p_lista.sql_parametros[i] = "@" + p_lista.sql_parametros[i];
                }
              this.a_sql.AppendLine(m_where);
       }

       m_reader = (SqlDataReader)this.ejecutar_query(this.a_sql.ToString(), p_lista.sql_parametros, p_lista.sql_valores);
       ArrayList m_lista = new ArrayList();

       while (m_reader.Read())
       {
       m_lista.Add(m_reader["id_estado_pk"].ToString());
       }
       m_reader.Dispose();

       return m_lista;
  }


  public bool existe_as_estado(As_estado p_as_estado)
  {
    SqlDataReader m_reader;
    this.a_sql = new StringBuilder();
    
    this.a_sql.AppendLine("SELECT * FROM as_estado WHERE ");
       this.a_sql.AppendLine("id_estado_pk = @id_estado_pk ");

    
    return(false);
  }

} // FIN DE LA CLASE FAC_ SISTEMA_AS_ESTADO
