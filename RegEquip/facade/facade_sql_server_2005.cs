﻿/**::::::::::::::::::::::::::::::::::::::::::
 
 Clase: OBJETO Facadesqlserver2005
 @author Ronald Peña Orellana
 @author ronaldd@ucn.cl
 @version Lunes, 25 de Abril de 2011 (15:52 Hr.)
 
:::::::::::::::::::::::::::::::::::::::::::*/

using System.Configuration;
using System;
using System.Web;
using System.Collections.Generic;
using System.Collections;
using System.Linq;
using System.Text;
using System.Data;
using System.Data.SqlClient;

/// <summary>
/// Permite la conexión a la base de datos SQL SERVER 2005
/// 
/// FORMA DE USAR
/// StringBuilder m_sql2 = new StringBuilder();
/// m_sql2.AppendLine("INSERT INTO personal (nombre,apellido, numero) VALUES (@nombre, @apellido, @numero)");
///        
/// string[] m_parametros = {"@nombre","@apellido", "@numero"}; 
/// string[] m_valores    = {"Ronald","Peña", "102"};

/// numero = Convert.ToInt32(m_fachada.ejecutar_query(m_sql2.ToString(), m_parametros, m_valores));
/// m_fachada.cerrar();
/// etiqueta.Text = etiqueta.Text + "  fila afectada: " + numero.ToString(); 
/// </summary>

public class FacadeSQLServer2005
{

    protected SqlConnection a_conexion;
    protected SqlCommand a_query;
    protected SqlTransaction a_transaccion;
    protected StringBuilder a_sql;


    protected string a_string_conexion;
    protected string a_error;
    protected string a_numero_error;


    public FacadeSQLServer2005(string p_nombre_conexion)
    {
        ConnectionStringSettings m_conexion_web_config;

        if (p_nombre_conexion == "")
        {
            m_conexion_web_config = ConfigurationManager.ConnectionStrings["conexion_principal"];
        }
        else
        {
            m_conexion_web_config = ConfigurationManager.ConnectionStrings[p_nombre_conexion];
        }

        if (m_conexion_web_config != null)
        {
            this.a_string_conexion = m_conexion_web_config.ConnectionString;
        }
        else
            Console.WriteLine("No existen el string de conexión en App.config!");


        a_conexion = null;
        a_transaccion = null;
    }
    /** ------------------------------------------------------------- */


    public void conectar()
    {

        try
        {
            this.a_conexion = new SqlConnection(this.a_string_conexion);

        }
        catch (Exception e)
        {
            this.manejo_errores("string_conexion", e);
            this.a_conexion = null;
        }

        if (this.a_conexion != null)
        {

            try
            {
                this.a_conexion.Open();

                /* if (this.a_conexion.State != ConnectionState.Open)
                 {
                     this.manejo_errores("conexion", e);

                 }*/
            }
            catch (Exception e)
            {
                this.manejo_errores("conexion", e);
                if (this.a_conexion.State.CompareTo(ConnectionState.Open) == 0) this.a_conexion.Close();
            }

        }


    }
    /** ------------------------------------------------------------- */

    public void manejo_errores(string p_error, Exception p_excepcion)
    {

        /*Session["error_previsto"] = true;

        if (p_error.CompareTo("string_conexion") == 0)
        {
            Session["mensaje_usuario"] = "Dejo de responder al tratar de realizar la conexión hacia la base de datos.";
            Session["mensaje_administrador"] = "Se produjo al utilizar el string de conexión al instanciar la clase fachada";

            if (p_excepcion.Message.Contains("Keyword not supported"))
                Session["mensaje_sistema"] = p_excepcion.Message + "<br />Es: Palabra clave es incorrecta en el string de conexión (web.config).";
            else
                Session["mensaje_sistema"] = p_excepcion.Message;

        }

        if (p_error.CompareTo("conexion") == 0)
        {
            Session["mensaje_usuario"] = "Dejo de responder al tratar de realizar la conexión hacia la base de datos.";
            Session["mensaje_administrador"] = "Es servidor de base de datos dejo de responder o datos de conexión incorrectos en el web.config";
            Session["mensaje_sistema"] = p_excepcion.Message;

        }

        if (p_error.CompareTo("query") == 0)
        {
            Session["mensaje_usuario"] = "Dejo de responder al tratar de ejecutar una consulta hacia la base de datos.";
            Session["mensaje_administrador"] = "Es servidor de base de datos dejo de responder por error en la formación de la Query";
            Session["mensaje_sistema"] = p_excepcion.Message;
            Session["mensaje_query"] = a_query.CommandText;
           
        }

        Server.Transfer("~/interface/excepcion.aspx");*/
    }
    /** ------------------------------------------------------------- */



    public void cerrar() { this.a_sql = null; this.a_conexion.Close(); }
    /** ------------------------------------------------------------- */


    public string get_query() { return (this.a_query.ToString()); }
    /** ------------------------------------------------------------- */


    public Object ejecutar_query(string p_sql, ArrayList p_parametros, ArrayList p_valores)
    {
        Object m_reader = new Object();
        m_reader = null;


        SqlCommand m_sql = new SqlCommand(p_sql, this.a_conexion);

        if (p_parametros != null)
        {
            int m_largo = p_parametros.Count;

            for (int i = 0; i < m_largo; i++)
                m_sql.Parameters.AddWithValue(p_parametros[i].ToString(), p_valores[i].ToString());
        }

        a_query = m_sql; //en caso de error recuperar la query;

        if (p_sql.Contains("insert") || p_sql.Contains("INSERT") || p_sql.Contains("update") || p_sql.Contains("UPDATE") || p_sql.Contains("delete") || p_sql.Contains("DELETE"))
        {

            //retorna el número de filas afectadas

            try
            {
                m_reader = m_sql.ExecuteNonQuery();

            }
            catch (Exception e)
            {
                this.manejo_errores("query", e);
                if (this.a_conexion.State.CompareTo(ConnectionState.Open) == 0) this.a_conexion.Close();
            }

        }
        else
            if (p_sql.Contains("select") || p_sql.Contains("SELECT"))
        {
            try
            {
                m_reader = m_sql.ExecuteReader();

            }
            catch (Exception e)
            {
                this.manejo_errores("query", e);
                if (this.a_conexion.State.CompareTo(ConnectionState.Open) == 0) this.a_conexion.Close();
            }
        }

        return (m_reader);
    }
    /** ------------------------------------------------------------- */

    public Object ejecutar_escalar(string p_sql, StringBuilder p_parametros, StringBuilder p_valores)
    {
        Object m_reader = new Object();
        m_reader = null;

        SqlCommand m_sql = new SqlCommand(p_sql, this.a_conexion);

        if (p_parametros != null)
        {
            int m_largo = p_parametros.Capacity;

            for (int i = 0; i < m_largo; i++)
                m_sql.Parameters.AddWithValue(p_parametros[i].ToString(), p_valores[i].ToString());
        }

        try
        {
            m_reader = m_sql.ExecuteScalar();

        }
        catch (Exception e)
        {
            this.manejo_errores("query", e);
            if (this.a_conexion.State.CompareTo(ConnectionState.Open) == 0) this.a_conexion.Close();
        }

        return (m_reader);
    }
    /** ------------------------------------------------------------- */

    public void iniciar_transaccion() { a_transaccion = this.a_conexion.BeginTransaction(); }
    /** ------------------------------------------------------------- */

    public int get_autonumerico(string p_tabla)
    {
        string m_sql = "SELECT IDENT_CURRENT('" + p_tabla + "')";

        int m_id = (int)this.ejecutar_escalar(m_sql, null, null);

        if (m_id > 0) return (m_id); else return (0);
    }
    /** ------------------------------------------------------------- */

    public void commit() { this.a_transaccion.Commit(); }
    /** ------------------------------------------------------------- */

    public void rollback() { this.a_transaccion.Rollback(); }
    /** ------------------------------------------------------------- */

    public string get_error() { return (this.a_error); }

}/** FIN DE CLASE Facadesqlserver2005 */

