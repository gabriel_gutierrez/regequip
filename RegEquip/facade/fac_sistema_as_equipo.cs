/**::::::::::::::::::::::::::::::::::::::::::
 *
 * Clase: Fachada base de datos para el objeto as_equipo
 * Clase responsable: sistema
 * @author Gabriel Gutierrez Carcamo <gabriel.gutierrez@emfe.cl>
 * @category Fachada base de datos de la aplicaci�n
 * @copyright Sistema de Registro de Equipos (c) 2018
 * @copyright 
 * @version 0.1 lunes, 19 de marzo de 2018 (10:33 Hr.)
 * 
 * :::::::::::::::::::::::::::::::::::::::::::*/

/** Realiza el mapeo desde la base de datos relacional a objeto as_equipo */

using System.Data.SqlClient;
using System.Text;
using System.Collections;


public class  Fac_sistema_as_equipo : FacadeSQLServer2005
{
 public Fac_sistema_as_equipo(string p_string_conexion) : base(p_string_conexion) { } 


  /// <summary>Ingresar un objeto As_equipo a la base de datos</summary>
  /// <param name="p_as_equipo" >objeto as_equipo</param> 
  /// <returns> Devuelve un verdadero si se ingreso correctamente, en caso contrario un falso.</returns>
  public int ingresar_as_equipo(As_equipo p_as_equipo)
  {
       int m_reader;
       this.a_sql = new StringBuilder();
       this.a_sql.AppendLine("INSERT INTO as_equipo(id_equipo_pk, id_tipo_equipo_fk, id_marca_fk, id_modelo_fk, mac_ethernet, ip_ethernet_telefono, mac_wifi, ip_wifi, identificador, observacion, id_detalle_linea_fk, valido) ");
       this.a_sql.AppendLine("VALUES (@id_equipo_pk, @id_tipo_equipo_fk, @id_marca_fk, @id_modelo_fk, @mac_ethernet, @ip_ethernet_telefono, @mac_wifi, @ip_wifi, @identificador, @observacion, @id_detalle_linea_fk, @valido);");

       ArrayList m_parametros = new ArrayList();

        m_parametros.Add("@id_equipo_pk");
        m_parametros.Add("@id_tipo_equipo_fk");
        m_parametros.Add("@id_marca_fk");
        m_parametros.Add("@id_modelo_fk");
        m_parametros.Add("@mac_ethernet");
        m_parametros.Add("@ip_ethernet_telefono");
        m_parametros.Add("@mac_wifi");
        m_parametros.Add("@ip_wifi");
        m_parametros.Add("@identificador");
        m_parametros.Add("@observacion");
        m_parametros.Add("@id_detalle_linea_fk");
        m_parametros.Add("@valido");


       ArrayList m_valores = new ArrayList();

       m_valores.Add(p_as_equipo.id_equipo);
       m_valores.Add(p_as_equipo.id_tipo_equipo);
       m_valores.Add(p_as_equipo.id_marca);
       m_valores.Add(p_as_equipo.id_modelo);
       m_valores.Add(p_as_equipo.mac_ethernet);
       m_valores.Add(p_as_equipo.ip_ethernet_telefono);
       m_valores.Add(p_as_equipo.mac_wifi);
       m_valores.Add(p_as_equipo.ip_wifi);
       m_valores.Add(p_as_equipo.identificador);
       m_valores.Add(p_as_equipo.observacion);
       m_valores.Add(p_as_equipo.id_detalle_linea);
       m_valores.Add(p_as_equipo.valido);


       m_reader = (int)this.ejecutar_query(this.a_sql.ToString(), m_parametros, m_valores);
       return m_reader;
  }


  /// <summary>Modificar un objeto As_equipo en la base de datos </summary>
  /// <param name="p_as_equipo" >objeto as_equipo</param> 
  /// <returns> Devuelve la cantidad de fila afectadas.</returns>
  public int modificar_as_equipo(As_equipo p_as_equipo)
  {
       int m_reader;
       this.a_sql = new StringBuilder();
       this.a_sql.AppendLine("UPDATE as_equipo SET ");


       this.a_sql.AppendLine("id_tipo_equipo = @id_tipo_equipo, ");
       this.a_sql.AppendLine("id_marca = @id_marca, ");
       this.a_sql.AppendLine("id_modelo = @id_modelo, ");
       this.a_sql.AppendLine("mac_ethernet = @mac_ethernet, ");
       this.a_sql.AppendLine("ip_ethernet_telefono = @ip_ethernet_telefono, ");
       this.a_sql.AppendLine("mac_wifi = @mac_wifi, ");
       this.a_sql.AppendLine("ip_wifi = @ip_wifi, ");
       this.a_sql.AppendLine("identificador = @identificador, ");
       this.a_sql.AppendLine("observacion = @observacion, ");
       this.a_sql.AppendLine("id_detalle_linea = @id_detalle_linea, ");
       this.a_sql.AppendLine("valido = @valido ");


       this.a_sql.AppendLine(" WHERE ");
       this.a_sql.AppendLine("id_equipo_pk = @id_equipo_pk ");


       ArrayList m_parametros = new ArrayList();


        m_parametros.Add("@id_equipo_pk");
        m_parametros.Add("@id_tipo_equipo_fk");
        m_parametros.Add("@id_marca_fk");
        m_parametros.Add("@id_modelo_fk");
        m_parametros.Add("@mac_ethernet");
        m_parametros.Add("@ip_ethernet_telefono");
        m_parametros.Add("@mac_wifi");
        m_parametros.Add("@ip_wifi");
        m_parametros.Add("@identificador");
        m_parametros.Add("@observacion");
        m_parametros.Add("@id_detalle_linea_fk");
        m_parametros.Add("@valido");


       ArrayList m_valores = new ArrayList();


       m_valores.Add(p_as_equipo.id_equipo);
       m_valores.Add(p_as_equipo.id_tipo_equipo);
       m_valores.Add(p_as_equipo.id_marca);
       m_valores.Add(p_as_equipo.id_modelo);
       m_valores.Add(p_as_equipo.mac_ethernet);
       m_valores.Add(p_as_equipo.ip_ethernet_telefono);
       m_valores.Add(p_as_equipo.mac_wifi);
       m_valores.Add(p_as_equipo.ip_wifi);
       m_valores.Add(p_as_equipo.identificador);
       m_valores.Add(p_as_equipo.observacion);
       m_valores.Add(p_as_equipo.id_detalle_linea);
       m_valores.Add(p_as_equipo.valido);


       m_reader = (int)this.ejecutar_query(this.a_sql.ToString(), m_parametros, m_valores);
       return m_reader;
  }


  /// <summary>Retorna un objeto As_equipo desde la base de datos  </summary>
  /// <returns> Devuelve un objeto As_equipo</returns>
  public As_equipo get_as_equipo(string p_id_equipo)
  {
      SqlDataReader m_reader;
      this.a_sql = new StringBuilder();
      this.a_sql.AppendLine("SELECT * FROM as_equipo WHERE ");
       this.a_sql.AppendLine("id_equipo_pk = @id_equipo_pk ");


       ArrayList m_parametros = new ArrayList();

        m_parametros.Add("@id_equipo_pk");


       ArrayList m_valores = new ArrayList();

       m_valores.Add(p_id_equipo);



      m_reader = (SqlDataReader)this.ejecutar_query(this.a_sql.ToString(), m_parametros, m_valores);

     if (m_reader.Read())
     {
       As_equipo m_as_equipo= new As_equipo();
       m_as_equipo.id_equipo = m_reader["id_equipo_pk"].ToString();
       m_as_equipo.id_tipo_equipo = m_reader["id_tipo_equipo_fk"].ToString();
       m_as_equipo.id_marca = m_reader["id_marca_fk"].ToString();
       m_as_equipo.id_modelo = m_reader["id_modelo_fk"].ToString();
       m_as_equipo.mac_ethernet = m_reader["mac_ethernet"].ToString();
       m_as_equipo.ip_ethernet_telefono = m_reader["ip_ethernet_telefono"].ToString();
       m_as_equipo.mac_wifi = m_reader["mac_wifi"].ToString();
       m_as_equipo.ip_wifi = m_reader["ip_wifi"].ToString();
       m_as_equipo.identificador = m_reader["identificador"].ToString();
       m_as_equipo.observacion = m_reader["observacion"].ToString();
       m_as_equipo.id_detalle_linea = m_reader["id_detalle_linea_fk"].ToString();
       m_as_equipo.valido = m_reader["valido"].ToString();

        m_reader.Dispose();

        return( m_as_equipo);
     }
     else
     {
        m_reader.Dispose();
        return (null);
     }
  }


  /// <summary>Retorna una lista de codigo primarios para los objetos As_equipo desde la base de datos </summary>
  /// <returns> Devuelve una lista de c�digo claves del objeto </returns>
  public ArrayList get_lista_as_equipos(Lista p_lista)
  {
    SqlDataReader m_reader;
    this.a_sql = new StringBuilder();

    this.a_sql.AppendLine("SELECT * FROM as_equipo");

    if (p_lista.sql_parametros.Count > 0)
       {
                int m_largo = p_lista.sql_parametros.Count;
                string m_where = " WHERE ";

                for (int i = 0; i < m_largo; i++)
                {
              m_where = m_where + p_lista.sql_parametros[i] + " LIKE @" + p_lista.sql_parametros[i] + " ";

              if (i < (m_largo-1) )
                  m_where = m_where + " AND ";

              p_lista.sql_parametros[i] = "@" + p_lista.sql_parametros[i];
                }
              this.a_sql.AppendLine(m_where);
       }

       m_reader = (SqlDataReader)this.ejecutar_query(this.a_sql.ToString(), p_lista.sql_parametros, p_lista.sql_valores);
       ArrayList m_lista = new ArrayList();

       while (m_reader.Read())
       {
       m_lista.Add(m_reader["id_equipo_pk"].ToString());
       }
       m_reader.Dispose();

       return m_lista;
  }


  public bool existe_as_equipo(As_equipo p_as_equipo)
  {
    SqlDataReader m_reader;
    this.a_sql = new StringBuilder();
    
    this.a_sql.AppendLine("SELECT * FROM as_equipo WHERE ");
       this.a_sql.AppendLine("id_equipo_pk = @id_equipo_pk ");

    
    return(false);
  }

} // FIN DE LA CLASE FAC_ SISTEMA_AS_EQUIPO
