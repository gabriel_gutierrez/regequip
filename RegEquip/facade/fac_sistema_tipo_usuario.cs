/**::::::::::::::::::::::::::::::::::::::::::
 *
 * Clase: Fachada base de datos para el objeto tipo_usuario
 * Clase responsable: sistema
 * @author Gabriel Gutierrez Carcamo <gabriel.gutierrez@emfe.cl>
 * @category Fachada base de datos de la aplicaci�n
 * @copyright Sistema de Registro de Equipos (c) 2018
 * @copyright 
 * @version 0.1 lunes, 19 de marzo de 2018 (11:35 Hr.)
 * 
 * :::::::::::::::::::::::::::::::::::::::::::*/

/** Realiza el mapeo desde la base de datos relacional a objeto tipo_usuario */

using System.Data.SqlClient;
using System.Text;
using System.Collections;


public class  Fac_sistema_tipo_usuario : FacadeSQLServer2005
{
 public Fac_sistema_tipo_usuario(string p_string_conexion) : base(p_string_conexion) { } 


  /// <summary>Ingresar un objeto Tipo_usuario a la base de datos</summary>
  /// <param name="p_tipo_usuario" >objeto tipo_usuario</param> 
  /// <returns> Devuelve un verdadero si se ingreso correctamente, en caso contrario un falso.</returns>
  public int ingresar_tipo_usuario(Tipo_usuario p_tipo_usuario)
  {
       int m_reader;
       this.a_sql = new StringBuilder();
       this.a_sql.AppendLine("INSERT INTO tipo_usuario(id_tipo_usuario_pk, descripcion) ");
       this.a_sql.AppendLine("VALUES (@id_tipo_usuario_pk, @descripcion);");

       ArrayList m_parametros = new ArrayList();

        m_parametros.Add("@id_tipo_usuario_pk");
        m_parametros.Add("@descripcion");


       ArrayList m_valores = new ArrayList();

       m_valores.Add(p_tipo_usuario.id_tipo_usuario);
       m_valores.Add(p_tipo_usuario.descripcion);


       m_reader = (int)this.ejecutar_query(this.a_sql.ToString(), m_parametros, m_valores);
       return m_reader;
  }


  /// <summary>Modificar un objeto Tipo_usuario en la base de datos </summary>
  /// <param name="p_tipo_usuario" >objeto tipo_usuario</param> 
  /// <returns> Devuelve la cantidad de fila afectadas.</returns>
  public int modificar_tipo_usuario(Tipo_usuario p_tipo_usuario)
  {
       int m_reader;
       this.a_sql = new StringBuilder();
       this.a_sql.AppendLine("UPDATE tipo_usuario SET ");


       this.a_sql.AppendLine("descripcion = @descripcion ");


       this.a_sql.AppendLine(" WHERE ");
       this.a_sql.AppendLine("id_tipo_usuario_pk = @id_tipo_usuario_pk ");


       ArrayList m_parametros = new ArrayList();


        m_parametros.Add("@id_tipo_usuario_pk");
        m_parametros.Add("@descripcion");


       ArrayList m_valores = new ArrayList();


       m_valores.Add(p_tipo_usuario.id_tipo_usuario);
       m_valores.Add(p_tipo_usuario.descripcion);


       m_reader = (int)this.ejecutar_query(this.a_sql.ToString(), m_parametros, m_valores);
       return m_reader;
  }


  /// <summary>Retorna un objeto Tipo_usuario desde la base de datos  </summary>
  /// <returns> Devuelve un objeto Tipo_usuario</returns>
  public Tipo_usuario get_tipo_usuario(string p_id_tipo_usuario)
  {
      SqlDataReader m_reader;
      this.a_sql = new StringBuilder();
      this.a_sql.AppendLine("SELECT * FROM tipo_usuario WHERE ");
       this.a_sql.AppendLine("id_tipo_usuario_pk = @id_tipo_usuario_pk ");


       ArrayList m_parametros = new ArrayList();

        m_parametros.Add("@id_tipo_usuario_pk");


       ArrayList m_valores = new ArrayList();

       m_valores.Add(p_id_tipo_usuario);



      m_reader = (SqlDataReader)this.ejecutar_query(this.a_sql.ToString(), m_parametros, m_valores);

     if (m_reader.Read())
     {
       Tipo_usuario m_tipo_usuario= new Tipo_usuario();
       m_tipo_usuario.id_tipo_usuario = m_reader["id_tipo_usuario_pk"].ToString();
       m_tipo_usuario.descripcion = m_reader["descripcion"].ToString();

        m_reader.Dispose();

        return( m_tipo_usuario);
     }
     else
     {
        m_reader.Dispose();
        return (null);
     }
  }


  /// <summary>Retorna una lista de codigo primarios para los objetos Tipo_usuario desde la base de datos </summary>
  /// <returns> Devuelve una lista de c�digo claves del objeto </returns>
  public ArrayList get_lista_tipo_usuarios(Lista p_lista)
  {
    SqlDataReader m_reader;
    this.a_sql = new StringBuilder();

    this.a_sql.AppendLine("SELECT * FROM tipo_usuario");

    if (p_lista.sql_parametros.Count > 0)
       {
                int m_largo = p_lista.sql_parametros.Count;
                string m_where = " WHERE ";

                for (int i = 0; i < m_largo; i++)
                {
              m_where = m_where + p_lista.sql_parametros[i] + " LIKE @" + p_lista.sql_parametros[i] + " ";

              if (i < (m_largo-1) )
                  m_where = m_where + " AND ";

              p_lista.sql_parametros[i] = "@" + p_lista.sql_parametros[i];
                }
              this.a_sql.AppendLine(m_where);
       }

       m_reader = (SqlDataReader)this.ejecutar_query(this.a_sql.ToString(), p_lista.sql_parametros, p_lista.sql_valores);
       ArrayList m_lista = new ArrayList();

       while (m_reader.Read())
       {
       m_lista.Add(m_reader["id_tipo_usuario_pk"].ToString());
       }
       m_reader.Dispose();

       return m_lista;
  }


  public bool existe_tipo_usuario(Tipo_usuario p_tipo_usuario)
  {
    SqlDataReader m_reader;
    this.a_sql = new StringBuilder();
    
    this.a_sql.AppendLine("SELECT * FROM tipo_usuario WHERE ");
       this.a_sql.AppendLine("id_tipo_usuario_pk = @id_tipo_usuario_pk ");

    
    return(false);
  }

} // FIN DE LA CLASE FAC_ SISTEMA_TIPO_USUARIO
