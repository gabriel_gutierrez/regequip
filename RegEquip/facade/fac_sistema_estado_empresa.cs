/**::::::::::::::::::::::::::::::::::::::::::
 *
 * Clase: Fachada base de datos para el objeto estado_empresa
 * Clase responsable: sistema
 * @author Gabriel Gutierrez Carcamo <gabriel.gutierrez@emfe.cl>
 * @category Fachada base de datos de la aplicaci�n
 * @copyright Sistema de Registro de Equipos (c) 2018
 * @copyright 
 * @version 0.1 lunes, 19 de marzo de 2018 (12:15 Hr.)
 * 
 * :::::::::::::::::::::::::::::::::::::::::::*/

/** Realiza el mapeo desde la base de datos relacional a objeto estado_empresa */

using System.Data.SqlClient;
using System.Text;
using System.Collections;


public class  Fac_sistema_estado_empresa : FacadeSQLServer2005
{
 public Fac_sistema_estado_empresa(string p_string_conexion) : base(p_string_conexion) { } 


  /// <summary>Ingresar un objeto Estado_empresa a la base de datos</summary>
  /// <param name="p_estado_empresa" >objeto estado_empresa</param> 
  /// <returns> Devuelve un verdadero si se ingreso correctamente, en caso contrario un falso.</returns>
  public int ingresar_estado_empresa(Estado_empresa p_estado_empresa)
  {
       int m_reader;
       this.a_sql = new StringBuilder();
       this.a_sql.AppendLine("INSERT INTO estado_empresa(id_estado_pk, descripcion) ");
       this.a_sql.AppendLine("VALUES (@id_estado_pk, @descripcion);");

       ArrayList m_parametros = new ArrayList();

        m_parametros.Add("@id_estado_pk");
        m_parametros.Add("@descripcion");


       ArrayList m_valores = new ArrayList();

       m_valores.Add(p_estado_empresa.id_estado);
       m_valores.Add(p_estado_empresa.descripcion);


       m_reader = (int)this.ejecutar_query(this.a_sql.ToString(), m_parametros, m_valores);
       return m_reader;
  }


  /// <summary>Modificar un objeto Estado_empresa en la base de datos </summary>
  /// <param name="p_estado_empresa" >objeto estado_empresa</param> 
  /// <returns> Devuelve la cantidad de fila afectadas.</returns>
  public int modificar_estado_empresa(Estado_empresa p_estado_empresa)
  {
       int m_reader;
       this.a_sql = new StringBuilder();
       this.a_sql.AppendLine("UPDATE estado_empresa SET ");


       this.a_sql.AppendLine("descripcion = @descripcion ");


       this.a_sql.AppendLine(" WHERE ");
       this.a_sql.AppendLine("id_estado_pk = @id_estado_pk ");


       ArrayList m_parametros = new ArrayList();


        m_parametros.Add("@id_estado_pk");
        m_parametros.Add("@descripcion");


       ArrayList m_valores = new ArrayList();


       m_valores.Add(p_estado_empresa.id_estado);
       m_valores.Add(p_estado_empresa.descripcion);


       m_reader = (int)this.ejecutar_query(this.a_sql.ToString(), m_parametros, m_valores);
       return m_reader;
  }


  /// <summary>Retorna un objeto Estado_empresa desde la base de datos  </summary>
  /// <returns> Devuelve un objeto Estado_empresa</returns>
  public Estado_empresa get_estado_empresa(string p_id_estado)
  {
      SqlDataReader m_reader;
      this.a_sql = new StringBuilder();
      this.a_sql.AppendLine("SELECT * FROM estado_empresa WHERE ");
       this.a_sql.AppendLine("id_estado_pk = @id_estado_pk ");


       ArrayList m_parametros = new ArrayList();

        m_parametros.Add("@id_estado_pk");


       ArrayList m_valores = new ArrayList();

       m_valores.Add(p_id_estado);



      m_reader = (SqlDataReader)this.ejecutar_query(this.a_sql.ToString(), m_parametros, m_valores);

     if (m_reader.Read())
     {
       Estado_empresa m_estado_empresa= new Estado_empresa();
       m_estado_empresa.id_estado = m_reader["id_estado_pk"].ToString();
       m_estado_empresa.descripcion = m_reader["descripcion"].ToString();

        m_reader.Dispose();

        return( m_estado_empresa);
     }
     else
     {
        m_reader.Dispose();
        return (null);
     }
  }


  /// <summary>Retorna una lista de codigo primarios para los objetos Estado_empresa desde la base de datos </summary>
  /// <returns> Devuelve una lista de c�digo claves del objeto </returns>
  public ArrayList get_lista_estado_empresas(Lista p_lista)
  {
    SqlDataReader m_reader;
    this.a_sql = new StringBuilder();

    this.a_sql.AppendLine("SELECT * FROM estado_empresa");

    if (p_lista.sql_parametros.Count > 0)
       {
                int m_largo = p_lista.sql_parametros.Count;
                string m_where = " WHERE ";

                for (int i = 0; i < m_largo; i++)
                {
              m_where = m_where + p_lista.sql_parametros[i] + " LIKE @" + p_lista.sql_parametros[i] + " ";

              if (i < (m_largo-1) )
                  m_where = m_where + " AND ";

              p_lista.sql_parametros[i] = "@" + p_lista.sql_parametros[i];
                }
              this.a_sql.AppendLine(m_where);
       }

       m_reader = (SqlDataReader)this.ejecutar_query(this.a_sql.ToString(), p_lista.sql_parametros, p_lista.sql_valores);
       ArrayList m_lista = new ArrayList();

       while (m_reader.Read())
       {
       m_lista.Add(m_reader["id_estado_pk"].ToString());
       }
       m_reader.Dispose();

       return m_lista;
  }


  public bool existe_estado_empresa(Estado_empresa p_estado_empresa)
  {
    SqlDataReader m_reader;
    this.a_sql = new StringBuilder();
    
    this.a_sql.AppendLine("SELECT * FROM estado_empresa WHERE ");
       this.a_sql.AppendLine("id_estado_pk = @id_estado_pk ");

    
    return(false);
  }

} // FIN DE LA CLASE FAC_ SISTEMA_ESTADO_EMPRESA
