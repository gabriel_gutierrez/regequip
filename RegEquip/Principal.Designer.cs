﻿namespace RegEquip
{
    partial class Principal
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle4 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle2 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle3 = new System.Windows.Forms.DataGridViewCellStyle();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Principal));
            this.tabPrincipal = new System.Windows.Forms.TabControl();
            this.tabPage1 = new System.Windows.Forms.TabPage();
            this.btnSubir = new System.Windows.Forms.Button();
            this.btnRegistro = new System.Windows.Forms.Button();
            this.gbChkList = new System.Windows.Forms.GroupBox();
            this.gbCelular = new System.Windows.Forms.GroupBox();
            this.chkI = new System.Windows.Forms.CheckBox();
            this.chkJ = new System.Windows.Forms.CheckBox();
            this.gbEquipos = new System.Windows.Forms.GroupBox();
            this.chkA = new System.Windows.Forms.CheckBox();
            this.chkF = new System.Windows.Forms.CheckBox();
            this.chkG = new System.Windows.Forms.CheckBox();
            this.chkE = new System.Windows.Forms.CheckBox();
            this.chkH = new System.Windows.Forms.CheckBox();
            this.chkB = new System.Windows.Forms.CheckBox();
            this.chkC = new System.Windows.Forms.CheckBox();
            this.chkD = new System.Windows.Forms.CheckBox();
            this.gbDatosEquipo = new System.Windows.Forms.GroupBox();
            this.gvEquipos = new System.Windows.Forms.DataGridView();
            this.gbDatosUser = new System.Windows.Forms.GroupBox();
            this.cbUsers = new System.Windows.Forms.ComboBox();
            this.lblUser = new System.Windows.Forms.Label();
            this.tabPage2 = new System.Windows.Forms.TabPage();
            this.lblFecha = new System.Windows.Forms.Label();
            this.clNumber = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.clItem = new System.Windows.Forms.DataGridViewComboBoxColumn();
            this.clMarca = new System.Windows.Forms.DataGridViewComboBoxColumn();
            this.clModelo = new System.Windows.Forms.DataGridViewComboBoxColumn();
            this.clPropA = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.clIPA = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.clMacB = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.clIP2 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.clID = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.clMomento = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this.clHasta = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.clObs = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.tabPrincipal.SuspendLayout();
            this.tabPage1.SuspendLayout();
            this.gbChkList.SuspendLayout();
            this.gbCelular.SuspendLayout();
            this.gbEquipos.SuspendLayout();
            this.gbDatosEquipo.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gvEquipos)).BeginInit();
            this.gbDatosUser.SuspendLayout();
            this.SuspendLayout();
            // 
            // tabPrincipal
            // 
            this.tabPrincipal.Controls.Add(this.tabPage1);
            this.tabPrincipal.Controls.Add(this.tabPage2);
            this.tabPrincipal.Location = new System.Drawing.Point(12, 27);
            this.tabPrincipal.Name = "tabPrincipal";
            this.tabPrincipal.SelectedIndex = 0;
            this.tabPrincipal.Size = new System.Drawing.Size(754, 513);
            this.tabPrincipal.TabIndex = 0;
            // 
            // tabPage1
            // 
            this.tabPage1.Controls.Add(this.btnSubir);
            this.tabPage1.Controls.Add(this.btnRegistro);
            this.tabPage1.Controls.Add(this.gbChkList);
            this.tabPage1.Controls.Add(this.gbDatosEquipo);
            this.tabPage1.Controls.Add(this.gbDatosUser);
            this.tabPage1.Location = new System.Drawing.Point(4, 22);
            this.tabPage1.Name = "tabPage1";
            this.tabPage1.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage1.Size = new System.Drawing.Size(746, 487);
            this.tabPage1.TabIndex = 2;
            this.tabPage1.Text = "Retiro";
            this.tabPage1.UseVisualStyleBackColor = true;
            // 
            // btnSubir
            // 
            this.btnSubir.Enabled = false;
            this.btnSubir.Location = new System.Drawing.Point(607, 420);
            this.btnSubir.Name = "btnSubir";
            this.btnSubir.Size = new System.Drawing.Size(84, 38);
            this.btnSubir.TabIndex = 5;
            this.btnSubir.Text = "Subir Documento";
            this.btnSubir.UseVisualStyleBackColor = true;
            this.btnSubir.Click += new System.EventHandler(this.btnSubir_Click);
            // 
            // btnRegistro
            // 
            this.btnRegistro.Enabled = false;
            this.btnRegistro.Location = new System.Drawing.Point(494, 420);
            this.btnRegistro.Name = "btnRegistro";
            this.btnRegistro.Size = new System.Drawing.Size(84, 38);
            this.btnRegistro.TabIndex = 4;
            this.btnRegistro.Text = "Registrar";
            this.btnRegistro.UseVisualStyleBackColor = true;
            this.btnRegistro.Click += new System.EventHandler(this.btnRegistro_Click);
            // 
            // gbChkList
            // 
            this.gbChkList.Controls.Add(this.gbCelular);
            this.gbChkList.Controls.Add(this.gbEquipos);
            this.gbChkList.Location = new System.Drawing.Point(7, 281);
            this.gbChkList.Name = "gbChkList";
            this.gbChkList.Size = new System.Drawing.Size(733, 133);
            this.gbChkList.TabIndex = 3;
            this.gbChkList.TabStop = false;
            this.gbChkList.Text = "CheckList";
            // 
            // gbCelular
            // 
            this.gbCelular.Controls.Add(this.chkI);
            this.gbCelular.Controls.Add(this.chkJ);
            this.gbCelular.Location = new System.Drawing.Point(621, 19);
            this.gbCelular.Name = "gbCelular";
            this.gbCelular.Size = new System.Drawing.Size(106, 100);
            this.gbCelular.TabIndex = 11;
            this.gbCelular.TabStop = false;
            this.gbCelular.Text = "Celulares";
            // 
            // chkI
            // 
            this.chkI.AutoSize = true;
            this.chkI.Location = new System.Drawing.Point(28, 28);
            this.chkI.Name = "chkI";
            this.chkI.Size = new System.Drawing.Size(49, 17);
            this.chkI.TabIndex = 8;
            this.chkI.Text = "MAC";
            this.chkI.UseVisualStyleBackColor = true;
            // 
            // chkJ
            // 
            this.chkJ.AutoSize = true;
            this.chkJ.Location = new System.Drawing.Point(28, 58);
            this.chkJ.Name = "chkJ";
            this.chkJ.Size = new System.Drawing.Size(48, 17);
            this.chkJ.TabIndex = 9;
            this.chkJ.Text = "IMEI";
            this.chkJ.UseVisualStyleBackColor = true;
            // 
            // gbEquipos
            // 
            this.gbEquipos.Controls.Add(this.chkA);
            this.gbEquipos.Controls.Add(this.chkF);
            this.gbEquipos.Controls.Add(this.chkG);
            this.gbEquipos.Controls.Add(this.chkE);
            this.gbEquipos.Controls.Add(this.chkH);
            this.gbEquipos.Controls.Add(this.chkB);
            this.gbEquipos.Controls.Add(this.chkC);
            this.gbEquipos.Controls.Add(this.chkD);
            this.gbEquipos.Location = new System.Drawing.Point(9, 19);
            this.gbEquipos.Name = "gbEquipos";
            this.gbEquipos.Size = new System.Drawing.Size(590, 100);
            this.gbEquipos.TabIndex = 10;
            this.gbEquipos.TabStop = false;
            this.gbEquipos.Text = "Equipos";
            // 
            // chkA
            // 
            this.chkA.AutoSize = true;
            this.chkA.Location = new System.Drawing.Point(29, 28);
            this.chkA.Name = "chkA";
            this.chkA.Size = new System.Drawing.Size(102, 17);
            this.chkA.TabIndex = 0;
            this.chkA.Text = "Acceso Remoto";
            this.chkA.UseVisualStyleBackColor = true;
            // 
            // chkF
            // 
            this.chkF.AutoSize = true;
            this.chkF.Location = new System.Drawing.Point(498, 58);
            this.chkF.Name = "chkF";
            this.chkF.Size = new System.Drawing.Size(62, 17);
            this.chkF.TabIndex = 5;
            this.chkF.Text = "Internet";
            this.chkF.UseVisualStyleBackColor = true;
            // 
            // chkG
            // 
            this.chkG.AutoSize = true;
            this.chkG.Location = new System.Drawing.Point(29, 58);
            this.chkG.Name = "chkG";
            this.chkG.Size = new System.Drawing.Size(54, 17);
            this.chkG.TabIndex = 6;
            this.chkG.Text = "Office";
            this.chkG.UseVisualStyleBackColor = true;
            // 
            // chkE
            // 
            this.chkE.AutoSize = true;
            this.chkE.Location = new System.Drawing.Point(357, 58);
            this.chkE.Name = "chkE";
            this.chkE.Size = new System.Drawing.Size(72, 17);
            this.chkE.TabIndex = 4;
            this.chkE.Text = "Impresora";
            this.chkE.UseVisualStyleBackColor = true;
            // 
            // chkH
            // 
            this.chkH.AutoSize = true;
            this.chkH.Location = new System.Drawing.Point(197, 58);
            this.chkH.Name = "chkH";
            this.chkH.Size = new System.Drawing.Size(57, 17);
            this.chkH.TabIndex = 7;
            this.chkH.Text = "Winrar";
            this.chkH.UseVisualStyleBackColor = true;
            // 
            // chkB
            // 
            this.chkB.AutoSize = true;
            this.chkB.Location = new System.Drawing.Point(197, 28);
            this.chkB.Name = "chkB";
            this.chkB.Size = new System.Drawing.Size(57, 17);
            this.chkB.TabIndex = 1;
            this.chkB.Text = "Adobe";
            this.chkB.UseVisualStyleBackColor = true;
            // 
            // chkC
            // 
            this.chkC.AutoSize = true;
            this.chkC.Location = new System.Drawing.Point(357, 28);
            this.chkC.Name = "chkC";
            this.chkC.Size = new System.Drawing.Size(66, 17);
            this.chkC.TabIndex = 2;
            this.chkC.Text = "Antivirus";
            this.chkC.UseVisualStyleBackColor = true;
            // 
            // chkD
            // 
            this.chkD.AutoSize = true;
            this.chkD.Location = new System.Drawing.Point(498, 28);
            this.chkD.Name = "chkD";
            this.chkD.Size = new System.Drawing.Size(64, 17);
            this.chkD.TabIndex = 3;
            this.chkD.Text = "Dominio";
            this.chkD.UseVisualStyleBackColor = true;
            // 
            // gbDatosEquipo
            // 
            this.gbDatosEquipo.Controls.Add(this.gvEquipos);
            this.gbDatosEquipo.Location = new System.Drawing.Point(7, 123);
            this.gbDatosEquipo.Name = "gbDatosEquipo";
            this.gbDatosEquipo.Size = new System.Drawing.Size(733, 141);
            this.gbDatosEquipo.TabIndex = 2;
            this.gbDatosEquipo.TabStop = false;
            this.gbDatosEquipo.Text = "Equipo";
            // 
            // gvEquipos
            // 
            dataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle1.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle1.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle1.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle1.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle1.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle1.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.gvEquipos.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle1;
            this.gvEquipos.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.gvEquipos.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.clNumber,
            this.clItem,
            this.clMarca,
            this.clModelo,
            this.clPropA,
            this.clIPA,
            this.clMacB,
            this.clIP2,
            this.clID,
            this.clMomento,
            this.clHasta,
            this.clObs});
            dataGridViewCellStyle4.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle4.BackColor = System.Drawing.SystemColors.Window;
            dataGridViewCellStyle4.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle4.ForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle4.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle4.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle4.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.gvEquipos.DefaultCellStyle = dataGridViewCellStyle4;
            this.gvEquipos.Location = new System.Drawing.Point(9, 19);
            this.gvEquipos.Name = "gvEquipos";
            this.gvEquipos.Size = new System.Drawing.Size(718, 116);
            this.gvEquipos.TabIndex = 0;
            this.gvEquipos.CellClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.gvEquipos_CellClick);
            this.gvEquipos.ColumnWidthChanged += new System.Windows.Forms.DataGridViewColumnEventHandler(this.gvEquipos_ColumnWidthChanged);
            this.gvEquipos.RowPostPaint += new System.Windows.Forms.DataGridViewRowPostPaintEventHandler(this.gvEquipos_RowPostPaint);
            this.gvEquipos.RowsAdded += new System.Windows.Forms.DataGridViewRowsAddedEventHandler(this.gvEquipos_RowsAdded);
            this.gvEquipos.Scroll += new System.Windows.Forms.ScrollEventHandler(this.gvEquipos_Scroll);
            // 
            // gbDatosUser
            // 
            this.gbDatosUser.Controls.Add(this.cbUsers);
            this.gbDatosUser.Controls.Add(this.lblUser);
            this.gbDatosUser.Location = new System.Drawing.Point(7, 7);
            this.gbDatosUser.Name = "gbDatosUser";
            this.gbDatosUser.Size = new System.Drawing.Size(733, 100);
            this.gbDatosUser.TabIndex = 1;
            this.gbDatosUser.TabStop = false;
            this.gbDatosUser.Text = "Datos Usuario";
            // 
            // cbUsers
            // 
            this.cbUsers.FormattingEnabled = true;
            this.cbUsers.ItemHeight = 13;
            this.cbUsers.Location = new System.Drawing.Point(56, 25);
            this.cbUsers.Name = "cbUsers";
            this.cbUsers.Size = new System.Drawing.Size(247, 21);
            this.cbUsers.TabIndex = 1;
            this.cbUsers.SelectedIndexChanged += new System.EventHandler(this.cbUsers_SelectedIndexChanged);
            // 
            // lblUser
            // 
            this.lblUser.AutoSize = true;
            this.lblUser.Location = new System.Drawing.Point(6, 28);
            this.lblUser.Name = "lblUser";
            this.lblUser.Size = new System.Drawing.Size(43, 13);
            this.lblUser.TabIndex = 0;
            this.lblUser.Text = "Usuario";
            // 
            // tabPage2
            // 
            this.tabPage2.Location = new System.Drawing.Point(4, 22);
            this.tabPage2.Name = "tabPage2";
            this.tabPage2.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage2.Size = new System.Drawing.Size(746, 487);
            this.tabPage2.TabIndex = 3;
            this.tabPage2.Text = "Recepción";
            this.tabPage2.UseVisualStyleBackColor = true;
            // 
            // lblFecha
            // 
            this.lblFecha.AutoSize = true;
            this.lblFecha.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblFecha.Location = new System.Drawing.Point(605, 9);
            this.lblFecha.Name = "lblFecha";
            this.lblFecha.Size = new System.Drawing.Size(50, 15);
            this.lblFecha.TabIndex = 0;
            this.lblFecha.Text = "Fecha:";
            // 
            // clNumber
            // 
            dataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle2.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle2.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle2.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle2.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle2.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle2.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.clNumber.DefaultCellStyle = dataGridViewCellStyle2;
            this.clNumber.HeaderText = "#";
            this.clNumber.Name = "clNumber";
            this.clNumber.Width = 30;
            // 
            // clItem
            // 
            this.clItem.HeaderText = "Item";
            this.clItem.Name = "clItem";
            this.clItem.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            this.clItem.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Automatic;
            // 
            // clMarca
            // 
            this.clMarca.HeaderText = "Marca";
            this.clMarca.Name = "clMarca";
            this.clMarca.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            this.clMarca.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Automatic;
            // 
            // clModelo
            // 
            this.clModelo.HeaderText = "Modelo";
            this.clModelo.Name = "clModelo";
            this.clModelo.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Automatic;
            // 
            // clPropA
            // 
            this.clPropA.HeaderText = "MAC Ethernet";
            this.clPropA.Name = "clPropA";
            this.clPropA.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            this.clPropA.Width = 150;
            // 
            // clIPA
            // 
            this.clIPA.HeaderText = "IP Ethernet / N° Telefónico";
            this.clIPA.Name = "clIPA";
            // 
            // clMacB
            // 
            this.clMacB.HeaderText = "MAC WIFI";
            this.clMacB.Name = "clMacB";
            // 
            // clIP2
            // 
            this.clIP2.HeaderText = "IP WIFI";
            this.clIP2.Name = "clIP2";
            // 
            // clID
            // 
            this.clID.HeaderText = "Identificador";
            this.clID.Name = "clID";
            // 
            // clMomento
            // 
            this.clMomento.FalseValue = false;
            this.clMomento.HeaderText = "Momentaneo";
            this.clMomento.IndeterminateValue = "0";
            this.clMomento.Name = "clMomento";
            this.clMomento.TrueValue = true;
            // 
            // clHasta
            // 
            dataGridViewCellStyle3.Format = "d";
            dataGridViewCellStyle3.NullValue = null;
            this.clHasta.DefaultCellStyle = dataGridViewCellStyle3;
            this.clHasta.HeaderText = "Hasta";
            this.clHasta.Name = "clHasta";
            this.clHasta.ReadOnly = true;
            // 
            // clObs
            // 
            this.clObs.HeaderText = "Observación";
            this.clObs.Name = "clObs";
            // 
            // Principal
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(778, 550);
            this.Controls.Add(this.lblFecha);
            this.Controls.Add(this.tabPrincipal);
            this.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MaximizeBox = false;
            this.Name = "Principal";
            this.Text = "Registro de Asignaciones de Equipos a Usuarios";
            this.FormClosed += new System.Windows.Forms.FormClosedEventHandler(this.Principal_FormClosed);
            this.Load += new System.EventHandler(this.Principal_Load);
            this.tabPrincipal.ResumeLayout(false);
            this.tabPage1.ResumeLayout(false);
            this.gbChkList.ResumeLayout(false);
            this.gbCelular.ResumeLayout(false);
            this.gbCelular.PerformLayout();
            this.gbEquipos.ResumeLayout(false);
            this.gbEquipos.PerformLayout();
            this.gbDatosEquipo.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.gvEquipos)).EndInit();
            this.gbDatosUser.ResumeLayout(false);
            this.gbDatosUser.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TabControl tabPrincipal;
        private System.Windows.Forms.TabPage tabPage1;
        private System.Windows.Forms.TabPage tabPage2;
        private System.Windows.Forms.Label lblFecha;
        private System.Windows.Forms.GroupBox gbDatosUser;
        private System.Windows.Forms.ComboBox cbUsers;
        private System.Windows.Forms.Label lblUser;
        private System.Windows.Forms.GroupBox gbChkList;
        private System.Windows.Forms.GroupBox gbDatosEquipo;
        private System.Windows.Forms.CheckBox chkH;
        private System.Windows.Forms.CheckBox chkG;
        private System.Windows.Forms.CheckBox chkF;
        private System.Windows.Forms.CheckBox chkE;
        private System.Windows.Forms.CheckBox chkD;
        private System.Windows.Forms.CheckBox chkC;
        private System.Windows.Forms.CheckBox chkB;
        private System.Windows.Forms.CheckBox chkA;
        private System.Windows.Forms.DataGridView gvEquipos;
        private System.Windows.Forms.Button btnRegistro;
        private System.Windows.Forms.CheckBox chkI;
        private System.Windows.Forms.CheckBox chkJ;
        private System.Windows.Forms.GroupBox gbCelular;
        private System.Windows.Forms.GroupBox gbEquipos;
        private System.Windows.Forms.Button btnSubir;
        private System.Windows.Forms.DataGridViewTextBoxColumn clNumber;
        private System.Windows.Forms.DataGridViewComboBoxColumn clItem;
        private System.Windows.Forms.DataGridViewComboBoxColumn clMarca;
        private System.Windows.Forms.DataGridViewComboBoxColumn clModelo;
        private System.Windows.Forms.DataGridViewTextBoxColumn clPropA;
        private System.Windows.Forms.DataGridViewTextBoxColumn clIPA;
        private System.Windows.Forms.DataGridViewTextBoxColumn clMacB;
        private System.Windows.Forms.DataGridViewTextBoxColumn clIP2;
        private System.Windows.Forms.DataGridViewTextBoxColumn clID;
        private System.Windows.Forms.DataGridViewCheckBoxColumn clMomento;
        private System.Windows.Forms.DataGridViewTextBoxColumn clHasta;
        private System.Windows.Forms.DataGridViewTextBoxColumn clObs;
    }
}