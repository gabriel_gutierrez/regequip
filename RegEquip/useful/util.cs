using System;
using System.Text;
using System.Net.Mail;
using System.Globalization;
using System.Text.RegularExpressions;
using System.Collections.Generic;
/// <summary>
/// Clase que agrega funciones adicionales a C#
/// </summary>
public class Util
{
	public Util()
	{
		//
		// TODO: Add constructor logic here
		//
	}
/* �--------------------------------------------------------------� */

/* �-- Pone en may�sculas el primer car�cter de cada palabra de --� */
/* �-- Una Cadena.                                              --� */
    public string ucwords(string p_cadena)
    {
        string[] PartsLine = p_cadena.Split(' ');
       
        string m_caracter;
        string m_cadena_final = "";

        foreach (string item in PartsLine)
        {
            if (item != "")
            {
                m_caracter = item.Substring(0, 1);
                p_cadena = item.Remove(0, 1);
                p_cadena = p_cadena.ToLower();
               
                m_cadena_final = m_cadena_final + m_caracter.ToUpper() + p_cadena + ' ';
            }
        }

        return (m_cadena_final.TrimEnd());
    }
/* �--------------------------------------------------------------� */

    public bool EsCorreoValido(string email)
    {
        String expresion = "\\w+([-+.']\\w+)*@\\w+([-.]\\w+)*\\.\\w+([-.]\\w+)*";
        if (Regex.IsMatch(email, expresion))
        {
            if (Regex.Replace(email, expresion, String.Empty).Length == 0)
            {
                return true;
            }
            else
            {
                return false;
            }
        }
        else
        {
            return false;
        }
    
    }
         
  
/* �-VERIFICA Y CORTA LA CADENA DE TEXT AREA A PARTIR DE UN LARGO-� */
    public string largo_texarea(string p_texto, int p_largo_max)
    {
        string m_texto = "";

        if (p_texto.Length > p_largo_max)
            m_texto = p_texto.Substring(0, p_largo_max);
        else
            m_texto = p_texto.Substring(0, (p_texto.Length));

        return (m_texto);
    
    }
/* �--------------------------------------------------------------� */

/* �-VERIFICA Y CORTA LA CADENA DE TEXT AREA A PARTIR DE UN LARGO-� */
    public string encriptar(string p_texto)
    {
        string rethash = "";
        try
        {
            System.Security.Cryptography.SHA1 hash = System.Security.Cryptography.SHA1.Create();
            System.Text.ASCIIEncoding encoder = new System.Text.ASCIIEncoding();
            byte[] combined = encoder.GetBytes(p_texto);
            hash.ComputeHash(combined);
            rethash = Convert.ToBase64String(hash.Hash);
        }
        catch (Exception ex)
        {
            string strerr = "Error al Encriptar : " + ex.Message;
        }
        return rethash;

    }
/* �--------------------------------------------------------------� */


/* �-VERIFICA Y CORTA LA CADENA DE TEXT AREA A PARTIR DE UN LARGO-� */
    public string desencriptar(string p_texto)
    {

        int m_largo = p_texto.Length;
        StringBuilder m_cadena = new StringBuilder("");
        int m_numero = 0;
      
        // Split string on spaces. This will separate all the words in a string
        string[] words = p_texto.Split((char)97, (char)101);
        foreach (string word in words)
        {
            if (word.CompareTo("") != 0)
            {
                m_numero = Int32.Parse(word);
                m_cadena.Append((char)m_numero);
            }
        }

        return (m_cadena.ToString());

    }
/* �--------------------------------------------------------------� */


    /* �-VERIFICA Y CORTA LA CADENA DE TEXT AREA A PARTIR DE UN LARGO-� */
    public string FormatoSqlFecha(string p_textoFecha,bool larga)
    {
        try
        {
            DateTime fecha = Convert.ToDateTime(p_textoFecha);
            if (larga)
            {
                return fecha.ToString("yyyyMMdd HH:mm:ss");
            }
            else
            {
                return fecha.ToString("yyyyMMdd");
            }
        }
        catch (Exception)
        {

            return "NULL";
        }



    }

    public string FormatoSqlFecha(string p_textoFecha)
    {
        return this.FormatoSqlFecha(p_textoFecha, true);
    }
    /* �--------------------------------------------------------------� */


    public string obtenerHora(string p_fecha)
    {
        
        try
        {
            DateTime fecha = Convert.ToDateTime(p_fecha);
            return fecha.ToString("HH:mm");
        }
        catch (Exception)
        {

            return "";
        }

    }

    public string obtenerFecha(string p_fecha)
    {
        try
        {
            DateTime fecha = Convert.ToDateTime(p_fecha);
            return fecha.ToString("dd/MM/yyyy");
        }
        catch (Exception)
        {

            return "";
        }

    }
    public string obtenerFecha(DateTime p_fecha)
    {
        try
        {

            return p_fecha.ToString("dd/MM/yyyy");
        }
        catch (Exception)
        {

            return "";
        }

    }
     
    /// <summary>
    ///  Devuelve una fecha a partir del string ingresado
    /// </summary>
    /// <param name="p_fecha">De tipo String</param>
    /// <returns>DateTime</returns>
    public DateTime obtenerFechaDateTime(string p_fecha)
    {
        
            DateTime fecha = Convert.ToDateTime(p_fecha);
            return fecha;
         

    }

    /// <summary>
    ///  Devuelve una fecha a partir del string ingresado sin la HOra o hora 00:00:0
    /// </summary>
    /// <param name="p_fecha">De tipo String</param>
    /// <returns>DateTime</returns>
    public DateTime obtenerFechaSinHora(string p_fecha)
    {

        String fechaCorta = this.obtieneFechaCorta(p_fecha);
        DateTime fechaCortaSinHora= Convert.ToDateTime(fechaCorta);
        return fechaCortaSinHora;

    }
    public DateTime obtenerFechaSinHora(DateTime p_fecha)
    {

        
        String fechaCorta = this.obtieneFechaCorta(p_fecha.ToString());
        DateTime fechaCortaSinHora = Convert.ToDateTime(fechaCorta);
        return fechaCortaSinHora;

    }
    /// <summary>
    ///  Devuelve el numero de dias entre las dos fechas
    /// </summary>
    /// <param name="p_fechaInicio">Datetime</param>
    /// <param name="p_fechaFin">Datetime</param>
    /// <returns>Int</returns>
    public int  obtenerNroDiasEntreDosFechas(DateTime p_fechaInicio, DateTime p_fechaTermino)
    {
        // obtiene el resto entre los dos dias como fecha
        TimeSpan diferencia = p_fechaTermino - p_fechaInicio; 
        // convierte la diferencia a d�as
        int dias = diferencia.Days;

        return dias;


    }

    ///<summary>
    ///Metodo que avalua dos fechas que comprenden un rango
    ///</summary>
    ///<remarks>
    /// determina si la fecha es mmayor a la actual
    ///</remarks>
    public bool validaFechaMayoraActual(string p_fecha)
    {
        try
        {
            DateTime fecha = Convert.ToDateTime(p_fecha);
            DateTime fechaActual = DateTime.Now;

            if (fecha > fechaActual)
            {

                return true;

            }
            else
            {
                return false;

            }
        }
        catch (Exception)
        {

            return false;
        }

    }

    ///<summary>
    ///Metodo que avalua dos fechas que comprenden un rango
    ///</summary>
    ///<remarks>
    /// determina si la fecha es igual a la actual
    ///</remarks>
    public bool validarFechaIgualActual(string p_fecha)
    {
        try
        {
            DateTime fecha = Convert.ToDateTime(p_fecha);
            DateTime fechaActual = DateTime.Now.Date;

            if (fecha == fechaActual)
            {

                return true;

            }
            else
            {
                return false;

            }
        }
        catch (Exception)
        {

            return false;
        }

    }


    ///<summary>
    ///Metodo que avalua dos fechas que comprenden un rango
    ///</summary>
    ///<remarks>
    /// fecha inicial y fecha termino como string
    ///</remarks>
    public bool validaRangoFecha(string p_fecha_inicio, string p_fecha_termino)
    {
        try
        {
            DateTime fechaInicio = Convert.ToDateTime(p_fecha_inicio);
            DateTime fechaTermino = Convert.ToDateTime(p_fecha_termino);

            if (fechaInicio >= fechaTermino && fechaInicio != fechaTermino)
            {

                return false;

            }
            else
            {
                return true;

            }
        }
        catch (Exception)
        {

            return false;
        }

    }
    /// <summary>
    /// Valida un rango de fecha inicial y final adem�s si cada fecha es validada con respecto al periodo
    /// recibido
    /// </summary>
    /// <param name="p_fecha_inicio"></param>
    /// <param name="p_fecha_termino"></param>
    /// <param name="m_periodo">periodo al que pertenece las fechas</param>
    /// <returns></returns>
    /// 
    /*
    public bool validaRangoFecha(string p_fecha_inicio, string p_fecha_termino, Periodo m_periodo, out string p_mensaje)
    {   // declaracion de variables 
        DateTime m_fechaInicio = Convert.ToDateTime(p_fecha_inicio);
        DateTime m_fechaTermino = Convert.ToDateTime(p_fecha_termino);

        DateTime m_fechaInicioPeriodo = m_periodo.fecha_limite_inferior;
        DateTime m_fechaTerminoPeriodo = m_periodo.fecha_limite_superior;

        bool m_condicion_fechaInicio= ( m_fechaInicioPeriodo <= m_fechaInicio && m_fechaInicio <= m_fechaTerminoPeriodo);
        bool m_condicion_fechaTermino= (m_fechaInicioPeriodo <= m_fechaTermino && m_fechaTermino <= m_fechaTerminoPeriodo);

        // valida si las fechas son validas
        if (!validaRangoFecha(p_fecha_inicio,  p_fecha_termino))
        {
            p_mensaje = "Error: El Rango de fechas no es valido, favor Revisar. ";
            return false;
        }

        if (!m_condicion_fechaInicio)
        {
            p_mensaje = "Error: La fecha Inicial del Rango no corresponde al Periodo ";
            return false;
        }

        if (!m_condicion_fechaTermino)
        {
            p_mensaje = "Error: La fecha Termino del Rango no corresponde al Periodo ";
            return false;
        }

        p_mensaje = "Rango ok";
        return true;

    }
    */
    public bool envioCorreo(string p_de, string p_para, string p_Asunto,string p_cuerpo)
    {
        // configura envio de correo
        try
        {
            MailMessage correo = new MailMessage();
            correo.From = new MailAddress(p_de);
            correo.To.Add(p_para);
            correo.Subject = p_Asunto;
            // texto cuerpo 
            correo.Body = p_cuerpo;
            correo.IsBodyHtml = true;
            correo.Priority = System.Net.Mail.MailPriority.Normal;
            System.Net.Mail.SmtpClient smtp = new System.Net.Mail.SmtpClient("mail.transmolina.cl");
            smtp.Credentials = new System.Net.NetworkCredential("SISTEMAOPERA@TRANSMOLINA.CL", "molinaMOLINA");
        
   
       
            smtp.Send(correo);
            return true;
        }
        catch (Exception )
        {
           return false;
        }
    }

    /// <summary>
    /// Funcion Retorna la Fecha actual en el formato se�alado
    /// si no se se�ala devuelve el formato de la base de datos yyyyMMdd HH:mm:ss
    /// </summary>
    /// <param name="p_formato">de tipo string ej. dd-MM-yyyy</param>
    /// <returns></returns>
    public string getFechaActual(string p_formato)
    {
        // formato
        string m_formato = "yyyyMMdd HH:mm:ss"; // formato db
        if (p_formato != "")
        {
            m_formato = p_formato; // formato especial
        }
        // para una solicitud nueva 
        DateTime m_time = DateTime.Now;
        string m_fecha_actual = m_time.ToString(m_formato);

        return m_fecha_actual;
    }
 

    public string getHoraActual()
    {
        // formato
        string m_formato = "HH:mm:ss"; // formato db
        
        // para una solicitud nueva 
        DateTime m_time = DateTime.Now;
        string m_hora_actual = m_time.ToString(m_formato);

        return m_hora_actual;
    }
    public string getHoraActualCorta()
    {
        // formato
        string m_formato = "HH:mm"; // formato db

        // para una solicitud nueva 
        DateTime m_time = DateTime.Now;
        string m_hora_actual = m_time.ToString(m_formato);

        return m_hora_actual;
    }
   public string digitoVerificador(string p_rut)
{
      int Digito;
      int Contador;
      int Multiplo;
      int Acumulador;
      string RutDigito;
      int m_rut; 
      int.TryParse(p_rut,out m_rut);

      Contador = 2;
      Acumulador = 0;

      while (m_rut != 0)
      {
      Multiplo = (m_rut % 10) * Contador;
      Acumulador = Acumulador + Multiplo;
      m_rut = m_rut/10;
      Contador = Contador + 1;
      if (Contador == 8)
            {
             Contador = 2;
            }

      }

      Digito = 11 - (Acumulador % 11);
      RutDigito = Digito.ToString().Trim();
      if (Digito == 10 )
      {
            RutDigito = "K";
      }
      if (Digito == 11)
      {
            RutDigito = "0";
      }
      return (RutDigito);
      }

   public bool esNumero(string p_numero)
   {
       double valor;
       if (double.TryParse(p_numero, out valor))
       {
           return true;
       }
       return false;
   
   
   }
   public string rut_sin_punto_y_guion(string p_rut_con_punto)
   {
       string[] texto_rut = p_rut_con_punto.Split('-');
       string rut_sin_puntos = texto_rut[0].Replace(".", string.Empty);
       return rut_sin_puntos;
   }

   public string agregarTexto(string p_texto, string p_textoNuevo)
   {
      if (p_texto != p_textoNuevo)
      {
          if (p_texto.IndexOf(p_textoNuevo) == 0 )
          {
            p_texto = p_texto +" \n"+p_textoNuevo;
          }
      }

      return p_texto;
   }

   public string NombreMes(string month)
   {
       DateTimeFormatInfo dtinfo = new CultureInfo("es-ES", false).DateTimeFormat;
       return dtinfo.GetMonthName(Convert.ToInt32(month));
   }
   private Boolean email_valido(String email)
   {
       String expresion;
       expresion = "\\w+([-+.']\\w+)*@\\w+([-.]\\w+)*\\.\\w+([-.]\\w+)*";
       if (Regex.IsMatch(email, expresion))
       {
           if (Regex.Replace(email, expresion, String.Empty).Length == 0)
           {
               return true;
           }
           else
           {
               return false;
           }
       }
       else
       {
           return false;
       }
   }
    /// <summary>
    ///  Limite de fecha ingresado en funcion al periodo 
    /// </summary>
    /// <param name="p_fecha"></param>
    /// <param name="p_id_periodo"></param>
    /// <param name="mensajeFecha"></param>
    /// <returns></returns>

    /*
   public bool validaLimiteFecha(string p_fecha, string p_id_periodo, out string mensajeFecha)
   {
       // inicio de valiables
       int anhio_sistema_inferior;
       int anhio_sistema_superior;
       int anhio_registro;
       mensajeFecha = " Error:  Error la Fecha parece inconsistente, valide los siguiente:<br/> -Fecha demasiado antigua en funci�n del periodo.<br/>-Fecha demasiado antigua en funci�n de la fecha actual.<br/> -Fecha no corresponde al periodo.<br/>-Fecha es superior fecha actual por m�s de un a�o.";
       int limite = 1; // en a�os
       // obtiene a partir del string un tipo DateTime 
       DateTime m_fecha = this.obtenerFechaDateTime(p_fecha);
       anhio_registro = m_fecha.Year;


       if (p_id_periodo != "" )
       {
           Log_sistema_periodo log_periodo = new Log_sistema_periodo();
           Periodo m_periodo = log_periodo.get_periodo(p_id_periodo);
           anhio_sistema_superior = Convert.ToInt32(m_periodo.anho)+limite;
           anhio_sistema_inferior = Convert.ToInt32(m_periodo.anho)-limite;

       }
       else
       {
           // toma la fecha actual
           anhio_sistema_superior = Convert.ToInt32(this.getFechaActual("yyyy")) + limite;
           anhio_sistema_inferior = Convert.ToInt32(this.getFechaActual("yyyy")) - limite;
       }


       // verifica el rango
       if (anhio_sistema_inferior <= anhio_registro && anhio_registro <= anhio_sistema_superior)
       {
           mensajeFecha = "";
           return true;
       }
       else
       {
           return false;
       }
   }
    */

    /// <summary>
    ///  Funci�n que valida la fecha ingresada en funci�n del periodo ingresado, devuelve un mensaje de error 
    /// </summary>
    /// <param name="p_fecha">fecha ingresada por el usuario</param>
    /// <param name="p_id_periodo"> periodo a consultar</param>
    /// <param name="mensajeFecha">mensaje de error</param>
    /// <returns></returns>

    /*
public bool validaLimiteFechaPeriodo(string p_fecha, string p_id_periodo, out string mensajeFecha)
{
   return validaLimiteFechaPeriodo(p_fecha, p_id_periodo,out mensajeFecha, true);
}


public bool validaLimiteFechaPeriodo(string p_fecha, string p_id_periodo, out string mensajeFecha, bool segunPeriodoActivo)
{
   // inicio de valiables
   DateTime fecha_periodo_inferior;
   DateTime fecha_periodo_superior;
   DateTime fecha_actual;
   mensajeFecha = " Error:  Error la Fecha parece inconsistente, valide lo siguiente:<br/> -Fecha demasiado antigua en funci�n del periodo.<br/> -Fecha no corresponde al periodo Actual.<br/> -Fecha Ingresadas Posteriores a la Actual";

   // obtiene a partir del string un tipo DateTime 
   DateTime m_fechaRegistro = this.obtenerFechaDateTime(p_fecha);


   if (p_id_periodo != "")
   {
       Log_sistema_periodo log_periodo = new Log_sistema_periodo();
       Periodo m_periodo = log_periodo.get_periodo(p_id_periodo);
       fecha_periodo_inferior = m_periodo.fecha_limite_inferior;
       fecha_periodo_superior = m_periodo.fecha_limite_superior;
       fecha_actual = DateTime.Now;


       if ( segunPeriodoActivo  && (fecha_actual >= fecha_periodo_inferior && fecha_actual >= fecha_periodo_superior))
       {
          // la fecha ingresada es menor al periodo 
           return false;
       }

       if (fecha_periodo_inferior <= m_fechaRegistro && m_fechaRegistro <= fecha_periodo_superior)
       {
           mensajeFecha = "";
           return true;

       }
       else
       {
          return false;
       }

   }
   else
   {
       mensajeFecha = "La funcion validaLimiteFechaPeriodo que valida el limite del periodo no recibio el periodo como parametro, informe a soporte, gracias";
       return false;
   }

}
*/
    /// <summary>
    ///  Funci�n que valida la fecha ingresada en funci�n del periodo ingresado, devuelve un mensaje de error 
    /// </summary>
    /// <param name="p_fecha">fecha ingresada por el usuario</param>
    /// <param name="p_id_periodo"> periodo a consultar</param>
    /// <param name="mensajeFecha">mensaje de error</param>
    /// <returns></returns>
    public bool validaFechaEnRango(string p_fechaEvaluar, string p_fechaInicioRango, string p_fechaTerminoRango)
   {
       // inicio de valiables
       DateTime fecha_periodo_inferior= Convert.ToDateTime(p_fechaInicioRango);
       DateTime fecha_periodo_superior= Convert.ToDateTime(p_fechaTerminoRango);
       DateTime fecha_evaluar = Convert.ToDateTime(p_fechaEvaluar);
       

           if (fecha_periodo_inferior <= fecha_evaluar  && fecha_evaluar <= fecha_periodo_superior)
           {
               // la fecha se ajusta a los limites del rango
               return true;
           }
   
           return false;
       

   }
   /* �------------ VALIDA SI LA CADENA ES UN NUMERO --------------� */
   public bool es_numero(string p_numero)
   {
       try
       {
           double i = Convert.ToDouble(p_numero);
           return true;
       }
       catch (FormatException)
       {
           return false;
       }
   }
   /* �--------------------------------------------------------------� */

   /* �----------- VALIDA SI LA CADENA ES UN RUT VALIDO -------------� */
   public bool es_valido_el_rut(string p_rut)
   {
       int m_largo = p_rut.Length;
       int index_ralla = p_rut.IndexOf("-", 0, m_largo);

       if (index_ralla == -1) return (false);

       string m_rut = p_rut.Substring(0, index_ralla);
       int rut;

       if (es_numero(m_rut)) rut = Convert.ToInt32(m_rut);
       else return (false);

       string m_digito = p_rut.Substring((index_ralla + 1), 1);

       string RutDigito = digito_rut(rut);

       if (RutDigito.CompareTo(m_digito) == 0) return (true);
       else return (false);

   }
   /* �--------------------------------------------------------------� */

   public string digito_rut(string rut)
   {
      return  this.digito_rut(Convert.ToInt32(rut));
   }

   public string digito_rut(int rut)
   {

         

       int Digito;
       int Contador;
       int Multiplo;
       int Acumulador;
       string RutDigito;

       Contador = 2;
       Acumulador = 0;
        

       while (rut != 0)
       {
           Multiplo = (rut % 10) * Contador;
           Acumulador = Acumulador + Multiplo;
           rut = rut / 10;
           Contador = Contador + 1;
           if (Contador == 8)
           {
               Contador = 2;
           }

       }

       Digito = 11 - (Acumulador % 11);
       RutDigito = Digito.ToString().Trim();
       if (Digito == 10)
       {
           RutDigito = "K";
       }
       if (Digito == 11)
       {
           RutDigito = "0";
       }

       return RutDigito;
   }

   public string digito_patente(String  patente)
   {
       if (!patente.Equals(""))
       {

           // inicia un map
           // TABLA DE VALORES PARA FORMATO XXXXNN
           List<String[]> serieLetraFormatoXXXXNN = new List<string[]>();
           serieLetraFormatoXXXXNN.Add(new string[] { "B", "1" });
           serieLetraFormatoXXXXNN.Add(new string[] { "C", "2" });
           serieLetraFormatoXXXXNN.Add(new string[] { "D", "3" });
           serieLetraFormatoXXXXNN.Add(new string[] { "F", "4" });
           serieLetraFormatoXXXXNN.Add(new string[] { "G", "5" });
           serieLetraFormatoXXXXNN.Add(new string[] { "H", "6" });
           serieLetraFormatoXXXXNN.Add(new string[] { "J", "7" });
           serieLetraFormatoXXXXNN.Add(new string[] { "K", "8" });
           serieLetraFormatoXXXXNN.Add(new string[] { "L", "9" });
           serieLetraFormatoXXXXNN.Add(new string[] { "P", "0" });
           serieLetraFormatoXXXXNN.Add(new string[] { "R", "2" });
           serieLetraFormatoXXXXNN.Add(new string[] { "S", "3" });
           serieLetraFormatoXXXXNN.Add(new string[] { "T", "4" });
           serieLetraFormatoXXXXNN.Add(new string[] { "V", "5" });
           serieLetraFormatoXXXXNN.Add(new string[] { "W", "6" });
           serieLetraFormatoXXXXNN.Add(new string[] { "X", "7" });
           serieLetraFormatoXXXXNN.Add(new string[] { "Y", "8" });
           serieLetraFormatoXXXXNN.Add(new string[] { "Z", "9" });

           String[] serieLetraFormatoXXNNNN = new string[] { "", "AA", "BA", "CA", "EA", "FA", "GA", "HA", "AB", "CB", "EB", "FB", "GB", "HB", "AC", "BC", "EC", "FC", "GC", "HC", "BD", "ED", "FD", "GD", "HD", "AE", "BE", "CE", "EE", "FE", "GE", "HE", "AF", "BF", "CF", "EF", "FF", "GF", "HF", "AG", "BG", "CG", "EG", "FG", "HG", "AH", "BH", "CH", "EH", "FH", "GH", "HH", "AJ", "BJ", "CJ", "EJ", "FJ", "GJ", "HJ", "BK", "CK", "EK", "FK", "GK", "HK", "AL", "BL", "CL", "EL", "FL", "GL", "HL", "AN", "BN", "CN", "EN", "FN", "GN", "HN", "AP", "BP", "CP", "EP", "FP", "GP", "HP", "AR", "BR", "CR", "ER", "FR", "GR", "HR", "AS", "BS", "CS", "ES", "FS", "GS", "HS", "AT", "BT", "CT", "ET", "FT", "GT", "HT", "AU", "BU", "CU", "EU", "FU", "GU", "HU", "AV", "BV", "CV", "EV", "FV", "GV", "HV", "AX", "BX", "CX", "EX", "FX", "GX", "HX", "BY", "CY", "EY", "FY", "GY", "HY", "AZ", "BZ", "CZ", "EZ", "FZ", "GZ", "DA", "DB", "DD", "DE", "DF", "DG", "DH", "DI", "DJ", "DK", "DL", "DN", "DP", "DR", "DS", "DT", "DU", "DV", "DX", "DY", "DZ", "KA", "KB", "KC", "KD", "KE", "KF", "KG", "KH", "KJ", "KK", "KL", "KN", "KP", "KR", "KS", "KT", "KU", "KV", "KX", "KY", "KZ", "LA", "LB", "LC", "LD", "LE", "LF", "LG", "LH", "LJ", "LK", "LL", "LN", "LP", "LR", "LS", "LT", "LU", "LV", "LX", "LY", "LZ", "NA", "NB", "NC", "ND", "NE", "NF", "NG", "NH", "NJ", "NK", "NL", "NN", "NP", "NR", "NS", "NT", "NU", "NV", "NY", "NZ", "PA", "PB", "PC", "PD", "PE", "PF", "PG", "PH", "PJ", "PK", "PL", "PN", "PP", "PS", "PT", "PU", "PV", "PX", "PY", "PZ", "NX", "RA", "RB", "RC", "RD", "RE", "RF", "RG", "RH", "RJ", "RK", "RL", "RN", "RP", "RR", "RS", "RT", "RU", "RV", "RX", "RY", "RZ", "HZ", "SA", "SB", "SC", "SD", "SE", "SF", "SG", "SH", "SJ", "SK", "SL", "SN", "SP", "SR", "SS", "ST", "SU", "SV", "SX", "SY", "SZ", "TA", "TB", "TC", "TD", "TE", "TF", "TG", "TH", "TJ", "TK", "TL", "TN", "TP", "TR", "TS", "TT", "TU", "TV", "TX", "TY", "TZ", "UA", "UB", "UC", "UD", "UE", "UF", "UG", "UH", "UJ", "UK", "UL", "UN", "UP", "UR", "US", "UT", "UU", "UV", "UX", "UY", "UZ", "VA", "VB", "VC", "VD", "VE", "VF", "VG", "VH", "VJ", "VK", "VL", "VN", "VP", "VR", "VS", "VT", "VU", "VV", "VX", "VY", "VZ", "XA", "XB", "XC", "XD", "XE", "XF", "XG", "XH", "XJ", "XK", "XL", "XM", "XN", "XP", "XQ", "XR", "XS", "XT", "XU", "XV", "XX", "XY", "XZ", "YA", "YB", "JA", "JB", "JC", "JD", "JE", "YC", "YD", "YE", "YF", "YG", "YH", "YJ", "YK", "YL", "YN", "YP", "YR", "YS", "YT", "YU", "YV", "YX", "YY", "YZ", "ZA", "ZB", "ZC", "ZD", "ZE", "ZF", "ZG", "ZH", "ZI", "ZJ", "ZK", "ZL", "JF", "JG", "JH", "ZN", "ZP", "ZR", "ZS", "ZT", "ZU", "ZV", "ZX", "ZY", "ZZ", "JL", "JN", "JO", "JP", "JR", "JS", "WA", "WB", "WC", "WD", "WE", "WF", "WG", "WH", "WJ", "WK", "WL", "WN", "WP", "WR", "WS", "WT", "WU", "JJ", "JK", "WV", "WW", "WX", "WY", "WZ", "ZW", "YW", "XW", "UW", "TW", "SW", "RW", "PW", "NW", "LW", "KW", "MZ", "MY", "MX", "MV", "MU", "MT", "MS", "JT", "JU", "JV", "JW", "JX", "JY", "JZ", "MA", "MB", "MC", "MD", "ME", "MF", "MG", "MH", "MJ", "MK", "ML", "MN", "MP", "MR" };

           Boolean esFormatoXXNNNN = this.esNumero(patente.Substring(2, 4));
           String letrasPatenteFormatoXXNNNN = patente.Substring(0, 2).ToUpper();
           String numerosPatenteFormatoXXNNNN = patente.Substring(2, 4).ToUpper();
           String[] letrasPatenteFormatoXXXXNN = new string[] { patente.Substring(0, 1).ToUpper(), patente.Substring(1, 1).ToUpper(), patente.Substring(2, 1).ToUpper(), patente.Substring(3, 1).ToUpper() };
           String numerosPatenteFormatoXXXXNN = patente.Substring(4, 2).ToUpper();
           int nroPatente = 0;
           int Digito;
           int Contador = 2;
           int Multiplo;
           int Acumulador = 0;
           string PatenteDigito;

           try
           {
               if (esFormatoXXNNNN)
               {
                   int valorSerieXXNNNN;
                   for (valorSerieXXNNNN = 0; valorSerieXXNNNN < serieLetraFormatoXXNNNN.Length; valorSerieXXNNNN++)
                   {

                       if (letrasPatenteFormatoXXNNNN.Equals(serieLetraFormatoXXNNNN[valorSerieXXNNNN]))
                       {
                           break;
                       }
                   }

                   nroPatente = Convert.ToInt32(valorSerieXXNNNN + numerosPatenteFormatoXXNNNN);
               }
               else
               {
                   // formato nuevo XXXX99
                   string valorSerieXXXX99 = "";
                   foreach (string letraPatente in letrasPatenteFormatoXXXXNN)
                   {
                       foreach (string[] letraTablaXXXXNN in serieLetraFormatoXXXXNN)
                       {
                           if (letraPatente.Equals(letraTablaXXXXNN[0]))
                           {
                               valorSerieXXXX99 = valorSerieXXXX99 + letraTablaXXXXNN[1];
                           }
                       }
                   }

                   nroPatente = Convert.ToInt32(valorSerieXXXX99 + numerosPatenteFormatoXXXXNN);
               }



               while (nroPatente != 0)
               {
                   Multiplo = (nroPatente % 10) * Contador;
                   Acumulador = Acumulador + Multiplo;
                   nroPatente = nroPatente / 10;

                   if (Contador == 7)
                   {
                       Contador = 2;
                   }
                   else
                   {
                       Contador = Contador + 1;
                   }
               }

               Digito = 11 - (Acumulador % 11);
               PatenteDigito = Digito.ToString().Trim();
               if (Digito == 10)
               {
                   PatenteDigito = "K";
               }

               if (Digito == 11)
               {
                   PatenteDigito = "0";
               }

               return PatenteDigito;
           }
           catch (Exception)
           {
               return "E"; // patente con error

           }
       }
       else
       {
           return "E"; //patente nula
       }
       
   }

   public string obtienerRUT(string p_rut)
   {
       string[] m_parte_rut = p_rut.Split('-');
       string m_rut = m_parte_rut[0].Replace(".", String.Empty);

       return m_rut;
   }

   public string formatoNumerico(string p_valorTexto)
   { 
       if (es_numero(p_valorTexto))
       {
           return string.Format("{0:#,##0.##}", Convert.ToDouble(p_valorTexto));
       }
       else{
           return p_valorTexto;
       }
   }


   public bool validaEsFecha(string p_fecha)
   {
       try
       {
           DateTime m_fechaInicio = Convert.ToDateTime(p_fecha);
           return true;
       }
       catch (Exception)
       {
           return false;

       }
   }

   public int obtenerFechaYYYYMMDD(String fecha)
   {
       int m_fecha_numero;
       // formato
       try
       {
            
           // para una solicitud nueva 
           DateTime m_fecha = Convert.ToDateTime(fecha);
           String mes = m_fecha.Month.ToString();
           if (m_fecha.Month < 10)
           {
               mes = "0" + m_fecha.Month;
           }

           String dia = m_fecha.Day.ToString();
           if (m_fecha.Day < 10)
           {
               dia = "0" + m_fecha.Day;
           }

           String m_fecha_String = (m_fecha.Year + " " + mes + " " + dia).Replace(" ","");
           m_fecha_numero = Convert.ToInt32(m_fecha_String);
       }
       catch (Exception)
       {
           m_fecha_numero = 0;

       }

       return m_fecha_numero;
   
   }
   public string obtieneFechaCorta(string p_fecha)
   {
       string m_fecha_corta;
       // formato
       try
       {
           string m_formato = "dd-MM-yyyy"; // formato db
           // para una solicitud nueva 
           DateTime m_time = Convert.ToDateTime(p_fecha);
           m_fecha_corta = m_time.ToString(m_formato);
       }
       catch (Exception)
       {
           m_fecha_corta = ""; 
         
       }

       return m_fecha_corta;
   }

   public string recortar(string texto, int largoSolicitado, bool soloRecortar)
   {
       if (soloRecortar)
       {
         return texto.Substring(0,largoSolicitado);
       }
       else
       {
         return recortar(texto, largoSolicitado);
       }
   }

   public string recortar(string texto, int largoSolicitado)
   {
       string conSignos = "��������������u�������������������'+�$�\"";
       string sinSignos = "aaaeeeiiiooouuunNAAAEEEIIIOOOUUUcC      ";
       StringBuilder textoLimpio = new StringBuilder(texto.Length);
       int indexConAcento;
       int largoTexto;

       foreach (char caracter in texto)
       {
           indexConAcento = conSignos.IndexOf(caracter);
           if (indexConAcento > -1)
               textoLimpio.Append(sinSignos.Substring(indexConAcento, 1));
           else
               textoLimpio.Append(caracter);
       }

       largoTexto= textoLimpio.Length;
       if (largoTexto > largoSolicitado)
       {
           return textoLimpio.ToString().Substring(0,largoSolicitado).Trim();
       }

       return textoLimpio.ToString();
        
   }


   public string limpiarEspacios(String texto)
   {
       if (texto != "")
       {
           return texto.Trim();
       }

       return texto;

   }

   public DateTime ConvertFromUnixTimestamp(double timestamp)
   {
       DateTime origin = new DateTime(1970, 1, 1, 0, 0, 0, 0);
       return origin.AddSeconds(timestamp);
   }


    public int ConvertToUnixTimestamp(DateTime date)
   {
       DateTime origin = new DateTime(1970, 1, 1, 0, 0, 0, 0);
       TimeSpan diff = date - origin;
       return Convert.ToInt32(Math.Floor(diff.TotalSeconds));
   }
} 
